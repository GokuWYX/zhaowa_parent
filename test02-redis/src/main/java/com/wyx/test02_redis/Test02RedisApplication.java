package com.wyx.test02_redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test02RedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(Test02RedisApplication.class, args);
    }

}
