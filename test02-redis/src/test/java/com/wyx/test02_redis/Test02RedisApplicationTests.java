package com.wyx.test02_redis;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.Serializable;

@SpringBootTest
@RunWith(SpringRunner.class)
class Test02RedisApplicationTests  {

        @Autowired
        @Qualifier("redisTemplate")
        RedisTemplate redisTemplate;


    @Test
    void contextLoads() {

        redisTemplate.opsForValue().set("name","123");

        System.out.println(redisTemplate.opsForValue().get("name"));

    }

}
