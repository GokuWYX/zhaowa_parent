package com.wyx.test02_redis;

import redis.clients.jedis.Jedis;

public class tets {
    public static void main(String[] args) {
        //1、new Jedis对象即可
        Jedis jedis=new Jedis("192.168.10.129",6381);
        // jedis 测试！
        System.out.println(jedis.ping());
        jedis.flushDB();
        jedis.set("姓名","王宇翔");
        System.out.println(jedis.get("姓名"));
    }
}
