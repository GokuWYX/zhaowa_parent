package com.wyx.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyx.pojo.Dict;
import com.wyx.pojo.User;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.xml.crypto.Data;
import java.util.*;


@SpringBootTest
class UserMapperTest {
    @Autowired
    private UserMapper mapper;

    @Autowired
    private DictMapper dictMapper;

    @Test
    public void findAll() {
        User user1 = new User( "我是改过的", 19, "12fs1412414@16.com");
        int i = mapper.updateById(user1);
        System.out.println(i);
        User userGet = mapper.selectById(user1.getId());
        System.out.println(user1);
        System.out.println(userGet);
    }

    @Test
    public void test01() {
        User wbd = new User("王二蛋", 19, "wbd嘤嘤嘤嘤嘤嘤嘤嘤@.com");
        System.out.println(mapper.insert(wbd));
    }

    @Test
    public void test02(){
        List<User> users = mapper.selectBatchIds(Arrays.asList(1, 2, 3, 4, 5));
        users.forEach((user)-> System.out.println(user));
    }

    @Test
    public void test03(){
        Page<User> pages = mapper.selectPage(new Page<>(2, 2), null);

        System.out.println("总页数"+pages.getPages());
        System.out.println("当前页"+pages.getCurrent());
        System.out.println("查询数据集合"+pages.getTotal());
        System.out.println("总记录数"+pages.getTotal());
        System.out.println("下一页"+pages.hasNext());
        System.out.println("上一页"+pages.hasPrevious());

        List<User> users = pages.getRecords();

        System.out.println("便利结果集");
        users.forEach((user -> System.out.println(user)));

    }

    @Test
    public void test04(){
        Page<Map<String,Object>> mapPage  = new Page<>(1, 5);
        Page<Map<String, Object>> page = mapper.selectMapsPage(mapPage, null);
        page.getRecords().forEach(System.out::println);
    }

    @Test
    public void test05(){
        int i = mapper.deleteById(1L);
        System.out.println(mapper.selectById(1L));
    }


    @Test
    public void test06(){
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        wrapper.eq(true,"is_deleted",0).eq("name","张三");
        Dict dict = dictMapper.selectOne(wrapper);
        System.out.println(dict.toString());
    }

    @Test
    public void test07(){
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        wrapper.eq(true,"is_deleted",0);
        wrapper.eq("name","张三");
        Dict dict = dictMapper.selectOne(wrapper);
        System.out.println(dict.toString());
    }


    @Test
    public void test08(){
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();

        wrapper.eq("name",11);

        System.out.println("------------------------");

        List<Dict> dicts1 = dictMapper.selectList(wrapper);

        dicts1.forEach(System.out::println);
        System.out.println(dicts1.size());
        System.out.println("------------------------");
    }
    @Test
    public void test09(){
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();

        System.out.println(dictMapper.selectList(wrapper));
    }


    @Test
    public void test10(){
        Date a=null;
        a.toString();
        System.out.println(a==null);


    }
}





