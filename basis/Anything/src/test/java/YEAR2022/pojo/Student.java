package YEAR2022.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.pojo
 * @ClassName: Student
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/25 9:42
 * @Version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private String name;
    private Integer id;
}
