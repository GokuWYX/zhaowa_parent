package YEAR2022.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.pojo
 * @ClassName: Bird
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/30 22:47
 * @Version: 1.0
 */


@AllArgsConstructor
@NoArgsConstructor
@Data
public class Bird{
    private String name;
    private Integer age;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bird bird = (Bird) o;
        return Objects.equals(name, bird.name) &&
                Objects.equals(age, bird.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}

