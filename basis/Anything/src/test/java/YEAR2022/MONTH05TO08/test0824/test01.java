package YEAR2022.MONTH05TO08.test0824;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.function.Consumer;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0824
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/24 9:17
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {
        Model model = new Model();
        Consumer<Model> sing = Model::sing;
        sing.accept(model);
    }
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class Model{
    private int id;
    private String name;
    public static void read(){
        System.out.println("读书");
    }
    public void sing(){
        System.out.println("唱歌");
    }
}