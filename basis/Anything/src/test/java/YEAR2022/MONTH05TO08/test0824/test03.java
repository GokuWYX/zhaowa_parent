package YEAR2022.MONTH05TO08.test0824;

import java.util.function.Function;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0824
 * @ClassName: test03
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/24 10:22
 * @Version: 1.0
 */
public class test03 {
    public static void main(String[] args) {
        Function<Integer,String[]> function = String[]::new;
        String[] strings = function.apply(6);
        System.out.println(strings.toString()+","+strings.length);
    }
}
