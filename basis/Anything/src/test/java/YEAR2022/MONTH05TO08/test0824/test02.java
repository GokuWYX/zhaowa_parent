package YEAR2022.MONTH05TO08.test0824;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0824
 * @ClassName: test02
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/24 9:55
 * @Version: 1.0
 */
public class test02 {
    public static void main(String[] args) {
        Supplier<Person> sup2 = Person::new;
        System.out.println(sup2.get());

        BiFunction<String, Integer, Person> fun2 = Person::new;
        System.out.println(fun2.apply("zx",13));

    }
}

@NoArgsConstructor
@Data
@AllArgsConstructor
class Person {
    private String name;
    private Integer id;
}
