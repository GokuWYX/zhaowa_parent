package YEAR2022.MONTH05TO08.test0506;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0506
 * @ClassName: test01
 * @Author: GoKu
 * @Description: 键盘输入的两种方法
 * @Date: 2022/5/6 19:25
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));

        String str=null;//用来接收内容
        try {
            str=buf.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("輸入内榕为："+str);
    }
}
