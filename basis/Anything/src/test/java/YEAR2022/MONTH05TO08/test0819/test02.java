package YEAR2022.MONTH05TO08.test0819;

import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0819
 * @ClassName: test02
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/19 9:58
 * @Version: 1.0
 */
public class test02 {
    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<>();

        Predicate<String> p1=t->t.contains("DA");
        Predicate<String> p2=t->t.length()>2;
        Stream.of("D","A","DTSA","DAnk","bbDAbb","AD").reduce(strings,(x,y)->{if (p1.and(p2).test(y))  x.add(y);return x;},(a,b)->a);

        //对strings遍历
        strings.forEach(System.out::println);
        //结果
        //DAnk
        //bbDAbb


    }
}
