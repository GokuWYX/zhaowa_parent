package YEAR2022.MONTH05TO08.test0819;

import java.util.stream.Stream;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0819
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/19 8:52
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {
        //
        Integer reduce = Stream.of(4, 5, 9, 3).reduce(0, (a, b) -> a + b);
        System.out.println(reduce);

        Integer reduce2 = Stream.of(4, 5, 9, 3).reduce(0, (a, b) -> Integer.sum(a,b));
        System.out.println(reduce2);

        Integer reduce4 = Stream.of(4, 5, 9, 3).reduce(19, Integer::sum);
        System.out.println(reduce4);

    }
}
