package YEAR2022.MONTH05TO08.test0819;

import java.util.stream.Stream;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0819
 * @ClassName: test03
 * @Author: GoKu
 * @Description: .
 * @Date: 2022/8/19 10:53
 * @Version: 1.0
 */
public class test03 {
    public static void main(String[] args) {
        //(4+1)*(4+2)*(4+3) = 210;
        Integer reduce = Stream.of(1, 2, 3).parallel().reduce(4, (x, y) -> (x + y), (a, b) -> (a * b));
        System.out.println(reduce);
    }
}
