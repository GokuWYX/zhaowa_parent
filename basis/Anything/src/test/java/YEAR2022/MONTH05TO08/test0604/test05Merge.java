package YEAR2022.MONTH05TO08.test0604;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0604
 * @ClassName: test05Merge
 * @Author: GoKu
 * @Description:
 * @Date: 2022/6/4 15:25
 * @Version: 1.0
 */
public class test05Merge {
    public static void main(String[] args) {
        int[] b={7,7,6,2,0,3};
        test05Merge.sort(b);
        for (int s : b) {
            System.out.print(s);
        }
    }
    private static int[] assist;

    public static void swap(int[] a,int j,int k){
        int tmp = a[j];
        a[j]=a[k];
        a[k]=tmp;
    }

    public static void sort(int[] a){
        assist=new int[a.length];
        int lo=0;
        int hi=a.length-1;
        sort(a,lo,hi);
    }
    public static void sort(int[] a,int lo,int hi){
        if (lo >= hi) {
            return;
        }
        int mid=(lo+hi)>>1;

        sort(a, lo, mid);
        sort(a,mid+1,hi);
        merge(a,lo,mid,hi);
    }
    public static void merge(int[] a,int lo,int mid,int hi){
        int index=lo;
        int p1=lo;
        int p2 =mid+1;

        while (p1<=mid && p2<=hi){
            if (a[p1]<a[p2]){
                assist[index++]=a[p1++];
            }else {
                assist[index++]=a[p2++];
            }
        }
        while (p1<=mid){
            assist[index++]=a[p1++];
        }
        while (p2<=hi){
            assist[index++]=a[p2++];
        }
        for (int i = lo; i <= hi; i++) {
            a[i]=assist[i];
        }

    }

}
