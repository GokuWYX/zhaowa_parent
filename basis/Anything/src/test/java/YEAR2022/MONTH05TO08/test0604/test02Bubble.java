package YEAR2022.MONTH05TO08.test0604;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0604
 * @ClassName: test02Bubble
 * @Author: GoKu
 * @Description:
 * @Date: 2022/6/4 11:02
 * @Version: 1.0
 */
public class test02Bubble {
    public static void main(String[] args) {
        int[] b={7,7,6,2,0,3};
        test02Bubble.sort(b);
        for (int s : b) {
            System.out.print(s);
        }
    }
    public static void swap(int[] a,int j,int k){
        int tmp = a[j];
        a[j]=a[k];
        a[k]=tmp;
    }

    public static void sort(int[] a){
        int l=a.length-1;
        while (true){
            int index=0;
            for (int i = 0; i < l; i++) {
                if (a[i]>a[i+1]){
                    swap(a,i,i+1);
                    index=i;
                }
            }

            if (index==0){
                break;
            }
            l=index;
        }


    }


}
