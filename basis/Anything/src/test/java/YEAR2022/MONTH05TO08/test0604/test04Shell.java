package YEAR2022.MONTH05TO08.test0604;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0604
 * @ClassName: test04Shell
 * @Author: GoKu
 * @Description:
 * @Date: 2022/6/4 15:25
 * @Version: 1.0
 */
public class test04Shell {
    public static void main(String[] args) {
        int[] b={7,7,6,2,0,3};
        test04Shell.sort(b);
        for (int s : b) {
            System.out.print(s);
        }
    }
    public static void swap(int[] a,int j,int k){
        int tmp = a[j];
        a[j]=a[k];
        a[k]=tmp;
    }

    public static void sort(int[] a){
        int N=a.length;
        int h=1;
        while (h < N / 2) {
            h=2*h+1;
        }
        while (h>=1){
            for (int i=h;i<N;i++){

                for (int j=i;j>=h;j-=h){
                    if (a[j-h]>a[j]){
                        swap(a,j-h,j);
                    }else {
                        break;
                    }
                }

            }
            h/=2;
        }


    }


}
