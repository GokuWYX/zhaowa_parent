package YEAR2022.MONTH05TO08.test0604;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0604
 * @ClassName: test06Quick
 * @Author: GoKu
 * @Description:
 * @Date: 2022/6/4 15:25
 * @Version: 1.0
 */
public class test06Quick {
    public static void main(String[] args) {
        int[] b={7,7,6,2,0,3};
        test06Quick.sort(b);
        for (int s : b) {
            System.out.print(s);
        }
    }
    public static void swap(int[] a,int j,int k){
        int tmp = a[j];
        a[j]=a[k];
        a[k]=tmp;
    }

    public static void sort(int[] a){
        int lo=0;
        int hi=a.length-1;
        sort(a,lo,hi);
    }
    private static void sort(int[] a,int lo,int hi){
        if (lo>=hi){ return; }

        int i = partition(a, lo, hi);
        sort(a, lo, i-1);
        sort(a, i+1, hi);

    }
    private static int partition(int[] a,int lo, int hi){
        int key=a[lo];
        int left=lo;
        int right=hi+1;
        while (true) {
            while (key<a[--right]){
                if (right==lo){
                    break;
                }
            }

            while (a[++left]<key){
                if (left==hi){
                    break;
                }
            }

            if (left>=right){
                break;
            }else {
                swap(a,left,right);
            }
        }
        swap(a, lo, right);
        return right;
    }
}
