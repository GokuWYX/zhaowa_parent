package YEAR2022.MONTH05TO08.test0604;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0604
 * @ClassName: test01Insertion
 * @Author: GoKu
 * @Description:
 * @Date: 2022/6/4 10:17
 * @Version: 1.0
 */
public class test01Insertion {
    public static void main(String[] args) {
        int[] b={7,7,6,2,0,3};
        test01Insertion.sort(b);
        for (int s : b) {
            System.out.print(s);
        }
    }
    public static void swap(int[] a,int j,int k){
        int tmp = a[j];
        a[j]=a[k];
        a[k]=tmp;
    }
    public static void sort(int[] a){
        for (int i = 1; i < a.length; i++) {
            for (int j=i;j>0; j--){
                if (a[j-1]>a[j]){
                    swap(a,j-1,j);
                }else {
                    break;
                }
            }
        }
    }
}
