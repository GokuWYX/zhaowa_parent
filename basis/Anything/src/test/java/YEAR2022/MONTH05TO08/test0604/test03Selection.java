package YEAR2022.MONTH05TO08.test0604;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0604
 * @ClassName: test03Selection
 * @Author: GoKu
 * @Description:
 * @Date: 2022/6/4 12:28
 * @Version: 1.0
 */
public class test03Selection {
    public static void main(String[] args) {
        int[] b={7,7,6,2,0,3};
        test03Selection.sort(b);
        for (int s : b) {
            System.out.print(s);
        }
    }
    public static void swap(int[] a,int j,int k){
        int tmp = a[j];
        a[j]=a[k];
        a[k]=tmp;
    }

    public static void sort(int[] a){
        for (int i = 0; i <= a.length-2; i++) {
            int minIndex=i;
            for (int j = i+1; j < a.length; j++) {
                if (a[minIndex]>a[j]){
                    minIndex=j;
                }
            }
            swap(a,minIndex,i);
        }
    }


}
