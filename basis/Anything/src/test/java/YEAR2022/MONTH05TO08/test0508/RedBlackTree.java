package YEAR2022.MONTH05TO08.test0508;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0508
 * @ClassName: RedBlackTree
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/8 16:38
 * @Version: 1.0
 */
public class RedBlackTree<Key extends Comparable<Key>,Value> {
    //红
    private final boolean RED=true;
    //黑
    private final boolean BLACK=false;

    //头结点
    private Node root;

    private class Node{
        public Key key;
        public Value value;
        public boolean color;
        public Node left;
        public Node right;

        public Node() {
        }

        public Node(Key key, Value value, boolean color, Node left, Node right) {
            this.key = key;
            this.value = value;
            this.color = color;
            this.left = left;
            this.right = right;
        }
    }

    public boolean isRED(Node node){
        return node.color;
    }
    public void colorReverse(Node node){
        node.color=RED;
        node.left.color=BLACK;
        node.right.color=BLACK;
    }

    public void put(Key key,Value value){
        root=put(root,key,value);
        root.color=BLACK;

    }
    private Node put(Node node,Key key,Value value){
        if (node==null){
            return new Node(key,value,RED,null,null);
        }
        int i = key.compareTo(node.key);
        Node result;
        if (i>0){
             node.right= put(node.right, key, value);
        }else if (i<0){
             node.left=put(node.left,key,value);
        }else {
            node.value=value;
        }
        if (isRED(node.right) && !isRED(node.left)){
            leftReverse(node);
        }

        if (isRED(node.left) && isRED(node.left.left)){
            rightReverse(node);
        }

        if (isRED(node.left) && isRED(node.right)){
            colorReverse(node);
        }

        return node;
    }
    //右红左黑，左旋
    private Node leftReverse(Node node){
        Node tmp=node.right;
        node.right=tmp.left;
        tmp.left=node;
        tmp.color=node.color;
        node.color=RED;
        return tmp;

    }
    //左红，左左红，右旋
    private Node rightReverse(Node node){
        Node tmp = node.left;
        node.left=tmp.right;
        tmp.right=node;
        tmp.color=node.color;
        node.color=RED;
        return tmp;
    }
    public Value get(Key key){
        return get(root,key);
    }
    private Value get(Node node,Key key){
        if (node==null){
            return null;
        }
        int i = key.compareTo(node.key);
        if (i>0){
            return get(node.right,key);
        }else if (i<0){
            return get(node.left,key);
        }else {
            return node.value;
        }
    }

}
