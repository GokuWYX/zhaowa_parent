package YEAR2022.MONTH05TO08.test0527;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0527
 * @ClassName: Test01_Insertion
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/27 9:37
 * @Version: 1.0
 */
public class Test01_Insertion {
    public static void main(String[] args) {
        int[] b={7,7,6,2,0,3};
        Test01_Insertion.Insertion(b);
        for (int s : b) {
            System.out.print(s);
        }
    }
    public static void swap(int[] a,int j,int k){
        int tmp = a[j];
        a[j]=a[k];
        a[k]=tmp;
    }

    public static void Insertion(int[] a){
        for (int i = 1; i < a.length; i++) {
            for (int i1 = i; i1 > 0; i1--) {
                if (a[i1-1]>a[i1]){
                    swap(a,i1-1,i1);
                }else {
                    break;
                }
            }
        }
    }

}
