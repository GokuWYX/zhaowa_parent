package YEAR2022.MONTH05TO08.test0831;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0831
 * @ClassName: test06
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/31 22:47
 * @Version: 1.0
 */
public class test06 {
    public static void main(String[] args) {
        ArrayList<Long> nums = new ArrayList<>();
        for (long i = 0; i < 10000000l; i++) {
            nums.add(i);
        }
        long start = System.currentTimeMillis();
        up(nums);
        long end=System.currentTimeMillis();
        System.out.println(end-start);
        long start1 = System.currentTimeMillis();
        aa(nums);
        long end1=System.currentTimeMillis();
        System.out.println(end1-start1);

//        Long[] array = nums.stream().parallel().toArray(Long[]::new);
//        System.out.println(array.length);
//        List<Long> collect = nums.stream().parallel().collect(Collectors.toList());
//        System.out.println(collect.size());
    }
    public static void up(ArrayList<Long> integers){
        ArrayList<Long> longs = new ArrayList<>();
        List<Long> collect = integers.stream().parallel().collect(Collectors.toList());
        collect.forEach(a->{longs.add(a);});

    }




    public static void aa(ArrayList<Long> integers) {
        ArrayList<Long> longs = new ArrayList<>();

        for (Long integer : integers) {
            longs.add(integer);
        }
    }



}
//对比直接存入数组一千个数的时间和存入数组的时间
