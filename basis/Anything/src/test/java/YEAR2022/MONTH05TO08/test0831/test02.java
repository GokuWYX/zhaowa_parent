package YEAR2022.MONTH05TO08.test0831;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0831
 * @ClassName: test02
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/31 10:47
 * @Version: 1.0
 */
public class test02 {
    public static void main(String[] args) {

        ArrayList<Studenter> studenters = new ArrayList<>();
        studenters.add(new Studenter("1",33,42));
        studenters.add(new Studenter("11",33,62));
        studenters.add(new Studenter("13",33,72));
        studenters.add(new Studenter("4",36,52));
        studenters.add(new Studenter("3",36,92));
        studenters.add(new Studenter("2",36,42));

        //根据年龄分一组，再根据这个年龄段的学生的成绩分组
        Map<Integer, Map<String, List<Studenter>>> collect = studenters.stream().collect(Collectors.groupingBy(a -> a.getAge(), Collectors.groupingBy(a -> {
            if (a.getScore() > 60) {
                return "及格了";
            } else {
                return "不及格";
            }

        })));
        collect.forEach((k,v)->{
            System.out.println("年龄："+k+"成绩"+v);
        });


    }
}
