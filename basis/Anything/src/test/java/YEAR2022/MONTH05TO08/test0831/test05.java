package YEAR2022.MONTH05TO08.test0831;

import java.util.stream.LongStream;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0831
 * @ClassName: test05
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/31 16:10
 * @Version: 1.0
 */
public class test05 {
    public static void main(String[] args) {
        test05 test05 = new test05();
        test05.test();
        test05.testByStream();
        test05.testByStreamPar();
    }


    public void test(){
        long result=0L;
        long size=1000000000;
        long start=System.currentTimeMillis();
        for (long i = 0; i <= size; i++) {
            result+=i;
        }
        long end=System.currentTimeMillis();
        long time=end - start;
        System.out.println(time);
        System.out.println(result);
    }


    public void testByStream(){
        int size=1000000000;
        long start=System.currentTimeMillis();
        int result=0;
        long reduce = LongStream.rangeClosed(0, size).reduce(0, Long::sum);
        long end=System.currentTimeMillis();
        long time=end - start;
        System.out.println(time);
        System.out.println(reduce);
    }

    public void testByStreamPar(){
        int size=1000000000;
        long start=System.currentTimeMillis();
        long reduce = LongStream.rangeClosed(0, size).parallel().reduce(0, Long::sum);
        long end=System.currentTimeMillis();
        System.out.println((end - start));
        System.out.println(reduce);
    }
}