package YEAR2022.MONTH05TO08.test0831;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0831
 * @ClassName: test01
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/31 9:02
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {

        ArrayList<Studenter> studenters = new ArrayList<>();
        studenters.add(new Studenter("1",33,42));
        studenters.add(new Studenter("11",3,62));
        studenters.add(new Studenter("13",33,72));
        studenters.add(new Studenter("4",36,52));
        studenters.add(new Studenter("3",63,92));
        studenters.add(new Studenter("2",13,42));
        //学生根据年龄分类并存入Map中
        Map<Integer, List<Studenter>> collect = studenters.stream().collect(Collectors.groupingBy(a -> a.getAge()));
        //遍历Map
        collect.forEach((k,v)->{
            System.out.println(k+"："+v);
        });


        //根据分数分组，大于60记为及格，小于60为不及格
        studenters.stream().collect(Collectors.groupingBy(a->{if (a.getScore()>60){
            return "及格";
        }else {
            return "不及格";
        }}));
    }
}

@AllArgsConstructor
@NoArgsConstructor
@Data
class Studenter{
    private String name;
    private Integer age;
    private Integer score;
}
