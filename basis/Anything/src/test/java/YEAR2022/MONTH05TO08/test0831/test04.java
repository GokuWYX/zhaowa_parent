package YEAR2022.MONTH05TO08.test0831;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0831
 * @ClassName: test04
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/31 12:15
 * @Version: 1.0
 */
public class test04 {
    public static void main(String[] args) {
        ArrayList<Studenter> studenters = new ArrayList<>();
        studenters.add(new Studenter("张三",33,62));
        studenters.add(new Studenter("里斯",33,62));
        studenters.add(new Studenter("王八蛋",33,72));
        studenters.add(new Studenter("赵刚",36,52));
        studenters.add(new Studenter("张飞",36,92));
        studenters.add(new Studenter("马云",36,42));
        //第一个参数是所有字符串中间的拼接值，第二个是第一个字符串前面的值，第三个是最后一个值
        //result start 张三 mid 里斯 mid 王八蛋 mid 赵刚 mid 张飞 mid 马云 end
        System.out.println(studenters.stream().map(Studenter::getName).collect(Collectors.joining(" mid ", "start ", " end")));


    }
 }
