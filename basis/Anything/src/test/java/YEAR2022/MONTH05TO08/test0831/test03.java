package YEAR2022.MONTH05TO08.test0831;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0831
 * @ClassName: test03
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/31 10:57
 * @Version: 1.0
 */
public class test03 {
    public static void main(String[] args) {
        ArrayList<Studenter> studenters = new ArrayList<>();
        studenters.add(new Studenter("1",33,62));
        studenters.add(new Studenter("11",33,62));
        studenters.add(new Studenter("13",33,72));
        studenters.add(new Studenter("4",36,52));
        studenters.add(new Studenter("3",36,92));
        studenters.add(new Studenter("2",36,42));

        //分数高于60设定为及格，低于60位不及格
        Map<Boolean, List<Studenter>> collect = studenters.stream().collect(Collectors.partitioningBy(a -> a.getScore() > 60));
        collect.forEach((k,v)->{
            if (k){
                System.out.print("及格");
                System.out.println(v);
            }else {
                System.out.print("不及格");
                System.out.println(v);

            }
        });

    }
}
