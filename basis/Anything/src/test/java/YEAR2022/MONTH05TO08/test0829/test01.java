package YEAR2022.MONTH05TO08.test0829;

import YEAR2022.pojo.Student;

import java.util.ArrayList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0829
 * @ClassName: test01
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/29 9:23
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student("张三",12));
        students.add(new Student("李四",121));
        students.add(new Student("王五",122));
        students.add(new Student("王钢蛋",132));
        students.add(new Student("尼古拉斯王麻子",125));
        students.add(new Student("铁拐李",1211));
        students.add(new Student("土行孙",1222));
        students.add(new Student("简",1299));
        students.stream().filter(student -> student.getName().length()<3).forEach(System.out::println);
        System.out.println("---------------------");
        students.stream().skip(3).forEach(System.out::println);
        System.out.println("-------------------------------");
        students.stream().limit(3).forEach(System.out::println);
        System.out.println("---------------------------------");

    }
}
