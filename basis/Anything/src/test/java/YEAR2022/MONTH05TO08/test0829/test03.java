package YEAR2022.MONTH05TO08.test0829;

import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0829
 * @ClassName: test03
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/29 15:35
 * @Version: 1.0
 */
public class test03 {
    public static void main(String[] args) {
        //形式1(不重要)
        Stream<String> stringStream = Stream.of("12", "323", "1111", "32233", "442");
        stringStream.map(Integer::parseInt).forEach(System.out::println);
        //形式2
        ArrayList<String> strings = new ArrayList<>();
        strings.add("12");
        strings.add("13");
        strings.add("14");
        strings.add("16");
        strings.stream().map(Integer::parseInt).forEach(System.out::println);
    }

}















