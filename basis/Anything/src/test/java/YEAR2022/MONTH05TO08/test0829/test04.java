package YEAR2022.MONTH05TO08.test0829;

import java.util.ArrayList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0829
 * @ClassName: test04
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/29 16:49
 * @Version: 1.0
 */
public class test04 {
    public static void main(String[] args) {

        //创建一堆重复数据
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        //过滤
        list.stream().distinct();

        list.forEach(System.out::print);
        //结果：01234567890123456789
    }
}
