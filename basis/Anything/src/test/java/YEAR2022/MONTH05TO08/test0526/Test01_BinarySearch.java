package YEAR2022.MONTH05TO08.test0526;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0526
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/26 8:50
 * @Version: 1.0
 */
public class Test01_BinarySearch {
    public int binrySearch(int[] a, int target){
        int l=0;
        int r=a[a.length-1];
        int mid;
        while (l<=r){
            mid=(l+r)>>>1;
            if (a[mid]==target){
                return mid;
            }else if (a[mid]<target){
                l=mid+1;
            }else {
                r=mid-1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        Test01_BinarySearch test01 = new Test01_BinarySearch();
        int[] a={1,3,4,5,6,7,9};
        System.out.println(test01.binrySearch(a, 6));
    }
}
