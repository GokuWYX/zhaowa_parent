package YEAR2022.MONTH05TO08.test0526;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0526
 * @ClassName: Test03_Selection
 * @Author: GoKu
 * @Description: 选择排序
 * @Date: 2022/5/26 15:22
 * @Version: 1.0
 */
public class Test03_Selection {
    public static void main(String[] args) {
        int[] b={7,7,6,2,0,3};
        Test03_Selection.Selection(b);
        for (int s : b) {
            System.out.print(s);
        }
    }
    public static void swap(int[] a,int j,int k){
        int tmp = a[j];
        a[j]=a[k];
        a[k]=tmp;
    }
    public static void Selection(int[] a){
        for (int i = 0; i <= a.length - 2; i++) {
            int minIndex=i;
            for (int i1 = i+1; i1 < a.length; i1++) {
                if (a[minIndex]>a[i1]){
                    minIndex=i1;
                }
            }
            swap(a,minIndex,i);
        }
    }
}
