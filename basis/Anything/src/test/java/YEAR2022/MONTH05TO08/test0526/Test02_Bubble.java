package YEAR2022.MONTH05TO08.test0526;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0526
 * @ClassName: Test02_Bubble
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/26 9:20
 * @Version: 1.0
 */
public class Test02_Bubble {
    public static void main(String[] args) {
        int[] b={7,7,6,2,0,3};
        Test02_Bubble.bubble(b);
        for (int s : b) {
            System.out.print(s);
        }
    }
    public static void swap(int[] a,int j,int k){
        int tmp = a[j];
        a[j]=a[k];
        a[k]=tmp;
    }
    public static void bubble(int[] a){
        int h=a.length-1;
        while (true){
            int min=0;
            for (int i = 0; i < h; i++) {
                if (a[i]>a[i+1]){
                    swap(a,i,i+1);
                    min=i+1;
                }
            }
            h=min;
            if (h==0){
                break;
            }
        }
    }

}
