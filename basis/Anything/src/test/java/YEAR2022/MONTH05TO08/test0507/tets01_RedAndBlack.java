package YEAR2022.MONTH05TO08.test0507;


/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0507
 * @ClassName: tets01_RedAndBlack
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/7 16:26
 * @Version: 1.0
 */
public class tets01_RedAndBlack<Key extends Comparable<Key>, Value> {
    public static void main(String[] args) {

    }
    private Node root;
    private int N;
    private static final boolean RED=true;
    private static final boolean BLACK=false;
    public void put(Key key, Value value){

        root=put(root,key,value);
        root.color=BLACK;
    }
    private Node put(Node h,Key key,Value value){
        if (h==null){
            N++;
        return new Node(key,value,null,null, RED);
        }
        int cmp = key.compareTo(h.key);
        if (cmp<0){
            h.left=put(h.left,key,value);
        }else if (cmp>0){
            h.right=put(h.right,key,value);
        }else {
            h.value=value;
        }
        if (isRed(h.right) && !isRed(h.left)){
            h=leftReverse(h);
        }
        h=rightReverse(h);
        if (isRed(h.left) && isRed(h.left.left)){
        }
        if (isRed(h.left) && isRed(h.right)){
            colorReverse(h);
        }
        return h;
    }

    public Value get(Key key){
        return get(root,key);
    }
    private Value get(Node node,Key key){
        if (node==null){
            return null;
        }
        int i = key.compareTo(node.key);
        if (i<0){
            return get(node.left,key);
        }else if (i>0){
            return get(node.right,key);
        }else {
            return node.value;
        }
    }


    private boolean isRed(Node x){
        if (x==null){
            return false;
        }
        return x.color==RED;
    }

    private Node leftReverse(Node h){
        Node x = h.right;
        h.right=x.left;
        x.left=h;
        x.color=h.color;
        h.color=RED;
        return x;
    }

    private Node rightReverse(Node h){
        Node x=h.left;
        h.left=x.right;
        x.right=h;
        x.color=h.color;
        h.color=RED;
        return x;
    }


    public void colorReverse(Node h){
        h.left.color=BLACK;
        h.right.color=BLACK;
        h.color=RED;

    }

    private class Node{

        public Key key;
        public Value value;
        public Node left;
        public Node right;
        public boolean color;
        //public void Node(){}
        public Node(Key key,
                         Value value,
                         Node left,
                         Node right,
                         boolean color){

            this.key=key;
            this.value=value;
            this.left=left;
            this.right=right;
            this.color=color;
        }

    }
}

