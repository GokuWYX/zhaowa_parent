package YEAR2022.MONTH05TO08.test0830;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.stream.Stream;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0830
 * @ClassName: test
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/30 11:12
 * @Version: 1.0
 */
public class test06 {
    public static void main(String[] args) {
        //统计所有金毛的年龄之和
        Integer ageSum = Stream.of(new KingMao(12, "小白"), new KingMao(13, "小黑"), new KingMao(14, "小黄")).map(a -> a.getAge()).reduce((a, b) -> a + b).get();
        System.out.println(ageSum);





    }




}

@Data
@NoArgsConstructor
@AllArgsConstructor
class KingMao{
    private Integer age;
    private String name;
}