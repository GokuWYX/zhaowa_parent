package YEAR2022.MONTH05TO08.test0830;

import java.util.ArrayList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0830
 * @ClassName: test04
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/30 10:11
 * @Version: 1.0
 */
public class test04 {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 10; i > 0; i--) {
            integers.add(i);
        }
        //累加
        System.out.println(integers.stream().reduce((a, b) -> a + b).get());


    }

}
