package YEAR2022.MONTH05TO08.test0830;

import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0830
 * @ClassName: test08
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/30 15:55
 * @Version: 1.0
 */
public class test08 {
    public static void main(String[] args) {
        //合并流：写法1，将两种不同类型的流合并成一种流并且遍历出来
        Stream<String> adawd = Stream.of("adawd");
        Stream<Integer> integerStream = Stream.of(12);
        Stream.concat(adawd,integerStream).forEach(System.out::println);

        //合并流：写法2，两种ArrayList转换成流合并便利出来
        ArrayList<Integer> integers = new ArrayList<>();
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 3; i < 7; i++) {
            integers.add(i*10);
            strings.add(String.valueOf(i));
        }
        Stream.concat(integers.stream(),strings.stream()).forEach(System.out::println);



    }
}
