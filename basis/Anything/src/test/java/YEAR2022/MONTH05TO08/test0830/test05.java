package YEAR2022.MONTH05TO08.test0830;

import java.util.ArrayList;
import java.util.function.Predicate;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0830
 * @ClassName: test05
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/30 10:35
 * @Version: 1.0
 */
public class test05 {
    public static void main(String[] args) {
        //创建一个又各种String类型的数组
        ArrayList<String> oldStrings = new ArrayList<>();
        oldStrings.add("D");
        oldStrings.add("Daa");
        oldStrings.add("DAa");
        oldStrings.add("DAasa");
        oldStrings.add("DA");
        oldStrings.add("da");

        //创建一个返回结果集
        ArrayList<String> resultStrings = new ArrayList<>();

        //创建筛选条件
        Predicate<String> p1=t->t.contains("DA");
        Predicate<String> p2=t->t.length()>2;

        oldStrings.stream().reduce(resultStrings,(x, y)->{
            if (p1.and(p2).test(y))  x.add(y);
               return x;},(a, b)->a);

        //对返回结果遍历
        resultStrings.forEach(System.out::println);




    }


}
