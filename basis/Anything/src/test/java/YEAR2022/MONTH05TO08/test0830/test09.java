package YEAR2022.MONTH05TO08.test0830;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0830
 * @ClassName: test09
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/30 16:12
 * @Version: 1.0
 */
public class test09 {
    public static void main(String[] args) {
        ArrayList<Person> one = new ArrayList<>();
        one.add(new Person("曾小贤"));
        one.add(new Person("胡一菲"));
        one.add(new Person("关谷神奇"));
        one.add(new Person("诸葛大力"));
        one.add(new Person("玉墨"));
        one.add(new Person("陈赫"));

        ArrayList<Person> two = new ArrayList<>();
        two.add(new Person("盖亚"));
        two.add(new Person("哈迪斯"));
        two.add(new Person("阿瑞斯"));
        two.add(new Person("伊迪斯"));
        two.add(new Person("宙斯"));
        two.add(new Person("赫拉克罗斯"));
        two.add(new Person("张飞"));
        two.add(new Person("刘张丰"));
        two.add(new Person("小张"));

//        1. 第一个队伍只要名字为3个字的成员姓名；
        one.stream().map(a->a.getName()).filter(a->a.length()==3).forEach(System.out::print);
        System.out.println();
        System.out.println("--------------------------------------");

//        2. 第一个队伍筛选之后只要前3个人；
        one.stream().limit(3).forEach(System.out::print);
        System.out.println();
        System.out.println("--------------------------------------");

//        3. 第二个队伍只要姓张的成员姓名；
        two.stream().map(a->a.getName()).filter(a->a.contains("张")).forEach(System.out::print);
        System.out.println();
        System.out.println("--------------------------------------");

//        4. 第二个队伍筛选之后不要前2个人；
        two.stream().skip(2).forEach(System.out::print);
        System.out.println();
        System.out.println("--------------------------------------");

//        5. 将两个队伍合并为一个队伍；
        Stream<Person> personStream = Stream.concat(one.stream(), two.stream());
        //personStream.forEach(System.out::print);
        System.out.println();
        System.out.println("--------------------------------------");

//        6. 根据姓名创建 Person 对象；
        personStream.map(a->a.getName()).map(Person::new).forEach(System.out::print);

//        7. 打印整个队伍的Person对象信息。
    }
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class Person{
    private String name;
}
