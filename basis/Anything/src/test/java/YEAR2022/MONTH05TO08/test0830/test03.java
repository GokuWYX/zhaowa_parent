package YEAR2022.MONTH05TO08.test0830;

import java.util.ArrayList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0830
 * @ClassName: test03
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/30 9:56
 * @Version: 1.0
 */
public class test03 {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 10; i > 0; i--) {
            integers.add(i);
        }
        //10
        System.out.println(integers.stream().max((a, b) -> a-b).get());
        //1
        System.out.println(integers.stream().min((a, b) -> a-b).get());

    }
}
