package YEAR2022.MONTH05TO08.test0830;

import java.util.ArrayList;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0830
 * @ClassName: test07
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/30 15:47
 * @Version: 1.0
 */
public class test07 {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            integers.add(i);
        }

        //筛选粗大于3的所有值，使用idea直接返回类型如下
        IntStream intStream = integers.stream().filter(a -> a > 3).mapToInt(Integer::intValue);
        Stream<Integer> integerStream = integers.stream().filter(a -> a > 3);


    }
}
