package YEAR2022.MONTH05TO08.test0830;

import java.util.ArrayList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0830
 * @ClassName: test02
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/30 9:34
 * @Version: 1.0
 */
public class test02 {
    public static void main(String[] args) {


        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            integers.add(i);
        }

        //快速得到流式集合的第一个值！
        System.out.println(integers.stream().findFirst().get());

        //快速得到流式集合的第一个值！
        System.out.println(integers.stream().findAny().get());

        //得到的结果都是1


    }
}
