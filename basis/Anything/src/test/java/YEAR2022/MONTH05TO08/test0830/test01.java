package YEAR2022.MONTH05TO08.test0830;

import java.util.ArrayList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0830
 * @ClassName: test01
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/30 8:42
 * @Version: 1.0
 */
public class test01 {

    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            integers.add(i);
        }
        //所有元素都处在大于0并小于11的区间吗？ true
        System.out.println(integers.stream().allMatch(a -> a > 0 && a<11));

        //所有元素都处在大于0并小于6的区间吗？ false
        System.out.println(integers.stream().allMatch(a -> a > 0 && a<6));

        //是否有一个元素满足这个条件？ true
        System.out.println(integers.stream().anyMatch(a -> a < 8));

        //是否有一个元素满足这个条件？ false
        System.out.println(integers.stream().anyMatch(a -> a > 80));

        //是否全不满足这个条件？ true
        System.out.println(integers.stream().noneMatch(a -> a > 1000));

        //是否全不满足这个条件？false
        System.out.println(integers.stream().noneMatch(a -> a <= 1));


    }
}
