package YEAR2022.MONTH05TO08.test0830;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0830
 * @ClassName: test10
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/30 22:27
 * @Version: 1.0
 */
public class test10 {
    public static void main(String[] args) {
        //Stream流中的结果到数组中。strs是可以便利的
        String[] strs = Stream.of("值1", "值2", "值3", "值4").toArray(String[]::new);

        //Stream流中的结果到集合中。strs是可以便利的
        List<String> stringList = Stream.of("值1", "值2", "值3", "值4").collect(Collectors.toList());
        Set<String> stringSet = Stream.of("值1", "值2", "值3", "值4").collect(Collectors.toSet());

    }
}










