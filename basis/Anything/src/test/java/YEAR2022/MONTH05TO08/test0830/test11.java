package YEAR2022.MONTH05TO08.test0830;

import YEAR2022.pojo.Bird;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0830
 * @ClassName: test11
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/30 22:46
 * @Version: 1.0
 */
public class test11 {
    public static void main(String[] args) {
        ArrayList<Bird> birds = new ArrayList<>();
        birds.add(new Bird("鹦鹉",3));
        birds.add(new Bird("大鹏",5));
        birds.add(new Bird("海鸥",4));
        birds.add(new Bird("大眼",2));
        birds.add(new Bird("老鹰",1));

        //获取年龄最大的鸟
        System.out.println(birds.stream().collect(Collectors.maxBy((o1, o2) -> o1.getAge() - o2.getAge())).get());
        //获取年龄最小的鸟
        System.out.println(birds.stream().collect(Collectors.minBy((o1, o2) -> o1.getAge() - o2.getAge())).get());

        //获取这几只鸟的年龄之和
        System.out.println(birds.stream().collect(Collectors.summingInt(o -> o.getAge())));

        //获取这几只鸟的平均年龄
        System.out.println(birds.stream().collect(Collectors.averagingInt(o -> o.getAge())));

        //获取这几只鸟的数量
        System.out.println(birds.stream().collect(Collectors.counting()));


        //返回结果是：
        //对年龄进行操作，哥伦布发现了新大陆！！！，我感觉这个方法比上面那个好用，因为上面那个一直在改内部代码，这个直接在代码尾get就可以了，真的无敌，你敢信这是我打错字发现的
        //获取全部聚合操作结果信息IntSummaryStatistics{count=5, sum=15, min=1, average=3.000000, max=5}
        System.out.println(birds.stream().collect(Collectors.summarizingInt(o -> o.getAge())));
        //获取鸟的年龄的和
        System.out.println(birds.stream().collect(Collectors.summarizingInt(o -> o.getAge())).getSum());
        //获取最大的鸟的年龄
        System.out.println(birds.stream().collect(Collectors.summarizingInt(o -> o.getAge())).getMax());
        //获取最小的鸟的年龄
        System.out.println(birds.stream().collect(Collectors.summarizingInt(o -> o.getAge())).getMin());
        //获取这几只鸟的年龄平均值
        System.out.println(birds.stream().collect(Collectors.summarizingInt(o -> o.getAge())).getAverage());
        //获取这几只鸟的数量和
        System.out.println(birds.stream().collect(Collectors.summarizingInt(o -> o.getAge())).getCount());


    }
}













