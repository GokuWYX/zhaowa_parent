package YEAR2022.MONTH05TO08.test0804;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0804
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/4 16:10
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {

        for (int i = 1; i <= 9; i++) {
            for (int i1 = 1; i1 <= i; i1++) {
                System.out.print(i1+"*"+i+"="+i1*i+" ");
            }
            System.out.println();
        }
    }
}
