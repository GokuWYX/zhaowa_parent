package YEAR2022.MONTH05TO08.test0825;

import YEAR2022.pojo.Student;

import java.util.ArrayList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0825
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/25 9:32
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {
        // STOPSHIP: 2022/8/25
        ArrayList<Student> students=new ArrayList<Student>();
        students.add(new Student("1",1));
        students.add(new Student("1",2));
        students.add(new Student("1",3));
        students.add(new Student("1",4));
        students.add(new Student("1",5));
        students.stream().filter(x->x.getId()>3).forEach(System.out::println);
        students.stream().skip(3).forEach(System.out::println);
        //count
        System.out.println(students.stream().filter(x -> x.getId() < 4).count());
    }
}
