package YEAR2022.MONTH05TO08.test0825;

import YEAR2022.pojo.Student;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0825
 * @ClassName: test02
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/8/25 10:50
 * @Version: 1.0
 */
public class test02 {
    public static void main(String[] args) {
        // STOPSHIP: 2022/8/25
        ArrayList<Student> students=new ArrayList<Student>();
        students.add(new Student("1",1));
        students.add(new Student("1",2));
        students.add(new Student("1",3));
        students.add(new Student("1",4));
        students.add(new Student("1",5));

        ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            strings.add(String.valueOf(i));
        }

        Integer[] array= (Integer[]) strings.stream().map(Integer::parseInt).toArray();
        ArrayList<Integer> integers = new ArrayList<>();
        integers.addAll(Arrays.asList(array));


    }
}
