package YEAR2022.MONTH05TO08.test0601;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0601
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/6/1 18:50
 * @Version: 1.0
 */
public class test01 {

    public static void main(String[] args) {
        MaoKe mao = new mao();
        mao.running();
        MaoKe hu = new hu();
        hu.running();
    }



}

class mao extends MaoKe {

    @Override
    public void eat() {
        System.out.println("猫吃鱼");
    }
}

class  hu extends MaoKe {

    @Override
    public void eat() {
        System.out.println("虎吃肉");
    }
}

interface maonmao{
    public int a();
}

abstract class MaoKe{
    public void running(){
        System.out.println("run");
    }
    public abstract void eat();
}

