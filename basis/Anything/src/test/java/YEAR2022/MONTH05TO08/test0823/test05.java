package YEAR2022.MONTH05TO08.test0823;

import java.util.ArrayList;
import java.util.function.Predicate;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0823
 * @ClassName: test05
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/23 15:08
 * @Version: 1.0
 */
public class test05 {
    public static void main(String[] args) {
        ArrayList<String> langguges = new ArrayList<>();
        langguges.add("Jdk8");
        langguges.add("Java");
        langguges.add("Jdk6");
        for (String langguge : langguges) {
            System.out.println(judge((String str) -> str.contains("Jd"), (String str) -> str.length() > 2, langguge));
        }

    }
    //如果包含Jd，并且长度大于2，返回正规语言，负责返回垃圾语言
    public static String judge(Predicate<String> p1,Predicate<String> p2,String langguge){
        boolean test = p1.and(p2).test(langguge);
        return test ? "正规语言":"垃圾语言";
    }
}
