package YEAR2022.MONTH05TO08.test0823;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.function.Supplier;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0823
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/23 9:19
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {
        Supplier<String> supplier=()->String.valueOf(new Date());
        System.out.println(getTime(() -> supplier.get()));


    }

    public static String getTime(Supplier<String> tools){
        return tools.get();
    }







}

@Data
@AllArgsConstructor
@NoArgsConstructor
class Sttudent{
    private int id;
    private String name;
}
