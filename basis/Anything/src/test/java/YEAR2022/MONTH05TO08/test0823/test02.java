package YEAR2022.MONTH05TO08.test0823;

import java.util.Arrays;
import java.util.function.Supplier;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0823
 * @ClassName: test02
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/23 10:28
 * @Version: 1.0
 */
public class test02 {

    public static void main(String[] args) {
        int[] arr = {10, 20, 100, 30, 40, 50};


        //1、从这进入方法
        Integer result = printMax(() -> {
            //3、代码的真正逻辑运算
            Arrays.sort(arr);
            return arr[arr.length - 1];// 最后就是最大的
        });
        //5、prinMax()方法执行结束，获得结果并返回
        System.out.println(result);

    }


    private static Integer printMax(Supplier<Integer> supplier){
        //2、进来之后在这运行get()方法进入3
        int max = supplier.get();
        //4、最后在此获得结果
        return max;
    }
}
