package YEAR2022.MONTH05TO08.test0823;

import java.util.function.Function;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0823
 * @ClassName: test04
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/23 14:52
 * @Version: 1.0
 */
public class test04 {
    public static void main(String[] args) {
        Integer result = consertAndRide((String str) -> Integer.parseInt(str),
                                         (Integer i) -> i * 10);
        System.out.println(result);
    }
    public static Integer consertAndRide(Function<String,Integer> f1,Function<Integer,Integer> f2){
        return f1.andThen(f2).apply("4396");
    }
}
