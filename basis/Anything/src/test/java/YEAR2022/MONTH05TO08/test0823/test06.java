package YEAR2022.MONTH05TO08.test0823;

import org.junit.Test;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0823
 * @ClassName: test06
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/23 16:25
 * @Version: 1.0
 */
public class test06 {
    public static void main(String[] args) {

    }
    @Test
    public void test03() {
        Function<String, Integer> f1 = (s) -> { return s.length(); };
        System.out.println(f1.apply("abc"));

        Function<String, Integer> f2 = String::length;
        System.out.println(f2.apply("abc"));
        BiFunction<String, Integer, String> bif = String::substring;
        String hello = bif.apply("hello", 2);
        System.out.println("hello = " + hello);
    }

}
