package YEAR2022.MONTH05TO08.test0823;

import java.util.function.Consumer;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0823
 * @ClassName: test03
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/23 11:05
 * @Version: 1.0
 */
public class test03 {
    public static void main(String[] args) {
        UpAndLo((String str)-> {
            //第一次消费，对应lo
            System.out.println(str.toLowerCase());
        },(String str)->{
            //第二次消费，对应up
            System.out.println(str.toUpperCase());
        });
    }
    public static void UpAndLo(Consumer<String> lo,Consumer<String> up){
        //在这定义需要消费的值
        String beConsunmed="UPUPUPlowerlowerlower";
        //先后执行lo和up两个消费者方法
        lo.andThen(up).accept(beConsunmed);
    }
}
