package YEAR2022.MONTH05TO08.test0608;

import org.assertj.core.util.Lists;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0608
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/6/8 15:28
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {
        List<Animal> animals= Lists.newArrayList();
        List<? super Dog> dogs=animals;
        dogs.add(new Dog());
//        dogs.add(new Cat());
//        dogs.add(new Animal());
        new ArrayList<>().add("as");
        new LinkedList<>().get(3);
        new HashMap<>();
    }
}

class Animal{}

class Dog extends Animal {}

class Cat extends Animal {}
