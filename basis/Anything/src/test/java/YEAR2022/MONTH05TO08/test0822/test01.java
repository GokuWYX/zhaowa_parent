package YEAR2022.MONTH05TO08.test0822;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0820
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/22 9:03
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {
        AA ab=new BB();
        ab.getB();
        ab.getA();
    }
}

class BB implements AA {

    @Override
    public void getB() {
        System.out.println("BBBBBBBBB");
    }
}

interface AA{
    default void getA(){
        System.out.println("A");
    }
    void getB();
}