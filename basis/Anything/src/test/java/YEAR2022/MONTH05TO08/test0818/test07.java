package YEAR2022.MONTH05TO08.test0818;

import java.util.Arrays;
import java.util.List;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0818
 * @ClassName: test07
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/18 10:28
 * @Version: 1.0
 */
public class test07 {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("a", "b", "s", "dsf", "afaf", "adaf", "Ad");
        //写法1
        list.forEach(a-> System.out.println(a));
        //写法2
        list.forEach(System.out::println);
    }
}
