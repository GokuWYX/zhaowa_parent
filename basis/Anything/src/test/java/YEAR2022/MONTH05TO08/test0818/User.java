package YEAR2022.MONTH05TO08.test0818;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0818
 * @ClassName: User
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/18 8:36
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String name;
    private String phone;
    private Integer age;

}
