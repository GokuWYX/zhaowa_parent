package YEAR2022.MONTH05TO08.test0818;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0818
 * @ClassName: test09
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/18 15:47
 * @Version: 1.0
 */
public class test09 {
    public static void main(String[] args) {
        ArrayList<User> users = new ArrayList<>();
        users.add(new User("a","1654644861",1));
        users.add(new User("d","1651616615",3));
        users.add(new User("c","1611616578",23));
        //将所有用户额手机号放到一个List里面
        List<String> collect = users.stream().map(user -> user.getPhone()).collect(Collectors.toList());

    }
}
