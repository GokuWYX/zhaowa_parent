package YEAR2022.MONTH05TO08.test0818;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0818
 * @ClassName: test08
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/18 10:38
 * @Version: 1.0
 */
public class test08
{
    public static void main(String[] args) {

        List<String> list = Arrays.asList("a", "b", "s", "dsf", "afaf", "adaf", "Ad");
        list.stream().filter(x->x.length()>3).collect(Collectors.toList()).forEach(System.out::println);
        System.out.println(list.stream().filter(x -> x.length() > 2).collect(Collectors.counting()));
    }
}
