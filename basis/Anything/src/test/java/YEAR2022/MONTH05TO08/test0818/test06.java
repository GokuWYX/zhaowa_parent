package YEAR2022.MONTH05TO08.test0818;

import java.util.Arrays;
import java.util.List;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0818
 * @ClassName: test06
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/18 10:22
 * @Version: 1.0
 */
public class test06 {
    public static void main(String[] args) {
        List<User> list = Arrays.asList(new User("a","a",1),new User("a","a",2),new User("a","a",12),new User("a","a",122));
        list.forEach(a-> System.out.println(a.getAge()));

    }
}
