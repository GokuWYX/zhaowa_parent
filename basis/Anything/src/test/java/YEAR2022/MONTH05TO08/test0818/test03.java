package YEAR2022.MONTH05TO08.test0818;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0818
 * @ClassName: test03
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/18 9:22
 * @Version: 1.0
 */
public class test03 {
    public static void main(String[] args) {
        ArrayList<User> users = new ArrayList<>();
        for (int i = 20; i >=4; i--) {
            Random random = new Random();
            int i1 = random.nextInt(2147483647);
            users.add(new User(String.valueOf(i1),String.valueOf(i*345+34234243),i));
        }
        Collections.sort(users, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                if (o1.getName().length()<o2.getName().length()){
                    return o2.getAge()-o1.getAge();
                }else {
                    return o1.getAge()-o2.getAge();
                }
            }
        });
        //Collections.sort(users,(x, y)->x.getName().length()-y.getName().length());

        for (User user : users) {
            System.out.println(user.toString());
        }

    }
}
