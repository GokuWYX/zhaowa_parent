package YEAR2022.MONTH05TO08.test0818;

import java.util.ArrayList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0818
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/18 8:34
 * @Version: 1.0
 */
public class test04 {
    public static void main(String[] args) {
        ArrayList<User> users = new ArrayList<>();
        for (int i = 20; i >=4; i--) {
            users.add(new User("name"+i,String.valueOf(i*345+34234243),i));
        }
//        Collections.sort(users, new Comparator<User>() {
//            @Override
//            public int compare(User o1, User o2) {
//                return o2.getAge()-o1.getAge();
//            }
//        });
        users.sort((o1,o2)->o1.getAge()-o2.getAge());

        for (User user : users) {
            System.out.println(user.toString());
        }


    }
}
