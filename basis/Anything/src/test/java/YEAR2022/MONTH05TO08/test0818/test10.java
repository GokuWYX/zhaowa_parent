package YEAR2022.MONTH05TO08.test0818;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test0818
 * @ClassName: test10
 * @Author: GoKu
 * @Description:
 * @Date: 2022/8/18 19:27
 * @Version: 1.0
 */
public class test10 {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("Java", "C", "C++", "Python", "Java8", "JDK", "JRE");
        //创建两个条件
        Predicate<String> p1 = x -> x.startsWith("J");
        Predicate<String> p2 = x -> x.length()>3;
        Predicate<String> p3 = x -> x.contains("C");
        //将不同条件放进过滤语句里面，可以放入无数种相关联的条件
        // 与 或
        list.stream().filter(p1.and(p2).or(p3)).forEach(System.out::println);


    }
}
