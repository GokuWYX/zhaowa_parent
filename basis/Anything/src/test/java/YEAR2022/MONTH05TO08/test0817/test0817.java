package YEAR2022.MONTH05TO08.test0817;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022
 * @ClassName: test0817
 * @Author: GoKu
 * @Description: lambada表达式的预习
 * @Date: 2022/8/17 15:56
 * @Version: 1.0
 */
public class test0817 {
    public static void main(String[] args) {
        new Thread(()->{
            System.out.println(Thread.currentThread().getName());
        }).start();

        new Thread(()-> {
            Thread.currentThread().setName("sdasdadasdsdadsa");
            System.out.println(Thread.currentThread().getName());
        }).start();



    }
}
