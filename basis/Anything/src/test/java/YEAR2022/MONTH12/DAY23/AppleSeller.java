package YEAR2022.MONTH12.DAY23;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-12-23 09:39
 * @description:
 **/
public class AppleSeller implements Tree{

    private Tree tree;

    public AppleSeller(Tree tree){
        this.tree=tree;
    }
    @Override
    public String rise(String root) {
        pre();
        String rise = tree.rise(root);
        after();
        return rise;
    }
    public void pre(){
        System.out.println("-----------------------------前置------------------");
    }
    public void after(){
        System.out.println("-----------------------------houzhi----------------");
    }



}
