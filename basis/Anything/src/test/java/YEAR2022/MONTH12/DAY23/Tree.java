package YEAR2022.MONTH12.DAY23;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-12-23 09:36
 * @description:
 **/
public interface Tree {
    public String rise(String root);
}
