package YEAR2022.MONTH12.DAY28;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-12-28 20:22
 * @description:
 **/
public class AbstartFactoryPattern{

    public static void main(String[] args) {
        //根据不同工厂，生产哈不同的产品
        //外国工厂
        AbstartFactory foreignFactory = new ForeignFactory();
        foreignFactory.createMask().printName();//外国口罩--N95是老美制定的标准
        foreignFactory.createPhone().printName();//外国手机--苹果
        //国产工厂
        AbstartFactory innerFactory = new InnerFactory();
        innerFactory.createMask().printName();//中国口罩--KN95是国产标准
        innerFactory.createPhone().printName();//中国手机--小米

    }
}

//抽象工厂
interface AbstartFactory{
    Phone createPhone();
    Mask createMask();
};

//具体工厂:外国工厂
class ForeignFactory implements AbstartFactory{

    @Override
    public Phone createPhone() {
        return new IPhone();
    }

    @Override
    public Mask createMask() {
        return new N95();
    }
}

//具体工厂:外国工厂
class InnerFactory implements AbstartFactory{

    @Override
    public Phone createPhone() {
        return new XiaoMi();
    }

    @Override
    public Mask createMask() {
        return new KN95();
    }
}

//产品大类--手机
interface Phone{ void  printName();}
class IPhone implements Phone{
    @Override
    public void printName() {
        System.out.println("外国手机--苹果");
    }
}
class XiaoMi implements Phone{
    @Override
    public void printName() {
        System.out.println("中国手机--小米");
    }
}

//产品大类--口罩
interface Mask{void  printName();}
class N95 implements Mask{
    @Override
    public void printName() {
        System.out.println("外国口罩--N95是老美制定的标准");
    }
}
class KN95 implements Mask{
    @Override
    public void printName() {
        System.out.println("中国口罩--KN95是国产标准");
    }
}







