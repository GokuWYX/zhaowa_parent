//package YEAR2022.MONTH12.DAY28;
//
///**
// * @program: zhaowa_parent
// * @author: Gogoku
// * @create: 2022-12-28 19:56
// * @description:
// **/
//
//public class FactoryPattern {
//
//    public static void main(String[] args) {
//        //iphone工厂创建iphone
//        Factory iPhoneFacory= new IPhoneFacory();
//        iPhoneFacory.createPhone().printName();//我是苹果手机
//
//        //华为工厂创建华为
//        Factory huaweiFactory=new HuaweiFactory();
//        huaweiFactory.createPhone().printName();//我是华为手机
//    }
//}
////手机接口
//interface Phone{ void printName();}
////手机接口实现类
//class IPhone implements Phone{
//    @Override
//    public void printName() {
//        System.out.println("我是苹果手机");
//    }
//}
//class Huaiwei implements Phone{
//    @Override
//    public void printName() {
//        System.out.println("我是华为手机");
//    }
//}
////工厂接口
//interface Factory{ Phone createPhone();}
////工厂接口实现类
//class IPhoneFacory implements Factory{
//    @Override
//    public Phone createPhone() {
//        return new IPhone();
//    }
//}
//class HuaweiFactory implements Factory{
//    @Override
//    public Phone createPhone() {
//        return new Huaiwei();
//    }
//}