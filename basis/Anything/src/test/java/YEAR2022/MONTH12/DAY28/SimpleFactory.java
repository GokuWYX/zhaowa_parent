package YEAR2022.MONTH12.DAY28;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-12-28 20:13
 * @description:
 **/


public class SimpleFactory {

    public static void main(String[] args) {
        createProduct("A").print();//产品A
        createProduct("B").print();//产品B

    }
    public static Product createProduct(String productName){
        if (productName.equals("A")){
            return new ProductA();
        }
        if (productName.equals("B")){
            return new ProductB();
        }
        return null;
    }
}
abstract class Product{ public abstract void print();}

class ProductA extends Product{
    @Override
    public void print() {
        System.out.println("产品A");
    }
}

class ProductB extends Product{

    @Override
    public void print() {
        System.out.println("产品B");
    }
}