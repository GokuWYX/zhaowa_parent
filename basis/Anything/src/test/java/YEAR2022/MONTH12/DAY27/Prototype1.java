package YEAR2022.MONTH12.DAY27;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 浅拷贝&深拷贝
 */
@Data
public class Prototype1 implements Cloneable {

    private String name;

    private List<String> arrayList = new ArrayList<>();



    public void setValue(String value) {
        this.arrayList.add(value);
    }

    public List<String> getValue() {
        return this.arrayList;
    }

    /**
     * 浅拷贝
     * @return
     */
//    @Override
//    protected Prototype1 clone() {
//        try {
//            return (Prototype1)super.clone();
//        } catch (CloneNotSupportedException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    /**
     * 深拷贝
     * @return
     */
    @Override
    protected Prototype1 clone() {
        Prototype1 prototype1 = null;
        try {
            prototype1 = (Prototype1)super.clone();
            prototype1.setArrayList(new ArrayList<>());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return prototype1;
    }

    public static void main(String[] args) {
        Prototype1 prototype1 = new Prototype1();
        prototype1.setName("orign object");
        prototype1.setValue("orign object");

        Prototype1 clone = prototype1.clone();
        clone.setName("clone object");
        /** 发现添加了执行了clone对象的setValue之后，也修改了prototype1中的arrayList中数据 */
        clone.setValue("clone object");
        System.out.println(prototype1);
        System.out.println(clone);
    }

}
