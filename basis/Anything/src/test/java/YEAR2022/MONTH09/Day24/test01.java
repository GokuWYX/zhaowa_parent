package YEAR2022.MONTH09.Day24;

import java.util.concurrent.atomic.AtomicInteger;

public class test01 {
    public static void main(String[] args) {

        AtomicInteger atomicInteger = new AtomicInteger();
        atomicInteger.set(12321321);
        System.out.println(atomicInteger);
        atomicInteger.get();
        System.out.println(atomicInteger.getAndSet(13));
        System.out.println(atomicInteger);

        System.out.println(atomicInteger.getAndAdd(13));
        System.out.println(atomicInteger);

    }
}
