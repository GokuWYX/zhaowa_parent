package YEAR2022.MONTH09.Day02;

import java.time.*;
import java.time.chrono.HijrahDate;
import java.time.chrono.JapaneseDate;
import java.time.chrono.MinguoDate;
import java.time.chrono.ThaiBuddhistDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

/**
 * @ProjectName: zhaowa_parent
 * @Package: YEAR2022.MONTH09.Day02
 * @ClassName: test02_Time
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/2 9:57
 * @Version: 1.0
 */
public class test02_Time {
    public static void main(String[] args) {
        // LocalDate ：表示日期，包含年月日，格式为 2019-10-16
        System.out.println(LocalDate.now());  //2022-09-02
        System.out.println(LocalDate.now().getYear());//2022
        System.out.println(LocalDate.now().getMonth());//SEPTEMBER
        System.out.println(LocalDate.now().getDayOfMonth());//2
        System.out.println(LocalDate.now().getDayOfYear());//245
        System.out.println(LocalDate.now().getDayOfWeek());//FRIDAY

        System.out.println(LocalDate.of(1999, 10, 14));//1999-10-14

        // LocalTime ：表示时间，包含时分秒，格式为 16:38:54.158549300
        System.out.println(LocalTime.now());  //10:01:25.652

        /*
        * 对日期时间的修改，对已存在的LocalDate对象，创建它的修改版，最简单的方式是使用withAttribute方法。
withAttribute方法会创建对象的一个副本，并按照需要修改它的属性。以下所有的方法都返回了一个修改属性的对
象，他们不会影响原来的对象。
        * */
        // LocalDateTime ：表示日期时间，包含年月日，时分秒，格式为 2018-09-06T15:33:56.750
        System.out.println(LocalDateTime.from(LocalDateTime.of(2021, 2, 3, 14, 23, 38)));//2021-02-03T14:23:38
        //当前时间
        LocalDateTime now = LocalDateTime.now();
        //将当前日期修改为1888不安，8月
        System.out.println(now.withYear(1888).withMonth(8));//1888-08-02T10:41:48.294
        //将当前日期增加一年之后的时间
        System.out.println(now.plusYears(1));
        //将当前日期增加45天之后的时间
        now.plusDays(45);


        // 日期时间的比较
        //模拟一个过去的时间,修改为6月份
        LocalDateTime old = now.withMonth(6);
        //模拟一个未来的时间，修改为2099年
        LocalDateTime future = now.withYear(2099);
        //now 是old的未来时间吗？  这英国佬的语法太sb了
        System.out.println(now.isAfter(old));//true
        System.out.println(now.isAfter(future));//false
        System.out.println(now.isBefore(old));//false
        System.out.println(now.isBefore(future));//true


        // DateTimeFormatter ：日期时间格式化类。
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formatDate = now.format(dateTimeFormatter);
        System.out.println(formatDate);//2022-09-02 12:08:39

        // Instant：时间戳，表示一个特定的时间瞬间。
        // Instant 时间戳/时间线，内部保存了从1970年1月1日 00:00:00以来的秒和纳秒。
        Instant instantNow = Instant.now();//当前时间
        System.out.println(instantNow);//2022-09-02T04:08:39.288Z
        //加2232秒
        System.out.println(instantNow.plusSeconds(2232));//2022-09-02T04:55:08.963Z
        //获得纳秒
        System.out.println(instantNow.getNano());//288000000
        //获得秒
        System.out.println(instantNow.getEpochSecond());//1662091802
        System.out.println(instantNow.toEpochMilli());//1662091848185
        System.out.println(System.currentTimeMillis());//1662091884507
        System.out.println(Instant.ofEpochSecond(5));//1970-01-01T00:00:05Z

        // Duration：用于计算2个时间(LocalTime，时分秒)的距离
        //一天多少秒
        System.out.println(Duration.ofDays(1).getSeconds());//86400
        //今天是2022-09-02T12:26:26.520，old是2022-06-02T12:25:28.514，future是2099-09-02T12:28:02.690
        System.out.println(Duration.between(old, now).toDays());//92
        System.out.println(Duration.between(old, future).toHours());//677184

        // Period：用于计算2个日期(LocalDate，年月日)的距离
        //2022和2099差了77年
        System.out.println(Period.between(old.toLocalDate(),future.toLocalDate()).getYears());//77
        //2013,10,23有几个月       自定义一个Period格式的时间Period.of()
        System.out.println(Period.from(Period.of(2013,10,23)).getMonths());//10

        //JDK 8的时间校正器
        //有时我们可能需要获取例如：将日期调整到“下一个月的第一天”等操作。可以通过时间校正器来进行。
        //TemporalAdjuster : 时间校正器。
        //TemporalAdjusters : 该类通过静态方法提供了大量的常用TemporalAdjuster的实现。

        //将日期调整为  两个月之后的第一天     自定义时间调整器
        TemporalAdjuster fiesw=temporal -> {
            //temporal需要调整的时间
            LocalDateTime dateTime = (LocalDateTime) temporal;
            LocalDateTime nextNextMonth = dateTime.plusMonths(2).withDayOfMonth(1);
            System.out.println("两个月后的第一天"+nextNextMonth);//两个月后的第一天2022-11-01T18:18:26.877
            return nextNextMonth;
        };
        LocalDateTime nextsMonth = now.with(fiesw);
        System.out.println("两个月后的第一天"+nextsMonth);//两个月后的第一天2022-11-01T18:18:26.877

        //JDK自带时间调整器
        TemporalAdjuster adjuster = TemporalAdjusters.firstDayOfNextYear();
        System.out.println(now.with(adjuster));//2023-01-01T18:29:17.409

        // ZonedDateTime ：包含时区的时间 当前时区的时间以及详细信息
        System.out.println(ZonedDateTime.now());//2022-09-02T12:43:52.017+08:00[Asia/Shanghai]

        // ThaiBuddhistDate：泰国佛教历
        System.out.println(ThaiBuddhistDate.now());//ThaiBuddhist BE 2565-09-02

        // MinguoDate：中华民国历
        System.out.println(MinguoDate.now());//Minguo ROC 111-09-02

        // JapaneseDate：日本历
        System.out.println(JapaneseDate.now());//Japanese Heisei 34-09-02

        // HijrahDate：伊斯兰历
        System.out.println(HijrahDate.now());//Hijrah-umalqura AH 1444-02-06

        //1、获取所有的时区ID
        //ZoneId.getAvailableZoneIds().forEach(System.out::println);

        //不带时区的计算机当前时间    // 中国使用的东八区的时区.比标准时间早8个小时
        System.out.println(LocalDateTime.now());//2022-09-02T21:35:25.589

        //2、带时区的计算机当前时间
        //创建世界标准时间！中国是东八区，比世界标准时间领先8小时
        ZonedDateTime dateTime = ZonedDateTime.now(Clock.systemUTC());
        System.out.println(dateTime);//2022-09-02T13:35:25.589Z

        //不带参数则直接输出本地时区时间
        // now(): 使用计算机的默认的时区,创建日期时间
        ZonedDateTime now1 = ZonedDateTime.now();
        System.out.println(now1);//2022-09-02T21:35:25.589+08:00[Asia/Shanghai]

        // 使用指定的时区创建日期时间    时区的ID目录通过下面这个语句中查找
        // ZoneId.getAvailableZoneIds().forEach(System.out::println);
        System.out.println(ZonedDateTime.now(ZoneId.of("Asia/Almaty")));//2022-09-02T19:42:11.381+06:00[Asia/Almaty]

    }
}
