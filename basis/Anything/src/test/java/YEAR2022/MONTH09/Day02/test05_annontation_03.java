package YEAR2022.MONTH09.Day02;


/**
 * @ProjectName: zhaowa_parent
 * @Package: YEAR2022.MONTH09.Day02
 * @ClassName: test05_annontation_03
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/3 0:36
 * @Version: 1.0
 */
public class test05_annontation_03 {
    private @AnyWhere int id=17;

    public void method(@AnyWhere String name,@AnyWhere Integer id){
        @AnyWhere Integer age;
    }

}
//TYPE_USE ：表示注解可以在任何用到类型的地方使用。
@interface AnyWhere{}
