package YEAR2022.MONTH09.Day02;

import org.junit.Test;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @ProjectName: zhaowa_parent
 * @Package: YEAR2022.MONTH09.Day02
 * @ClassName: test03_
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/2 23:32
 * @Version: 1.0
 */



//3、配置重复注解
@Myannotation("a")
@Myannotation("b")
@Myannotation("c")
public class test03_annotation {
    @Test
    @Myannotation("d")
    @Myannotation("e")
    public void method(){}

    public static void main(String[] args) throws Exception {
        // 4.解析重复注解
        // 获取注解在类上的重复注解
        // getAnnotationsByType是新增的API用户获取重复的注解
        Myannotation[] classAnnotations = test03_annotation.class.getAnnotationsByType(Myannotation.class);
        for (Myannotation myannotation : classAnnotations) {
            System.out.println(myannotation);
        }
        //获取注解在方法上的重复注解
        Myannotation[] methodAnnotations = test03_annotation.class.getMethod("method").getAnnotationsByType(Myannotation.class);
        for (Myannotation annotation : methodAnnotations) {
            System.out.println(annotation);
        }
//         运行结果如下
//        @YEAR2022.MONTH09.Day02.Myannotation(value=a)
//        @YEAR2022.MONTH09.Day02.Myannotation(value=b)
//        @YEAR2022.MONTH09.Day02.Myannotation(value=c)
//        @YEAR2022.MONTH09.Day02.Myannotation(value=d)
//        @YEAR2022.MONTH09.Day02.Myannotation(value=e)
    }

}
//1、定义重复的注解容器注解
@Retention(RetentionPolicy.RUNTIME)
@interface Myannotations{// 这是重复注解的容器
        Myannotation[] value();
}

//2、定义一个可以重复的注解
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(Myannotations.class)
@interface Myannotation{
    String value();
}