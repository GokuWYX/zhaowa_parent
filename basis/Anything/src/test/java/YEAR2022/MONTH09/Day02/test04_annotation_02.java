package YEAR2022.MONTH09.Day02;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * @ProjectName: zhaowa_parent
 * @Package: YEAR2022.MONTH09.Day02
 * @ClassName: test04_annotation_02
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/3 0:07
 * @Version: 1.0
 */
public class test04_annotation_02<@TypeParam T> {
    private  <@TypeParam E extends Integer> void test01(){}
}
//TYPE_PARAMETER ：表示该注解能写在类型参数的声明语句中。 类型参数声明如： <T> 、

@Target(ElementType.TYPE_PARAMETER)
@interface TypeParam{}



