package YEAR2022.MONTH09.Day02;

import java.util.Optional;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test09.test02
 * @ClassName: test01
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/2 9:07
 * @Version: 1.0
 */
public class test01_Optional {
    public static void main(String[] args) {
        //类的创建方式
        //Optional.of(T t) : 创建一个 Optional 实例
        Optional<Object> empty = Optional.empty();

        //Optional.empty() : 创建一个空的 Optional 实例
        Optional<String> isNotNull = Optional.of("我不是空");

        //Optional.ofNullable(T t):若 t 不为 null,创建 Optional 实例,否则创建空实例
        Optional<String> sss = Optional.ofNullable("sss");
        Optional<Object> bbb = Optional.ofNullable(null);

        //类的常用方法
        //isPresent() : 判断是否包含值,包含值返回true，不包含值返回false
        System.out.println(sss.isPresent());//true
        System.out.println(bbb.isPresent());//false

        //get() : 如果Optional有值则将其返回，否则抛出NoSuchElementException
        System.out.println(sss.get());//sss
        //bbb.get();//Exception in thread "main" java.util.NoSuchElementException: No value present

        //orElse(T t) : 如果调用对象包含值，返回该值，否则返回参数t
        System.out.println(sss.orElse("sss"));//sss
        System.out.println(bbb.orElse("null?"));//null?

        //orElseGet(Supplier s) :如果调用对象包含值，返回该值，否则返回 s 获取的值
        System.out.println(sss.orElseGet(() -> "ok"));//sss
        System.out.println(bbb.orElseGet(() -> "ok"));//ok

        //map(Function f): 如果有值对其处理，并返回处理后的Optional，否则返回 Optional.empty()
        System.out.println(sss.map((a) -> a + a));//Optional[ssssss]
        System.out.println(bbb.map((a) -> a.toString()));//Optional.empty


    }
}
