package YEAR2022.MONTH09.Day06;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @ProjectName: zhaowa_parent
 * @Package: YEAR2022.MONTH09.Day06
 * @ClassName: test01
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/6 8:24
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {
//        int[] ints={12,423,2435,55};
//        System.out.println(ints[3]);
//        ints[3]=1;
//        System.out.println(ints[3]);
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(43);
        arrayList.add(3435);
        arrayList.add(2435);
        arrayList.add(123);
        arrayList.add(122);
        arrayList.add(44);
//        Collections.sort(arrayList);
//        arrayList.forEach(System.out::print);
//        Collections.reverse(arrayList);
//        System.out.println();
//        arrayList.forEach(System.out::print);
        arrayList.stream().sorted().forEach(System.out::println);
        System.out.println("---------------------------------");
        arrayList.stream().map(a->a.doubleValue()).forEach(System.out::println);
    }


}
