package YEAR2022.MONTH09.Day01;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test09.test01
 * @ClassName: test01
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/2 9:09
 * @Version: 1.0
 */
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;
import java.util.stream.IntStream;

public class test01 {
    public test01() {
    }


    public static void main(String[] args) {




        List<Integer> result = new ArrayList();
        Object lock = new Object();
        IntStream.rangeClosed(1, 1000).parallel().forEach((a) -> {
            synchronized(lock) {
                result.add(a);
            }
        });
        System.out.println(result.size());
    }
}

class SumRecursiveTask extends RecursiveTask<Long> {
    private static final long THRESHOLD = 3000L;
    private final long start;
    private final long end;

    public SumRecursiveTask(long start, long end) {
        this.start = start;
        this.end = end;
    }

    protected Long compute() {
        long length = this.end - this.start;
        long sum;
        if (length >= 3000L) {
            sum = (this.start + this.end) / 2L;
            SumRecursiveTask left = new SumRecursiveTask(this.start, sum);
            left.fork();
            SumRecursiveTask right = new SumRecursiveTask(sum + 1L, this.end);
            right.fork();
            return (Long)left.join() + (Long)right.join();
        } else {
            sum = 0L;

            for(long i = this.start; i <= this.end; ++i) {
                sum += i;
            }

            return sum;
        }
    }
}
