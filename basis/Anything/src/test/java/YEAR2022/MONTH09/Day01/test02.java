package YEAR2022.MONTH09.Day01;

import java.util.List;
import java.util.Vector;
import java.util.stream.IntStream;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test09.test01
 * @ClassName: test02
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/2 9:09
 * @Version: 1.0
 */

public class test02 {

    public static void main(String[] args) {
        List<Integer> result = new Vector();
        IntStream.rangeClosed(1, 1000).parallel().forEach((a) -> {
            result.add(a);
        });
        System.out.println(result.size());
    }
}
