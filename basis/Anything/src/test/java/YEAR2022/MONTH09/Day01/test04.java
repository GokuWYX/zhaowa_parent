package YEAR2022.MONTH09.Day01;

import java.util.concurrent.ForkJoinPool;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test09.test01
 * @ClassName: test04
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/2 9:11
 * @Version: 1.0
 */
public class test04 {
    public test04() {
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        ForkJoinPool pool = new ForkJoinPool();
        SumRecursiveTask task = new SumRecursiveTask(1L, 99999999999L);
        Long result = (Long)pool.invoke(task);
        System.out.println("result = " + result);
        long end = System.currentTimeMillis();
        System.out.println("消耗时间: " + (end - start));
        long l = System.currentTimeMillis();
        long sum = 0L;

        long i;
        for(i = 1L; i <= 99999999999L; ++i) {
            sum += i;
        }

        i = System.currentTimeMillis();
        System.out.println("result:" + sum);
        long l1 = i - l;
        System.out.println("消耗时间:" + l1);
    }
}
