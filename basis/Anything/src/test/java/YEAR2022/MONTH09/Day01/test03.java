package YEAR2022.MONTH09.Day01;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test2022.test09.test01
 * @ClassName: test03
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/2 9:11
 * @Version: 1.0
 */
public class test03 {
    public test03() {
    }

    public static void main(String[] args) {
        System.out.println(((List) IntStream.rangeClosed(1, 1000).parallel().boxed().collect(Collectors.toList())).size());
        System.out.println(IntStream.rangeClosed(1, 1000).parallel().boxed().toArray().length);
    }
}
