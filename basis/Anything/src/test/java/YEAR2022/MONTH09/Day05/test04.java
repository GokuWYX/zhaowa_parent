package YEAR2022.MONTH09.Day05;

/**
 * @ProjectName: zhaowa_parent
 * @Package: YEAR2022.MONTH09.Day05
 * @ClassName: test04
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/5 15:20
 * @Version: 1.0
 */
public class test04 {
    public static void main(String[] args) {
        Sheeper sheeper = new Sheeper();
        sheeper.id=21;
        String[] hobb={"eat","do"};
        sheeper.hobbys=hobb;

        Sheeper cloneSheeper = null;
        try {
            cloneSheeper = sheeper.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        System.out.println(cloneSheeper.id==sheeper.id);

        System.out.println(sheeper.hobbys);
        System.out.println(cloneSheeper.hobbys);


    }
}
class Sheeper implements Cloneable{
    int id=1;
    String[] hobbys;
    @Override
    protected Sheeper clone() throws CloneNotSupportedException {

            return (Sheeper)super.clone();
    }

}
