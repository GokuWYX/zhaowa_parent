package YEAR2022.MONTH09.Day05;

import lombok.Getter;
import lombok.Setter;

/**
 * @ProjectName: zhaowa_parent
 * @Package: YEAR2022.MONTH09.Day05
 * @ClassName: test01
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/5 8:53
 * @Version: 1.0
 */
public class test01 {

    public static void main(String[] args) {
//        for (int i = 0; i < 100; i++) {
//            System.out.println();
//            return;
//        }
//        System.out.println(AOTE.age);
//
//        System.out.println("循环结束了");

        System.out.println(AOTE.age);
        AOTE.moder();
        System.out.println(AOTE.age);

        AOTE aote = new AOTE();
        aote.a();
    }
}
class AOTE{
    static int age=13;

    static void moder(){
        age=age-1;
    }

    void a(){
        System.out.println(age);
    }

    static void test(int a){return ;}

    void test(){}

    int test(String a){return 1;}

}
