package YEAR2022.MONTH09.Day28;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-09-28 19:24
 * @description: 线程池简历
 **/
public class test05 {
    //main方法，启动类

    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, 6, 10L, TimeUnit.SECONDS, new ArrayBlockingQueue<>(2), new ThreadPoolExecutor.AbortPolicy());
        for (int i = 0; i < 10; i++) {
            threadPoolExecutor.execute(()->System.out.println(Thread.currentThread().getName()));
        }
        try {

        }catch (Exception e){
        }finally {

        }
    }
}
