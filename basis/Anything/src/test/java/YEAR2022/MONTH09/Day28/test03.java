package YEAR2022.MONTH09.Day28;

import java.util.concurrent.Semaphore;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-09-28 10:10
 * @description:
 **/
public class test03 {
    //main方法，启动类
    public static void main(String[] args) throws Exception {
        Semaphore semaphore = new Semaphore(4);
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                try {
                    semaphore.acquire();
                    String name = Thread.currentThread().getName();
                    System.out.println(name+"获得semphore");
                    Thread.sleep(3000);
                    semaphore.release();
                    System.out.println(name+"扔掉semphore");
                }catch (Exception e){
                    e.printStackTrace();
                }
            },"第"+String.valueOf(i)+"线程").start();
        }
    }
}
