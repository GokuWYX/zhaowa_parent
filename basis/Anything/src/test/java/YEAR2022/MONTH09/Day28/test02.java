package YEAR2022.MONTH09.Day28;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-09-28 09:22
 * @description: Cylibarrier
 **/
public class test02 {
    //main方法，启动类
    public static void main(String[] args) throws Exception {
        CyclicBarrier barrier = new CyclicBarrier(17,()->{
            System.out.println("向下执行的任务！！！！");
        });



        for (int i = 0; i < 6; i++) {

            new Thread(()->{
                System.out.println(Thread.currentThread().getName());

                try {
                    barrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }

            },"第"+String.valueOf(i)+"线程").start();

        }

        barrier.await(5,TimeUnit.SECONDS);






    }
}
