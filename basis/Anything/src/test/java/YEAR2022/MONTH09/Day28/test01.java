package YEAR2022.MONTH09.Day28;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-09-28 08:18
 * @description:
 **/
public class test01 {
    //main方法，启动类
    public static void main(String[] args) throws Exception {
        CountDownLatch count = new CountDownLatch(8);
        for (int i = 0; i <= 7; i++) {

            new Thread(()->{
                System.out.println(Thread.currentThread().getName());
                count.countDown();
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },String.valueOf(i)).start();

        }

        //线程削减之后可以向下执行
        count.await();
        System.out.println("线程数削减为0");

    }
}
