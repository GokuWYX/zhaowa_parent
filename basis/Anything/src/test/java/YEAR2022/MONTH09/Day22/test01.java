package YEAR2022.MONTH09.Day22;

public class test01 {
    private volatile static test01 unique;

    private static test01 getUnique(){
        if (unique==null){
            synchronized (test01.class){
                if (unique==null){
                    unique=new test01();
                }
            }
        }
        return unique;
    }
}
