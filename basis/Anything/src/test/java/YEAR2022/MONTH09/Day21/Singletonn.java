package YEAR2022.MONTH09.Day21;

public class Singletonn {

    private static volatile Singletonn uniqueSingleton;

    private static Singletonn getUniqueSingleton(){
        if (uniqueSingleton==null){
            synchronized (Singletonn.class){
                if (uniqueSingleton==null){
                    uniqueSingleton=new Singletonn();
                }
            }
        }
        return uniqueSingleton;
    }
}
