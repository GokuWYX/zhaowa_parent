package YEAR2022.MONTH09.Day21;

public class Singleton {
    private volatile static Singleton uniqueInstance;

    private static Singleton getUniqueInstance(){
        if (uniqueInstance==null) {
            synchronized (Singleton.class){
                if (uniqueInstance==null){
                    uniqueInstance=new Singleton();
                }
            }
        }
        return uniqueInstance;
    }

}
