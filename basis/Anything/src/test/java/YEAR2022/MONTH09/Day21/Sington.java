package YEAR2022.MONTH09.Day21;

public class Sington {
    private volatile static Sington sington;
    private static Sington getSington(){
        if (sington==null){
            synchronized (Sington.class){
                if (sington==null){
                    sington=new Sington();
                }
            }
        }
        return sington;
    }
}
