package YEAR2022.MONTH09.Day09;

/**
 * @ProjectName: zhaowa_parent
 * @Package: YEAR2022.MONTH09.Day09
 * @ClassName: test01
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/9 8:38
 * @Version: 1.0
 */
public class Singleton {
    private volatile  static Singleton uniqueInstance;
    private Singleton(){}

    public static Singleton getUniqueInstance(){
        if (uniqueInstance==null){
            synchronized (Singleton.class){
                if (uniqueInstance==null){
                    uniqueInstance=new Singleton();
                }
            }
        }
        return uniqueInstance;
    }

}
