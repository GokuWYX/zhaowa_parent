package YEAR2022.MONTH09.Day26;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

public class test01 {

    public static void main(String[] args) {

        new Thread(()->Thread.currentThread().getName()).start();
        ExecutorService service = new ExecutorService() {
            @Override
            public void execute(Runnable command) {

            }

            @Override
            public void shutdown() {

            }

            @Override
            public List<Runnable> shutdownNow() {
                return null;
            }

            @Override
            public boolean isShutdown() {
                return false;
            }

            @Override
            public boolean isTerminated() {
                return false;
            }

            @Override
            public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
                return false;
            }

            @Override
            public <T> Future<T> submit(Callable<T> task) {
                return null;
            }

            @Override
            public <T> Future<T> submit(Runnable task, T result) {
                return null;
            }

            @Override
            public Future<?> submit(Runnable task) {
                return null;
            }

            @Override
            public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
                return null;
            }

            @Override
            public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException {
                return null;
            }

            @Override
            public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
                return null;
            }

            @Override
            public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
                return null;
            }
        };

        Future<String> submit = service.submit(new Thread02());
        String s = null;
        try {
            s = submit.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println(s);
    }
}

class Thread01 implements Runnable{
    @Override
    public void run() {
        System.out.println("ruuable");
    }
}
class Thread02 implements Callable<String>{

    @Override
    public String call() throws Exception {
        return "我是奥特曼";
    }
}

class Thread03 extends Thread{
    @Override
    public void run() {

        System.out.println("i am bat man");
    }
}
