package YEAR2022.MONTH09.Day23;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolExecutorDemo {
    private static final int CORE_POOL_SIZE = 5;
    private static final int MAX_POOL_SIZE = 10;
    private static final int QUEUE_CAPACITY = 100;
    private static final Long KEEP_ALIVE_TIME = 1L;

    public static void main(String[] args) {
        //使用阿里巴巴推荐的创建线程池的方式
        // 通过ThreadPoolExecutor构造函数自定义参数创建
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                CORE_POOL_SIZE, MAX_POOL_SIZE,
                KEEP_ALIVE_TIME,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(QUEUE_CAPACITY),
                new ThreadPoolExecutor.CallerRunsPolicy());

//        可以看到我们上面的代码指定了：
//        1. corePoolSize:
//        核心线程数为 5。
//        2. maximumPoolSize ：最大线程数 10
//        3. keepAliveTime:等待时间为 1L。
//        4. unit:等待时间的单位为 TimeUnit.SECONDS。5. workQueue ：任务队列为 ArrayBlockingQueue ，并且容量为 100;
//        6. handler:饱和策略为 CallerRunsPolicy 。
//        Output：
//        线程池原理分析
//        承接 4.6 节，我们通过代码输出结果可以看出：线程池首先会先执行 5 个任务，然后这些任务有任务被
//        执行完的话，就会去拿新的任务执行。大家可以先通过上面讲解的内容，分析一下到底是咋回事？（自己独立思考一会）
//        现在，我们就分析上面的输出内容来简单分析一下线程池原理。

        for (int i = 0; i < 10; i++) {
            //创建 MyRunnable 对象（MyRunnable 类实现了Runnable 接口）
            Runnable worker = new MyRunnable("" + i);
            // /执行Runnable
            executor.execute(worker);
        }
            // 终止线程池
            executor.shutdown(); while (!executor.isTerminated()) { }
            System.out.println("Finished all threads"); }
}
