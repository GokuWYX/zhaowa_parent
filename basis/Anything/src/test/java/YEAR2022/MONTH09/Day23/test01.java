package YEAR2022.MONTH09.Day23;


public class test01 {
    public static void main(String[] args) {

        new Thread(() -> {
            ThreadLocal<Object> threadLocalMain = new ThreadLocal<>();

            threadLocalMain.set("我牛逼");
            System.out.println(threadLocalMain.get());
            try {
                Thread.sleep(9000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("现场1借宿");
        }).start();

        new Thread(()->{
            ThreadLocal<Object> threadLocalMain = new ThreadLocal<>();
            ThreadLocal<Object> objectThreadLocal = new ThreadLocal<>();
            threadLocalMain.set("我牛逼吗？");
            System.out.println(threadLocalMain.get()+threadLocalMain.getClass().getName());
            objectThreadLocal.set("我当然牛逼");
            System.out.println(objectThreadLocal.get()+objectThreadLocal.getClass().getName());
        }).start();




    }
}
