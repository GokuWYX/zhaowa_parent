package com.wyx.controller;

import com.wyx.pojo.User;
import com.wyx.result.Result;
import com.wyx.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private IUserService service;

    @RequestMapping("/user")
    public String userTest(){
        return service.userTest();
    }

    @RequestMapping("/findAll")
    public List<User> findAll(){
        return service.findAll();
    }

    @RequestMapping("/mutiInsert")
    public Result<ArrayList<User>>  mutiInsert(){

        return service.getAndCompare();

    }

}
