package com.wyx.controller;

import com.wyx.service.IIDCardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.controller
 * @ClassName: TestController
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/15 22:07
 * @Version: 1.0
 */
@RestController
public class IDCardController {
    @Autowired
    private IIDCardService service;


    @RequestMapping("/write")
    public String write(){
        service.insertID();

        return "ok";
    }

}
