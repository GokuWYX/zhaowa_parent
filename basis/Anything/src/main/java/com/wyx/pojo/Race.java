package com.wyx.pojo;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.pojo
 * @ClassName: Race
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/15 17:24
 * @Version: 1.0
 */
public class Race {
    public static String HAN="汉族";
    public static String HUI="回族";
    public static String DAI="傣族";
    public static String BAI="白族";
}
