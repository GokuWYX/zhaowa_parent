package com.wyx.pojo.contract;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-12-20 14:48
 * @description: 用于合同-->分页查询
 **/
@Data
public class ContractListResp {

    /**
     * 表主键
     */
    @TableId(value = "id",type = IdType.ASSIGN_UUID)
    private Integer id;

    /**
     * 合同名称
     */
    @NotBlank
    private String contractName;

    /**
     * 合同编号
     */
    @NotBlank
    private String contractCode;

    /**
     *所属单位
     */
    @NotBlank
    private String affiliated;

    /**
     * 项目名称
     */
    @NotBlank
    private String projectName;

    /**
     * 合同金额
     */
    @NotBlank
    private BigDecimal contractAmount;


    /**
     * 合同状态
     */
    private Integer contractStatus;

}




