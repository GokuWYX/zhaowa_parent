package com.wyx.pojo.contract;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: Gogoku
 * @create: 2022-12-12 15:14
 * @description: 合同实体类
 **/
@Data
@Accessors(chain = true)
@TableName("contract")
public class Contract extends BaseEntity implements Serializable {

    /**
     * 表主键
     */
    @TableId(value = "id",type = IdType.ASSIGN_UUID)
    private Integer id;

    /**
     * 合同名称
     */
    @NotBlank
    private String contractName;

    /**
     * 合同编号
     */
    @NotBlank
    private String contractCode;

    /**
     *所属单位
      */
    @NotBlank
    private String affiliated;

    /**
     * 项目名称
     */
    @NotBlank
    private String projectName;

    /**
     * 合同金额
     */
    @NotBlank
    private BigDecimal contractAmount;

    /**
     * 合同产品明细总金额
     */
    @NotBlank
    private BigDecimal allAmount;

    /**
     * 买方
     */
    @NotBlank
    private String buyer;

    /**
     * 卖方
     */
    @NotBlank
    private String seller;

    /**
     * 录入人
     */
    @NotBlank
    private String record;

    /**
     * 录入日期
     */
    @NotBlank
    private Date recordDate;

    /**
     * 备注
     */
    private String remark;


    //自己输入

    /**
     * 合同状态
     */
    private Integer contractStatus;
    /**
     * 逻辑删除
     */
    private Integer status;

}








