package com.wyx.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wyx.pojo.contract.BaseEntity;
import lombok.Data;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-10-26 15:34
 * @description:
 **/
@Data
@TableName("dict")
public class Dict extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @TableField("parent_id")
    private Long parentId;

    @TableField("name")
    private String name;

    @TableField("value")
    private String value;

    @TableField("dict_code")
    private String dictCode;

    @TableField(exist = false)
    private boolean hasChildren;

}














