package com.wyx.pojo.contract;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-12-20 15:07
 * @description: 合同产品明细
 **/
@Data
@Accessors(chain = true)
@TableName("contract")
public class ContractProduct  extends BaseEntity{
    /**
     * 表主键
     */
    @TableId(value = "id",type = IdType.ASSIGN_UUID)
    private Integer id;

    /**
     * 物资编码，其实是物资类型编码
     */
    private String typeCode;

    /**
     * 数量
     */
    private Integer nums;


    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 单价
     */
    private BigDecimal price;


    /**
     * 逻辑删除
     */
    private Integer status;
}
