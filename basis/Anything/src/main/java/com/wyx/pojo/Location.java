package com.wyx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.pojo
 * @ClassName: Location
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/15 17:00
 * @Version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Location {
    private String province;
    private String city;
    private String fullAddress;


}
