package com.wyx.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.logging.SimpleFormatter;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.pojo
 * @ClassName: IDCard
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/15 16:54
 * @Version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IDCard {
    private Long Id;
    private String name;
    //true是男的，false是女的
    private String sex;
    //民族
    private String race;
    private String address;
    //生日
    private Timestamp birthday;
    //开始有效日期
    private Timestamp termValidityStart;
    //截止有效日期
    private Timestamp termValidityEnd;
    //籍贯
    private String nativePlace;
    //签发部门
    private String stateOrgans;
}
