package com.wyx.pojo.contract;


import com.wyx.pojo.ProductReq;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * @author: Gogoku
 * @create: 2022-12-12 15:14
 * @description: 合同实体类
 **/
@Data
public class ContractReq extends Contract implements Serializable {

    /**
     * 附件
     */
    private List<AttachmentReq> attachments;

    /**
     * 合同产品明细表
     */
    private List<ProductReq> products;

}
