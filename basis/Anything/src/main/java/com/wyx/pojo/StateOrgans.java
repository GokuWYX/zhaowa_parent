package com.wyx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.pojo
 * @ClassName: City
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/15 17:17
 * @Version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StateOrgans {
    private String name;
}
