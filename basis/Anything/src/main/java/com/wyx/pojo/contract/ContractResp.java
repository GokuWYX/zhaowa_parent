package com.wyx.pojo.contract;

import com.wyx.pojo.ProductReq;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: Gogoku
 * @create: 2022-12-13 09:27
 * @description: 用于合同-->查看
 **/
@Data
public class ContractResp implements Serializable {

    /**
     * 附件
     */
    private List<AttachmentReq> attachments;

    /**
     * 合同产品明细表
     */
    private List<ProductReq> products;

}
