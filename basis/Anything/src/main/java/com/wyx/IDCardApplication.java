package com.wyx;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx
 * @ClassName: TestApplication
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/15 21:52
 * @Version: 1.0
 */
@SpringBootApplication
public class IDCardApplication {


    public static void main(String[] args) {
        SpringApplication.run(IDCardApplication.class,args);

    }

}
