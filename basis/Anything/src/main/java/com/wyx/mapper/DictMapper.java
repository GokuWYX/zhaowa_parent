package com.wyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyx.pojo.Dict;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DictMapper extends BaseMapper<Dict> {

}