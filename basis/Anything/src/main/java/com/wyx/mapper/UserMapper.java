package com.wyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyx.pojo.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper extends BaseMapper<User> {
    List<User> findAll();

}