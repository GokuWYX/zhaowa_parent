package com.wyx.mapper;

import com.wyx.pojo.IDCard;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.mapper
 * @ClassName: TestMapper
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/15 22:09
 * @Version: 1.0
 */

@Mapper
@Repository
public interface IDCardMapper {
    void write(IDCard idCard);
}
