package com.wyx.service;

import com.wyx.pojo.IDCard;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.service
 * @ClassName: TestService
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/15 22:08
 * @Version: 1.0
 */
public interface IIDCardService {
    void insertID();
}
