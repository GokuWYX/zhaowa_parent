package com.wyx.service.impl;

import com.wyx.mapper.IDCardMapper;
import com.wyx.pojo.*;
import com.wyx.service.IIDCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.service.impl
 * @ClassName: TestServiceImpl
 * @Author: Gogoku
 * @Description:
 * @Date: 2022/9/15 17:20
 * @Version: 1.0
 */
@Service
public class IDCardServiceImpl implements IIDCardService {
    @Autowired
    private IDCardMapper mapper;

    @Override
    public void insertID() {

        IDCard idCard = new IDCard();
        idCard.setId(new Long(130528199910146652L));
        idCard.setName("王宇翔");
        idCard.setSex("男");
        Location location = new Location("河北省", "邢台市", "宁晋县苏家庄乡北朱家庄村永盛南大街25号");
        idCard.setAddress(location.getProvince()+location.getCity()+location.getFullAddress());
        idCard.setNativePlace(location.getProvince()+location.getCity());
        //采用JDK8新特性创建时间类
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime birthDay = now.withYear(1999).withMonth(10).withDayOfMonth(10).withHour(18).withMinute(18).withSecond(18);
        Timestamp myBirthDay = Timestamp.valueOf(birthDay);
        idCard.setBirthday(myBirthDay);
        idCard.setRace(Race.HAN);
        LocalDateTime start = now.withYear(2020).withMonth(7).withDayOfMonth(23).withHour(0).withMinute(0).withSecond(0);
        Timestamp startTime = Timestamp.valueOf(start);
        LocalDateTime end = now.withYear(2030).withMonth(7).withDayOfMonth(23).withHour(0).withMinute(0).withSecond(0);
        Timestamp endTime = Timestamp.valueOf(end);
        idCard.setTermValidityStart(startTime);
        idCard.setTermValidityEnd(endTime);
        idCard.setStateOrgans(new StateOrgans("宁晋县公安局").getName());


        mapper.write(idCard);
    }



}
