package com.wyx.service;

import com.wyx.pojo.User;
import com.wyx.result.Result;

import java.util.ArrayList;
import java.util.List;

public interface IUserService {
    public String userTest();

    List<User> findAll();

    Result<ArrayList<User>>  getAndCompare();

}
