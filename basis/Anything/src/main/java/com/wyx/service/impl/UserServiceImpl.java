package com.wyx.service.impl;

import com.wyx.mapper.UserMapper;
import com.wyx.pojo.User;
import com.wyx.result.Result;
import com.wyx.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper mapper;

    //测试关于mp的各类基本功能
    @Override
    public String userTest() {
        mapper.insert(new User("张三",18,"adwadaw@11.com"));
        return null;
    }

    @Override
    public List<User> findAll() {
        return mapper.findAll();
    }

    @Override
    public Result<ArrayList<User>> getAndCompare() {

        ArrayList<User> list = new ArrayList<>();
        for (int i = 10; i > 1; i--) {
            User user = new User();
            user.setAge(i*2);
            list.add(user);
        }
        list.sort((u1,u2)->u1.getAge()-u2.getAge());
        try {
            list.forEach(a->mapper.insert(a));
        }catch (Exception e){
            log.error("插入：{}",e);
        }

        return Result.success(list);
    }


}
