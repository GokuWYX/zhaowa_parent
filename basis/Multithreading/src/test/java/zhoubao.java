import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

/**
 * @ProjectName: zhaowa_parent
 * @Package: PACKAGE_NAME
 * @ClassName: zhoubao
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/22 14:57
 * @Version: 1.0
 */
public class zhoubao {
    public static void main(String[] args) {
        maker();


    }

    public static void maker(){
        String location="河北省廊坊市固安县经济管理学院中国天然气管道通信电力工程有限公司";

        ArrayList<String> neirongBase = new ArrayList<>();
        ArrayList<String> neirong = new ArrayList<>();

        ArrayList<String> inner = innerWork(neirongBase);
        //inner.forEach((String a)-> System.out.println(a));

        for (int i = 0; i <30; i++) {
            StringBuffer a=new StringBuffer("");
            for (int c = 0; c < 5; c++) {
                if (c==4){
                    a.append((inner.get((int) (Math.random() * 56))+"。"));
                }else {
                    a.append((inner.get((int) (Math.random() * 56))+"，"));
                }
            }
            neirong.add(a.toString());
        }

        ArrayList<String> tihui = new ArrayList<>();
        for (int i = 0; i <30; i++) {
            StringBuffer b=new StringBuffer("");

            b.append("深入理解"+(inner.get((int) (Math.random() * 56))+"，"));
            b.append("掌握了"+(inner.get((int) (Math.random() * 56))+"，"));
            b.append("通过工作学会了"+(inner.get((int) (Math.random() * 56))+"，"));
            b.append("在前辈的指导下基本掌握了"+(inner.get((int) (Math.random() * 56))+"，"));
            b.append("工作中完成"+(inner.get((int) (Math.random() * 56))+"使我得到了解决问题的心得和体会。"));
            tihui.add(b.toString());
        }




        for (int i = 0; i <= neirong.size()-1; i++) {
            System.out.println("第"+(i+1)+"周");
            System.out.println();
            System.out.println("地点："+location);
            System.out.println();
            System.out.println("内容："+neirong.get(i));
            System.out.println();
            System.out.println("学习收获与体会："+tihui.get(i));
            System.out.println();
        }
    }
    public static ArrayList<String> innerWork(ArrayList<String> neirong){
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> work = new ArrayList<>();

        name.add("巡检系统");
        name.add("特种车辆系统");
        name.add("大数据平台");
        name.add("位置服务系统");
        name.add("北斗运营中心");
        name.add("终端数据系统");
        name.add("链路监控服务");

        work.add("的数据处理服务，topic数据到Hbase，topic数据转移到topic，数据传输");
        work.add("的大数据处理工作，Flink数据离线，Kfaka数据到Hbase，topic数据存储到redis");
        work.add("的多表集成操作，数据解析服务，数据封装接口");
        work.add("的人员信息的入库、级联查询、SQL编写、信息的定时更新");
        work.add("的终端信息的绑定、人员终端信息解析接口");
        work.add("的绑定信息的级联绑定、查询服务、级联获取");
        work.add("的测试工作");
        work.add("的运维工作");

        for (int i = 0; i < name.size(); i++) {
            for (int i1 = 0; i1 < work.size(); i1++) {
                neirong.add(name.get(i)+work.get(i1));
            }
        }
        neirong.add("链表的学习");
        neirong.add("数组的学习，模拟，api实现");
        neirong.add("Hbase底层原理深究，完成hbase基本demo");
        neirong.add("Spring源码探究，学习spring内部原理");
        neirong.add("算法分析，学习");

        return neirong;
    }

}
