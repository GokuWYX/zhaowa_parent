package com.wyx.test02_state;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test02_state
 * @ClassName: test05
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/15 8:31
 * @Version: 1.0
 */
public class test05 {
    public static void main(String[] args) throws InterruptedException {
        Thread thread=new Thread(()->{
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("////////");
        });
        Thread.State state = thread.getState();
        System.out.println(state);

        thread.start();
        state = thread.getState();
        System.out.println(state);
        while (state!=Thread.State.TERMINATED){
            Thread.sleep(100);
            state=thread.getState();
            System.out.println(state);
        }
    }
}
