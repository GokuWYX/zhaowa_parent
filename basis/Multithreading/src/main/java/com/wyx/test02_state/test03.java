package com.wyx.test02_state;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test02_state
 * @ClassName: test03
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/14 22:21
 * @Version: 1.0
 */
public class test03 {
    public static void main(String[] args) {
        MyYield myYield = new MyYield();
        new Thread(myYield,"t1").start();
        new Thread(myYield,"t2").start();
    }
}
class MyYield implements Runnable{

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"线程开始运行");
        Thread.yield();
        System.out.println(Thread.currentThread().getName()+"线程结束");
    }
}
