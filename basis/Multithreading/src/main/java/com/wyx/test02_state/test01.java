package com.wyx.test02_state;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test02_state
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/14 22:00
 * @Version: 1.0
 */
public class test01 implements Runnable {
    private boolean flag=true;
    @Override
    public void run() {
        int i=0;
        while (flag){

            System.out.println("线程在运行"+i++);
        }
    }
    public void stop(){
        this.flag=false;
    }

    public static void main(String[] args) {
        test01 test = new test01();
        new Thread(test).start();
        for (int i = 0; i < 1000; i++) {
            System.out.println("main"+i);
            if (i==900){
                test.stop();
                System.out.println("线程停止了");
            }
        }
    }
}
