package com.wyx.test02_state;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test02_state
 * @ClassName: test07
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/15 8:53
 * @Version: 1.0
 */
public class test07 {
    public static void main(String[] args) {
        Person person = new Person();
        God god = new God();
        Thread p = new Thread(person);
        Thread g = new Thread(god);
        g.setDaemon(true);
        p.start();
        g.start();
    }
}
class Person implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 365; i++) {
            System.out.println("人活着");
        }
        System.out.println("--------------------------人死了");
    }
}
class God implements Runnable{

    @Override
    public void run() {
        while (true){
            System.out.println("上帝 is alive" );
        }
    }
}