package com.wyx.test02_state;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test02_state
 * @ClassName: test04
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/14 22:27
 * @Version: 1.0
 */
public class test04 implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("线程vip来了"+i);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        test04 test04 = new test04();
        Thread a=new Thread(test04);
        a.start();
        for (int i = 0; i < 1000; i++) {
            if (i==200){
                a.join();
            }
            System.out.println(i+"main");

        }
    }
}
