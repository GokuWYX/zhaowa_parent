package com.wyx.test02_state;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test02_state
 * @ClassName: test06
 * @Author: GoKu
 * @Description: 线程优先级
 * @Date: 2022/4/15 8:39
 * @Version: 1.0
 */
public class test06 {
    public static void main(String[] args) {
        //主线程优先级5
        System.out.println(Thread.currentThread().getPriority());
        MyPriority myPriority = new MyPriority();
        //默认线程优先级5
        new Thread(myPriority,"a").start();

        Thread t1 = new Thread(myPriority);
        Thread t2 = new Thread(myPriority);
        Thread t3 = new Thread(myPriority);
        Thread t4 = new Thread(myPriority);
        Thread t5 = new Thread(myPriority);

        t1.setPriority(2);
        t2.setPriority(4);
        t3.setPriority(6);
        t4.setPriority(8);
        t5.setPriority(Thread.MAX_PRIORITY);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }
}
class MyPriority implements Runnable{
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getPriority()+Thread.currentThread().getName());
    }
}
