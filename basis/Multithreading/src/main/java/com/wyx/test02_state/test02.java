package com.wyx.test02_state;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test02_state
 * @ClassName: test02
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/14 22:12
 * @Version: 1.0
 */
public class test02  {
    public static void main(String[] args) {
        Date nowDate = new Date(System.currentTimeMillis());
        while (true){
            try {
                Thread.sleep(1000);
                System.out.println(new SimpleDateFormat("HH:mm:ss").format(nowDate));
                nowDate=new Date(System.currentTimeMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
