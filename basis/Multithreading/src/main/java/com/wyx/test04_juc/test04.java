package com.wyx.test04_juc;

import java.util.concurrent.TimeUnit;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test04
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/17 10:17
 * @Version: 1.0
 */
public class test04 {
    public static void main(String[] args) {
        Phone phone = new Phone();
        Phone phone1 = new Phone();
        new Thread(()-> phone.send()).start();
        new Thread(()->phone1.receive()).start();
    }
}
class Phone{
    public static synchronized void send() {
        try {
            TimeUnit.SECONDS.sleep(3);
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"发送");
    }
    public static synchronized void receive(){
        System.out.println(Thread.currentThread().getName()+"接受");
    }
}
