package com.wyx.test04_juc;

import java.util.concurrent.CountDownLatch;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test07
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/18 9:17
 * @Version: 1.0
 */
public class test07 {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(5);
        for (int i = 0; i < 3; i++) {
            new Thread(()->
            {
                countDownLatch.countDown();
                System.out.println(Thread.currentThread().getName()+"startt");
            }).start();
        }
        //等待计数器归零然后向下执行
        countDownLatch.await();
        System.out.println(Thread.currentThread().getName()+" End");
    }
}
interface a extends b,c{
    public void aa();
}
interface b{
    public void bb();
}
interface c{
    public void cc();

}
class aa implements a{

    @Override
    public void aa() {

    }

    @Override
    public void bb() {

    }

    @Override
    public void cc() {

    }
}