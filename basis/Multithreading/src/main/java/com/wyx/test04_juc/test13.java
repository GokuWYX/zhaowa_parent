package com.wyx.test04_juc;

import java.util.concurrent.*;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test13
 * @Author: GoKu
 * @Description: 线程池技术
 * @Date: 2022/4/19 10:52
 * @Version: 1.0
 */
public class test13 {
    public static void main(String[] args) {
        //单线程
        //test01();
        //多线程
        //test02();
        //缓冲弹性线程数量
        test03();
    }
    public static void test01(){
        ExecutorService threadPool= Executors.newSingleThreadExecutor();
        try {
            for (int i = 0; i < 11; i++) {
                threadPool.execute(()->{
                    System.out.println(Thread.currentThread().getName());
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            threadPool.shutdown();
        }
    }
    public static void test02(){
        ExecutorService threadPol=Executors.newFixedThreadPool(5);
        try {
            for (int i = 0; i < 11; i++) {
                threadPol.execute(()-> System.out.println(Thread.currentThread().getName()));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            threadPol.shutdown();
        }
    }
    public static void test03(){
        ExecutorService pool=Executors.newCachedThreadPool();
        try {
            for (int i = 0; i < 11; i++) {
                pool.execute(()-> System.out.println(Thread.currentThread().getName()));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            pool.shutdown();
        }
    }
    public static void test04(){
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2, 8, 3L, TimeUnit.SECONDS
                , new LinkedBlockingDeque<>(3), Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.DiscardPolicy());

    }
}
