package com.wyx.test04_juc;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test12
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/18 19:18
 * @Version: 1.0
 */
public class test12 {
    public static void main(String[] args) {
        SynchronousQueue<String> strings = new SynchronousQueue<>();
        new Thread(()->{
            try {
                System.out.println(Thread.currentThread().getName()+"1");
                strings.put("1");
                System.out.println(Thread.currentThread().getName()+"2");
                strings.put("2");
                System.out.println(Thread.currentThread().getName()+"3");
                strings.put("3");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(2);
                System.out.println(strings.take()+":"+Thread.currentThread().getName());
                TimeUnit.SECONDS.sleep(2);
                System.out.println(strings.take()+":"+Thread.currentThread().getName());
                TimeUnit.SECONDS.sleep(2);
                System.out.println(strings.take()+":"+Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();



    }
}
