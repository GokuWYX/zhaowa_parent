package com.wyx.test04_juc;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test24
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/3 21:42
 * @Version: 1.0
 */
public class test24 {
    public static void main(String[] args) {
        ArrayBlockingQueue<Object> objects = new ArrayBlockingQueue<>(2);
        new Thread(()->{
            objects.offer("1");
            objects.offer(1);
        }).start();

        new Thread(()->{

            try {
                System.out.println(objects.poll(1, TimeUnit.SECONDS));
                System.out.println(objects.poll(1, TimeUnit.SECONDS));
                System.out.println(objects.poll(1, TimeUnit.SECONDS));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }).start();

    }
}
