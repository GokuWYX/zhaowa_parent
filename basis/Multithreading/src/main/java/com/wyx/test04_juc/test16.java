package com.wyx.test04_juc;

import sun.rmi.runtime.Log;

import java.util.concurrent.*;
import java.util.stream.LongStream;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test16
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/20 19:22
 * @Version: 1.0
 */
public class test16  extends RecursiveTask<Long> {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ForkJoinTask<Long> test16 = new test16(0L, 1000L);
        ForkJoinPool forkJoinPool=new ForkJoinPool();
        System.out.println(forkJoinPool.submit(test16).get());
        long sum=0;
        for (long i = 0; i < 1000L; i++) {
            sum+=i;
        }
        System.out.println(sum);
        System.out.println(stream(0L, 1000L));
    }

    private Long start;
    private Long end;
    private static final Long critical=100L;
    public test16(Long start, Long end) {
        this.start = start;
        this.end = end;
    }

    @Override
    protected Long compute() {
        long lenth = end - start;
        if (lenth<=critical){
            Long sum=0L;
            for (Long i = start; i < end; i++) {
                sum+=i;
            }
            return sum;
        }else {
            long oneThree = (end + start) / 2;
            test16 r1 = new test16(start, oneThree);
            r1.fork();

            test16 r2 = new test16(oneThree + 1, end);
            r2.fork();

            return r1.join()+r2.join();
        }
    }
    public static Long stream(Long start,Long end){
        return LongStream.rangeClosed(start,end).parallel().reduce(0,Long::sum);
    }
}

















