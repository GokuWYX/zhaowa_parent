package com.wyx.test04_juc;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test11
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/18 16:49
 * @Version: 1.0
 */
public class test11 {
    public static void main(String[] args) throws InterruptedException {
        //test01();
        //test02();
        //test03();
        test04();
    }
    public static ArrayBlockingQueue arrayBlockingQueue = new ArrayBlockingQueue<>(3);
    public static void test01(){

        System.out.println(arrayBlockingQueue.add("1"));
        System.out.println(arrayBlockingQueue.add("2"));
        System.out.println(arrayBlockingQueue.add("3"));
        //System.out.println(arrayBlockingQueue.add("4"));
        for (int i = 0; i < 5; i++) {
            arrayBlockingQueue.remove();
        }
    }
    public static void test02(){
        for (int i = 0; i < 4; i++) {
            System.out.println(arrayBlockingQueue.offer(i));
        }
        for (int i = 0; i < 4; i++) {
            System.out.println(arrayBlockingQueue.poll());
        }
    }
    public static void test03() throws InterruptedException {

        for (int i = 0; i < 3; i++) {
            System.out.println("put"+i);
            arrayBlockingQueue.put(i);
            System.out.println(i+"成功");
        }
        for (int i = 0; i < 4; i++) {
            System.out.println("take"+i);

            arrayBlockingQueue.take();
            System.out.println("take"+i+"成功");
        }
    }
    public static void test04() throws InterruptedException {

        System.out.println(arrayBlockingQueue.offer("1")+"成功");
        System.out.println(arrayBlockingQueue.offer("2")+"成功");
        System.out.println(arrayBlockingQueue.offer("3")+"成功");
        //System.out.println(arrayBlockingQueue.offer("4",3, TimeUnit.SECONDS)+"成功");

        System.out.println(arrayBlockingQueue.poll());
        System.out.println(arrayBlockingQueue.poll());
        System.out.println(arrayBlockingQueue.poll());
        System.out.println(arrayBlockingQueue.poll(2, TimeUnit.SECONDS));


    }
}

