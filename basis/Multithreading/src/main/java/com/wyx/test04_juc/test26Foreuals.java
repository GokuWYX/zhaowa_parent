package com.wyx.test04_juc;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test26Foreuals
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/6 14:50
 * @Version: 1.0
 */
public class test26Foreuals {
    public static void main(String[] args) {
//        int a=1;
//        int b=1;
//        System.out.println(a==b);
//        User a1 = new User();
//        User a2 = new User();
//        System.out.println(a1==a2);
//        System.out.println(a1.getAge()==a2.getAge());
//        System.out.println(a1.equals(a2));
//        String s = String.valueOf(12);
//        Integer integer = new Integer(1);
//        int i = integer.intValue();
//        System.out.println(i);


        //定义初始值
        int a=12;
        //装箱
        Integer b=new Integer(a);
        //拆箱
        int c = b.intValue();
        //装箱
        String d = String.valueOf(c);

    }
}
