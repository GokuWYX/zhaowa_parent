package com.wyx.test04_juc;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test18
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/3 8:06
 * @Version: 1.0
 */
public class test18 {
    public static void main(String[] args) throws Exception {
        ExecutorService ser = Executors.newFixedThreadPool(1);
        System.out.println(ser.submit(new Thread02()).get());
        ser.shutdownNow();
    }
}
class Thread01 implements Runnable{

    @Override
    public void run() {
        System.out.println("实现了run");
    }
}
class Thread02 implements Callable<String>{

    @Override
    public String call() throws Exception {
        System.out.println("call内部");
        return "实现了call";
    }
}









