package com.wyx.test04_juc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test05
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/17 15:32
 * @Version: 1.0
 */
public class test05 {
    public static void main(String[] args) {
        aboutCopyOnWrite();
    }
    public static void aboutArrayList(){
        List<String> list = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            new Thread(()-> {list.add(UUID.randomUUID().toString().substring(0,8));
                //System.out.println(list);
                for (String s : list) {
                    System.out.println(s.toString());
                }},String.valueOf(i)).start();
        }
    }
    public static void aboutVetor(){
        List<String> list = new Vector<>();
        for (int i = 1; i < 11; i++) {
            new Thread(()->{list.add(UUID.randomUUID().toString().substring(9));
                //System.out.println(list);
                for (String s : list) {
                    System.out.println(s);
                }},String.valueOf(i)).start();
        }
    }
    public static void aboutCopyOnWrite(){
        List list = new CopyOnWriteArrayList<>();
        for (int i = 1; i < 11; i++) {
            new Thread(()->{list.add(UUID.randomUUID().toString().substring(9));
                //System.out.println(list);
                for (Object o : list) {
                    System.out.println(o);
                }
            },String.valueOf(i)).start();
        }
    }
}
