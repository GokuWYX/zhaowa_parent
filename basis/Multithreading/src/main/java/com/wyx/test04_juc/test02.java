package com.wyx.test04_juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/15 22:24
 * @Version: 1.0
 */
public class test02 {
    public static void main(String[] args) {
        Data02 data = new Data02();
        new Thread(()->{ for (int i = 0; i < 10; i++) {
            try {
                data.increase();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } },"Producer").start();

        new Thread(()->{ for (int i = 0; i < 10; i++) {
            try {
                data.decrease();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } },"Consumer").start();
        new Thread(()->{ for (int i = 0; i < 10; i++) {
            try {
                data.increase();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } },"Producer1").start();

        new Thread(()->{ for (int i = 0; i < 10; i++) {
            try {
                data.decrease();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } },"Consumer1").start();
    }
}

class Data02{
    private int sum=0;
    Lock lock=new ReentrantLock();
    Condition condition = lock.newCondition();

    public void decrease() throws Exception {
        lock.lock();
        try {
            while (sum==0){
                condition.await();
            }
            sum--;
            System.out.println(Thread.currentThread().getName()+"现在"+sum);
            condition.signalAll();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
    public void increase() throws Exception{
        lock.lock();
        try {
            while (sum!=0){
                condition.await();
            }
            sum++;
            System.out.println(Thread.currentThread().getName()+"现在"+sum);
            condition.signalAll();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }

    }
}
