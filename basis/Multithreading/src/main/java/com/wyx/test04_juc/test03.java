package com.wyx.test04_juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test03
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/17 1:27
 * @Version: 1.0
 */
public class test03 {
    public static void main(String[] args) {
        Data03 data = new Data03();
        new Thread(()->{
            for (int i = 0; i <5 ; i++) {
                data.testA();
            }
        },"线程1").start();
        new Thread(()->{
            for (int i = 0; i <5 ; i++) {
                data.testB();
            }
        },"线程2").start();
        new Thread(()->{
            for (int i = 0; i <1 ; i++) {
                data.testC();
            }
        },"线程3").start();
    }
}
class Data03{
    private Lock lock=new ReentrantLock();
    private Condition condition01 = lock.newCondition();
    private Condition condition02 = lock.newCondition();
    private Condition condition03 = lock.newCondition();
    private int num=1;
    public void testA(){
        lock.lock();
        try {
            while (num!=1){
                condition01.await();
            }
            System.out.println(Thread.currentThread().getName());
            num=2;
            condition02.signal();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
    public void testB(){
        lock.lock();
        try {
            while (num!=2){
                condition02.await();
            }
            System.out.println(Thread.currentThread().getName());
            num=3;
            condition03.signal();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
    public void testC(){
        lock.lock();
        try {
            while (num!=3){
                condition03.await();
            }
            System.out.println(Thread.currentThread().getName());
            num=1;
            condition01.signal();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }

    }



}
