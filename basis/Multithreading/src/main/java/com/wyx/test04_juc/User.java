package com.wyx.test04_juc;

import java.util.Objects;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: User
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/20 10:26
 * @Version: 1.0
 */

public class User {
    public static void main(String[] args) {
        User user1 = new User(12);
        User user2 = new User(13);
        swap(user1,user2);
        System.out.println("user01: "+user1.getId());
        System.out.println("user02: "+user2.getId());

        int id = user1.getId();
        user1.setId(user2.getId());
        user2.setId(id);
        System.out.println("user01: "+user1.getId());
        System.out.println("user02: "+user2.getId());
    }

    public User(int id) {
        this.id = id;
    }
    public static void swap(User a,User b){
        User tmp=a;
        a=b;
        b=tmp;
        System.out.println("user01: "+a.getId());
        System.out.println("user02: "+b.getId());
    }

    private int id;
    private String userName;
    private int age;
//get、set、有参/无参构造器、toString


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                age == user.age &&
                Objects.equals(userName, user.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName, age);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", age=" + age +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public User() {
    }

    public User(int id, String userName, int age) {
        this.id = id;
        this.userName = userName;
        this.age = age;
    }
}
