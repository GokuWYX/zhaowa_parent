package com.wyx.test04_juc;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test15
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/20 10:27
 * @Version: 1.0
 */
public class test15 {
    public static void main(String[] args) {
        User a = new User(11, "a", 23);
        User b = new User(12, "b", 25);
        User c = new User(13, "c", 26);
        User d = new User(14, "d", 27);
        User e = new User(15, "e", 28);
        List<User> list= Arrays.asList(a,b,c,d,e);
        list.stream()
                .filter(user -> {return user.getId()>=12;})
                .filter(user -> {return user.getAge()<=27;})
                .map(user -> {return user.getUserName().toUpperCase();})
                .sorted(((o1, o2) -> {return o2.compareTo(o1);}))
                .limit(3)
                .forEach(System.out::println);
    }
}
