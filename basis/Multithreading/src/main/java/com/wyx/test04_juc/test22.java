package com.wyx.test04_juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test22
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/3 19:40
 * @Version: 1.0
 */
public class test22 {
    public static void main(String[] args) throws Exception{
        Dataa dataa = new Dataa();
        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    dataa.increase();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    dataa.decrase();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    dataa.increase();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    dataa.decrase();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();

    }
}
class Dataa{
    private int num=0;
    private Lock lock=new ReentrantLock();
    private Condition condition = lock.newCondition();
    public void decrase() throws Exception{
        lock.lock();
        try {
            while (num==0){
                condition.await();
            }
            num--;
            System.out.println(Thread.currentThread().getName()+"\t"+num);
            condition.signalAll();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
    public void increase(){
        lock.lock();
        try {
            while (num!=0){
            condition.await();
            }
            //产一个用一个
            num++;
            System.out.println(Thread.currentThread().getName()+"\t"+num);
            condition.signalAll();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

}
