package com.wyx.test04_juc;

import org.testng.annotations.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test25
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/3 21:52
 * @Version: 1.0
 */
public class test25 {

    @Test
    public static void test01(){
        ExecutorService pool = Executors.newCachedThreadPool();
        try {
            for (int i = 0; i < 100; i++) {
                pool.execute(()->{
                    System.out.println(Thread.currentThread().getName()+"doing");
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            pool.shutdownNow();
        }

    }

}
