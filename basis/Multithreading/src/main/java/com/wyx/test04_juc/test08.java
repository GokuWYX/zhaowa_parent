package com.wyx.test04_juc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test08
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/18 9:24
 * @Version: 1.0
 */
public class test08 {
    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(10, () -> System.out.println("10个-------------"));
        for (int i = 0; i < 13; i++) {
            new Thread(()-> {System.out.println(Thread.currentThread().getName());         try {
                cyclicBarrier.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }},String.valueOf(i)).start();

        }


    }
}
