package com.wyx.test04_juc;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test19
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/3 17:24
 * @Version: 1.0
 */
public class test19 {
//    public static void main(String[] args) {
//        Function<Integer,String> function=s->{return s.toString();};
//        System.out.println(function.apply(12));
//
//        Supplier<String> stringSupplier=()->{return "string";};
//        System.out.println(stringSupplier.get());
//
//        Consumer<Integer> stringConsumer=a->{
//            System.out.println(a);
//        };
//        int a=13;
//        stringConsumer.accept(a);
//
//        Predicate<Integer> aa=b->{return b.compareTo(12)>0;};
//        System.out.println(aa.test(a));
//    }
public static void main(String[] args) {
    A a= a1 -> {
        return a1;
    };
    System.out.println(a.getA("A"));


    B b=(a1, b1) -> {
        System.out.println(a1);
        System.out.println(b1);
    };
    b.soutTwo("a","b");

}
}
interface A{
    String getA(String a);
}
interface B{
    void soutTwo(String a,String b);
}











