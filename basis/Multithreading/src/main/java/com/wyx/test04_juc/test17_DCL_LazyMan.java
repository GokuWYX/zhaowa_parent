package com.wyx.test04_juc;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test17_DCL_LazyMan
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/22 8:57
 * @Version: 1.0
 */
public class test17_DCL_LazyMan {
    public test17_DCL_LazyMan() {
    }
    private static test17_DCL_LazyMan lazyMan;

    public static test17_DCL_LazyMan instance(){
        if (lazyMan==null){
            synchronized (test17_DCL_LazyMan.class){
                if (lazyMan==null){
                    lazyMan=new test17_DCL_LazyMan();
                }
            }

        }
        return lazyMan;
    }
}
