package com.wyx.test04_juc;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test23BlockingQueue
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/3 21:28
 * @Version: 1.0
 */
public class test23BlockingQueue {
    public static void main(String[] args) throws InterruptedException {
        ArrayBlockingQueue<Object> objects = new ArrayBlockingQueue<>(2);
        new Thread(()->{
            System.out.println(objects.add("2"));
            System.out.println(objects.add("1"));
//        System.out.println(objects.add("1"));
//        System.out.println(objects.offer("1"));
            System.out.println(objects.element());
            try {
                objects.put("ds");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }).start();
        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(objects.remove());
        }).start();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("----poll------");
        System.out.println(objects.poll());
        //System.out.println(objects.poll());
/*        System.out.println(objects.poll());*/
  //      System.out.println("-----takr---");
/*        System.out.println(objects.take());
        System.out.println(objects.take());*/
        objects.poll();

    }


}
