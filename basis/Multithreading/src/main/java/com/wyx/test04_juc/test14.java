package com.wyx.test04_juc;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test14
 * @Author: GoKu
 * @Description: 函数式接口
 * @Date: 2022/4/20 9:02
 * @Version: 1.0
 */
public class test14 {
    public static void main(String[] args) {
        Function<String,Integer> a=c->{return c.length();};
        System.out.println(a.apply("rthrfthrthrthrthrtyhttyha"));
        Predicate<Integer> predicate= pp->{return pp>0;};
        System.out.println(predicate.test(1241241));
        Consumer<String> consumer=(cc)->{
            System.out.println(cc);
        };
        consumer.accept("cc");
        Supplier<String> maker=()->{return "你是来";};
        System.out.println(maker.get());
    }
}
