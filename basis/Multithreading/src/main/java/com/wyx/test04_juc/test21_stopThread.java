package com.wyx.test04_juc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test21_stopThread
 * @Author: GoKu
 * @Description:
 * @Date: 2022/5/3 18:09
 * @Version: 1.0
 */
public class test21_stopThread {
    public static void main(String[] args) {
        tst tst = new tst();
        new Thread(tst).start();
        try {
            Thread.sleep(12);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        tst.stop();

    }
}
class tst implements Runnable{

    private boolean flag=true;
    @Override
    public void run() {
        while (flag){
            System.out.println("run");
        }
        System.out.println("stop");
    }
    public void stop(){
        flag=false;
    }
}
