package com.wyx.test04_juc;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test10
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/18 14:22
 * @Version: 1.0
 */
public class test10 {
    public static void main(String[] args) {

        CacheClass cache = new CacheClass();
        for (int i = 0; i < 15; i++) {
            final String key=String.valueOf(i);
            final String value=i+"的結果";
            new Thread(()->cache.put(key,value),key).start();
        }
        System.out.println("--------------------------");
        for (int i = 0; i < 15; i++) {
            final String key=String.valueOf(i);
            new Thread(()->cache.get(key),String.valueOf(i)).start();
        }


    }
}
class CacheClass{
    private volatile Map<String,String> cache=new HashMap<>();
    private ReadWriteLock readWriteLock=new ReentrantReadWriteLock();
    public void put(String key,String value){
        readWriteLock.writeLock().lock();
        System.out.println(Thread.currentThread().getName()+"  key:"+key);
        cache.put(key,value);
        System.out.println(key+"写入OK");
        readWriteLock.writeLock().unlock();
    }
    public void get(String key){
        readWriteLock.readLock().lock();
        System.out.println(Thread.currentThread().getName()+key);
        System.out.println(cache.get(key));
        System.out.println(key+"获取OK");
        readWriteLock.readLock().unlock();
    }
}
