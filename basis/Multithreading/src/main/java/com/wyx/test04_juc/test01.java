package com.wyx.test04_juc;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/15 22:24
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {
        Data data = new Data();
        new Thread(()->{ for (int i = 0; i < 10; i++) {
            try {
                data.increase();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } },"Producer").start();

        new Thread(()->{ for (int i = 0; i < 10; i++) {
            try {
                data.decrease();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } },"Consumer").start();
        new Thread(()->{ for (int i = 0; i < 10; i++) {
            try {
                data.increase();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } },"Producer1").start();

        new Thread(()->{ for (int i = 0; i < 10; i++) {
            try {
                data.decrease();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } },"Consumer1").start();
    }
}
class Data{
    private int sum=0;
    public synchronized void decrease() throws Exception {
        while (sum==0){
            this.wait();
        }
        sum--;
        System.out.println(Thread.currentThread().getName()+"现在"+sum);

        notifyAll();

    }
    public synchronized void increase() throws Exception{
        while (sum!=0){
            this.wait();
        }
        sum++;
        System.out.println(Thread.currentThread().getName()+"现在"+sum);
        notifyAll();
    }
}
