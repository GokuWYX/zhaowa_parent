package com.wyx.test04_juc;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test04_juc
 * @ClassName: test06
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/17 19:15
 * @Version: 1.0
 */
public class test06 {
    public static void main(String[] args) throws Exception {
        Dock doc = new Dock();
        FutureTask futureTask = new FutureTask(doc);
        new Thread(futureTask,"A").start();
        new Thread(futureTask,"B").start();
        System.out.println(Thread.currentThread().getName()+"OK");
        Object o = futureTask.get();
        System.out.println(o);

    }
}

class Dock implements Callable<Integer>{

    @Override
    public Integer call() throws Exception {
        return 23;
    }
}
