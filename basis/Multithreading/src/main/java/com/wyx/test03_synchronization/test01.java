package com.wyx.test03_synchronization;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test03_synchronization
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/15 9:43
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {
        Buy buy = new Buy();
        new Thread(buy,"01").start();
        new Thread(buy,"02").start();
    }
}
class Buy implements Runnable{

    private Integer yue=1000;
    @Override
    public  void run() {
        synchronized (yue){
            while (yue>0){
                yue-=50;
                System.out.println("余额是"+yue+"----"+Thread.currentThread().getName());
            }
        }
    }
}
