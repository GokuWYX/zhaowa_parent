package com.wyx.test03_synchronization;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test03_synchronization
 * @ClassName: test02
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/15 9:51
 * @Version: 1.0
 */
public class test02 {
    public static void main(String[] args) {
        Shop shop = new Shop();
        new Thread(shop,"顾客1").start();
        new Thread(shop,"顧客2").start();
    }
}

class Shop implements Runnable{
    private Integer sum=1000;
    private final  ReentrantLock lock = new ReentrantLock();

    @Override
    public void run() {

        while (sum>0){
            try {
                lock.lock();
                sum-=50;
                Thread.sleep(100);
                System.out.println("余额："+sum+"  线程名 "+Thread.currentThread().getName());

            }catch (Exception e){
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
        }

    }
}
