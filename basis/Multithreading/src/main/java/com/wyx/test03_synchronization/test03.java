package com.wyx.test03_synchronization;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test03_synchronization
 * @ClassName: test03
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/15 15:15
 * @Version: 1.0
 */
public class test03 {
    public static void main(String[] args) {
        ExecutorService service= Executors.newFixedThreadPool(10);
        for (int i = 0; i < 15; i++) {
            service.execute(new MyThread());
        }
    }
}
class MyThread implements Runnable{

    @Override
    public void run() {
            System.out.println(Thread.currentThread().getName());
    }
}
