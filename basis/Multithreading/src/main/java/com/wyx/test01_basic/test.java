package com.wyx.test01_basic;

import org.junit.Test;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx
 * @ClassName: test01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/13 17:03
 * @Version: 1.0
 */
public class test {
    public static void main(String[] args) {
        //实现Runnnable
        ThreadImplementsRunnnable t1 = new ThreadImplementsRunnnable("01");
        ThreadImplementsRunnnable t2 = new ThreadImplementsRunnnable("02");
        ThreadImplementsRunnnable t3 = new ThreadImplementsRunnnable("03");
        new Thread(t1).start();
        new Thread(t2).start();
        new Thread(t3).start();
        //集成Thread
        ThreadExtendThread thread = new ThreadExtendThread();
        thread.start();
    }
    @Test
    public void test( ) {
        TstContonent tstContonent = new TstContonent();

        for (int i = 1; i < 1000; i++) {
            new Thread(()->{
                tstContonent.setMsg(Thread.currentThread().getName());
                System.out.println(Thread.currentThread().getName() .equals(tstContonent.getMsg()));

            }).start();
        }
    }

}

class TstContonent{

    ThreadLocal<String> localMsg=new ThreadLocal<>();
    private String msg;

    public String getMsg() {
        return localMsg.get();
        //return msg;
    }

    public void setMsg(String msg) {
        localMsg.set(msg);

       // this.msg=msg;
    }

    public TstContonent() {
    }

    public TstContonent(String msg) {
        this.msg = msg;
    }
}

