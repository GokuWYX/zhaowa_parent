package com.wyx.test01_basic;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URL;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx
 * @ClassName: StartThread
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/13 17:12
 * @Version: 1.0
 */
public class ThreadImplementsRunnnable implements Runnable {
    @Override
    public void run() {
        WebDownLoader webDownLoader = new WebDownLoader();
        String url="https://www.7762032.com/web_File/images/Java/JavaSE/List.jpg";
        String file=name+".jpg";
        webDownLoader.downloader(url,file);
        System.out.println(name);
    }
    private String name;

    public ThreadImplementsRunnnable(String name) {
        this.name = name;
    }

}

class WebDownLoader {
    public void downloader(String url,String name){
        try {
            FileUtils.copyURLToFile(new URL(url),new File(name));
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("下载失败");
        }
    }
}