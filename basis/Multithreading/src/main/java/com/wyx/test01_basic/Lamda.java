package com.wyx.test01_basic;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.test01_basic
 * @ClassName: Lamda
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/14 19:11
 * @Version: 1.0
 */
public class Lamda{
    //2、静态内部类
    static class staticInerClass implements a{
        @Override
        public void getIne() {
            System.out.println("静态内部类");

        }
    }


    public static void main(String[] args) {

        //3、局部内部类
        class innerMethodClass implements a{
            @Override
            public void getIne() {
                System.out.println("局部内部类");
            }
        }
        //1、外部普通类
        a aa = new outClass();
        aa.getIne();

        //2、静态内部类
        aa=new staticInerClass();
        aa.getIne();

        //3、局部内部类
        aa=new innerMethodClass();
        aa.getIne();

        //4、匿名内部类
            aa = new a() {
            @Override
            public void getIne() {
                System.out.println("匿名内部类");
            }
        };



        //4、匿名内部类
        aa.getIne();



        //5、lambda表达式
        aa=()->{
            System.out.println("lambad 表达式");
        };
        aa.getIne();

        //6、lambda表达式有参数
        b bb=(canshu)->{
            System.out.println(""+canshu);
        };
        String canshu="lambda表达式有参数";
        bb.getIne(canshu);
        //6、lambda表达式有参数,多行,多行必须有花括号
        b bd=(canshubd)->{
            System.out.println(""+canshubd+"多行");
            System.out.println(""+canshubd+"第二行");
        };
        bd.getIne(canshu);
        //7、lambda表达式单行,简化物花括号,简化单参数括号，无参不可去掉无参的括号
        bb=canshubb-> System.out.println(canshubb+"单行无花括号,并且参数无括号");
        bb.getIne(canshu);
        //8、lambda表达式单行,多参数无法去掉参数括号，简化花括号
        c cc=(canshuc1,canshuc2)-> System.out.println(canshuc1+"单行无花括号,多参数必须有括号   "+canshuc2);
        cc.getss(canshu,canshu+"2");
    }
}
//1、外部普通类
class outClass implements a{
    @Override
    public void getIne() {
        System.out.println("外部类");
    }
}
interface a{
    void getIne();
}
interface b{
    void  getIne(String a);
}
interface c{
    void getss(String a,String b);
}