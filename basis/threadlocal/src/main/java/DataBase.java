public class DataBase {
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public static void main(String[] args) {
        DataBase dataBase = new DataBase();
        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    dataBase.setData(Thread.currentThread().getName() + "的数据");
                    System.out.println("----------------");
                    System.out.println(Thread.currentThread().getName() + "-->" + dataBase.getData());
                }
            });
            thread.setName("线程" + i);
            thread.start();
        }
    }
}
