import java.util.concurrent.atomic.AtomicInteger;

public class ThreadLocalDataBase {
    private static ThreadLocal<String> t1 = new ThreadLocal<>();
    private String data;

    public String getData() {
        return t1.get();
    }

    public void setData(String data) {
        t1.set(data);
    }

    public static void main(String[] args) {
        ThreadLocalDataBase threadLocalDataBase = new ThreadLocalDataBase();
        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    threadLocalDataBase.setData(Thread.currentThread().getName() + "的数据");
                    System.out.println("----------------");
                    System.out.println(Thread.currentThread().currentThread().getName() + "-->" + threadLocalDataBase.getData());
                }
            });
            thread.setName("线程" + i);
            thread.start();
        }
    }
}
