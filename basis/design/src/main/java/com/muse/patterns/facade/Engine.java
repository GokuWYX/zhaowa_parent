package com.muse.patterns.facade;

/**
 * @description
 * @author: muse
 **/
public class Engine {
    public void start() {
        System.out.println("Engine start");
    }
}
