package com.muse.patterns.strategy.quack;

/**
 * @description 飞行行为
 * @author: muse
 **/
public interface QuackBehaviorBehavior {

    void quack();
}
