package com.muse.patterns.observer;

/**
 * @description 观察者2
 * @author: muse
 **/
public class ConcreteObserver2 implements Observer {

    public void update() {
        System.out.println("ConcreteObserver2");
    }
}
