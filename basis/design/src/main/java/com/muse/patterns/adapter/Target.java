package com.muse.patterns.adapter;

/**
 * @description
 * @author: muse
 **/
public interface Target {

    void prepare();

    void execute();
}
