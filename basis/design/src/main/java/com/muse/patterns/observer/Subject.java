package com.muse.patterns.observer;

/**
 * @description
 * @author: muse
 **/
public interface Subject {

    void registerObserver(Observer observer);

    void removeObserver(Observer observer);

    void notifyObserver();
}
