package com.muse.patterns.decorator;

/**
 * @description 奶泡
 * @author: muse
 **/
public class Whip extends CondimentDecorator {
    private Beverage beverage;

    public Whip(Beverage beverage) {
        this.beverage = beverage;
    }

    public String getDescription() {
        return "Whip";
    }

    public double cost() {
        System.out.println("Whip case：" + 0.5);
        return 0.5 + beverage.cost();
    }

}
