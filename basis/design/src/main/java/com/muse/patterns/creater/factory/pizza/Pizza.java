package com.muse.patterns.creater.factory.pizza;

/**
 * @description
 * @author: muse
 * @create: 202d1-08-11 09:22
 **/
public interface Pizza {
    void prepare();
    void bake();
    void cut();
    void box();
}
