package com.muse.patterns.iterator;

/**
 * @description
 * @author: muse
 **/
public interface Menu {

    // 获得迭代器
    MenuIterator iterator();
}
