package com.muse.patterns.decorator;

/**
 * @description 调料抽象类
 * @author: muse
 **/
public abstract class CondimentDecorator extends Beverage {

    public abstract String getDescription();
}
