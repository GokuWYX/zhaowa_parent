package com.muse.patterns.strategy.duck;

import com.muse.patterns.strategy.fly.FlyWithWings;
import com.muse.patterns.strategy.quack.Quack;

/**
 * @description 红头鸭
 * @author: muse
 **/
public class RedheadDuck extends Duck {

    public RedheadDuck() {
        flyBehavior = new FlyWithWings(); // 鸭子飞行
        quackBehaviorBehavior = new Quack(); // 红头鸭的呱呱叫
    }

    public void display() {
        System.out.println("红头鸭的外貌特征");
    }
}
