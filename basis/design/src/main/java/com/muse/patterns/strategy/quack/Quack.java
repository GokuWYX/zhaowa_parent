package com.muse.patterns.strategy.quack;

/**
 * @description
 * @author: muse
 **/
public class Quack implements QuackBehaviorBehavior {
    public void quack() {
        System.out.println("普通鸭子的呱呱叫");
    }
}
