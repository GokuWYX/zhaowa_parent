package com.muse.patterns.facade;

/**
 * @description
 * @author: muse
 **/
public class Key {
    public boolean turns() {
        System.out.println("Key turns");
        return true;
    }
}
