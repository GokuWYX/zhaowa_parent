package com.muse.patterns.creater.factory.pizza.v2;

import com.muse.patterns.creater.factory.pizza.Pizza;

/**
 * @description
 * @author: muse
 **/
public class NYCheesePizza implements Pizza {

    public void prepare() {
        System.out.println("NYCheesePizza prepare()");
    }

    public void bake() {
        System.out.println("NYCheesePizza bake()");
    }

    public void cut() {
        System.out.println("NYCheesePizza cut()");
    }

    public void box() {
        System.out.println("NYCheesePizza box()");
    }
}
