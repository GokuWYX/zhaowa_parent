package com.muse.patterns.creater.factory.ingredient;

/**
 * @description 调味汁
 * @author: muse
 **/
public interface Sauce {

    void description();
}
