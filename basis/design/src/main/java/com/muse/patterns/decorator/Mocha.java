package com.muse.patterns.decorator;

/**
 * @description 摩卡
 * @author: muse
 **/
public class Mocha extends CondimentDecorator {

    private Beverage beverage;

    public Mocha(Beverage beverage) {
        this.beverage = beverage;
    }

    public String getDescription() {
        return "Mocha";
    }

    public double cost() {
        System.out.println("Mocha case：" + 0.99);
        return 0.99 + beverage.cost();
    }
}
