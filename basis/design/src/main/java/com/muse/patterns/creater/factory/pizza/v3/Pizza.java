package com.muse.patterns.creater.factory.pizza.v3;

import com.muse.patterns.creater.factory.PizzaIngredientFactory;
import com.muse.patterns.creater.factory.ingredient.Cheese;
import com.muse.patterns.creater.factory.ingredient.Clams;
import com.muse.patterns.creater.factory.ingredient.Dough;
import com.muse.patterns.creater.factory.ingredient.Sauce;

/**
 * @description
 * @author: muse
 **/
public abstract class Pizza {
    protected String name;
    protected Dough dough;
    protected Sauce sauce;
    protected Cheese cheese;
    protected Clams clams;
    protected PizzaIngredientFactory pizzaIngredientFactory;

    /** 准备原材料 */
    public abstract void prepare();

    public void bake() {
        System.out.println("pizzaIngredientFactory bake()");
    }
    public void cut() {
        System.out.println("pizzaIngredientFactory cut()");
    }
    public void box() {
        System.out.println("pizzaIngredientFactory box()");
    }
}
