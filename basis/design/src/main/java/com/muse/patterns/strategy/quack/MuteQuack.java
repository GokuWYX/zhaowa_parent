package com.muse.patterns.strategy.quack;

/**
 * @description
 * @author: muse
 **/
public class MuteQuack implements QuackBehaviorBehavior {
    public void quack() {
        System.out.println("哑巴鸭，不会叫");
    }
}
