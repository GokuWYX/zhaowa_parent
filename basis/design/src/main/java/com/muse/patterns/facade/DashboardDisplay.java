package com.muse.patterns.facade;

/**
 * @description
 * @author: muse
 **/
public class DashboardDisplay {
    void refreshDisplay() {
        System.out.println("DashboardDisplay refreshDisplay");
    }
}
