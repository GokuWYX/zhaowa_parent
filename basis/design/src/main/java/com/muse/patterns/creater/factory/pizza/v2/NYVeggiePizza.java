package com.muse.patterns.creater.factory.pizza.v2;

import com.muse.patterns.creater.factory.pizza.Pizza;

/**
 * @description
 * @author: muse
 **/
public class NYVeggiePizza implements Pizza {

    public void prepare() {
        System.out.println("NYVeggiePizza prepare()");
    }

    public void bake() {
        System.out.println("NYVeggiePizza bake()");
    }

    public void cut() {
        System.out.println("NYVeggiePizza cut()");
    }

    public void box() {
        System.out.println("NYVeggiePizza box()");
    }
}
