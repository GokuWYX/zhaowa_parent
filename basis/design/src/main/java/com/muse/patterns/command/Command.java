package com.muse.patterns.command;

/**
 * @description
 * @author: muse
 **/
public interface Command {
    void execute();
}
