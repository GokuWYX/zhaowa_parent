package com.muse.patterns.strategy.fly;

/**
 * @description 飞行行为
 * @author: muse
 **/
public interface FlyBehavior {

    void fly();
}
