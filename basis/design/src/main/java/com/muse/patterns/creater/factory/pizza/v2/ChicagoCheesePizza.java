package com.muse.patterns.creater.factory.pizza.v2;

import com.muse.patterns.creater.factory.pizza.Pizza;

/**
 * @description
 * @author: muse
 **/
public class ChicagoCheesePizza implements Pizza {

    public void prepare() {
        System.out.println("ChicagoCheesePizza prepare()");
    }

    public void bake() {
        System.out.println("ChicagoCheesePizza bake()");
    }

    public void cut() {
        System.out.println("ChicagoCheesePizza cut()");
    }

    public void box() {
        System.out.println("ChicagoCheesePizza box()");
    }
}
