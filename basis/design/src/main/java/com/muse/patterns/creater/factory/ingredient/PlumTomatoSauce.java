package com.muse.patterns.creater.factory.ingredient;

/**
 * @description 梅子西红柿调味汁
 * @author: muse
 **/
public class PlumTomatoSauce implements Sauce {

    public void description() {
        System.out.println("PlumTomatoSauce");
    }
}
