package com.muse.patterns.observer;

/**
 * @description
 * @author: muse
 **/
public interface Observer {

    void update();
}
