package com.muse.patterns.strategy.quack;

/**
 * @description
 * @author: muse
 **/
public class Squeak implements QuackBehaviorBehavior {
    public void quack() {
        System.out.println("橡皮鸭的吱吱叫");
    }
}
