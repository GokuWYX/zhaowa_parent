package com.muse.patterns.command;

/**
 * @description
 * @author: muse
 **/
public class NoCommand implements Command {
    public void execute() {
        System.out.println("Doing nothing!");
    }
}
