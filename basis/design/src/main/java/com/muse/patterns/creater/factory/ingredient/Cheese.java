package com.muse.patterns.creater.factory.ingredient;

/**
 * @description 奶酪
 * @author: muse
 **/
public interface Cheese {

    void description();
}
