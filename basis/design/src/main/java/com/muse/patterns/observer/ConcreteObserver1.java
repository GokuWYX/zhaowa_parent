package com.muse.patterns.observer;

/**
 * @description 观察者1
 * @author: muse
 **/
public class ConcreteObserver1 implements Observer {

    public void update() {
        System.out.println("ConcreteObserver1");
    }
}
