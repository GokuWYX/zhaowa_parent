package com.muse.patterns.observer;

/**
 * @description 观察者3
 * @author: muse
 **/
public class ConcreteObserver3 implements Observer {

    public void update() {
        System.out.println("ConcreteObserver3");
    }
}
