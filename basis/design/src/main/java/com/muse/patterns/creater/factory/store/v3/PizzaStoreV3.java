package com.muse.patterns.creater.factory.store.v3;

import com.muse.patterns.creater.factory.pizza.v3.Pizza;

/**
 * @description
 * @author: muse
 **/
public abstract class PizzaStoreV3 {

    protected abstract Pizza createPizza(String pizzaType);

    public Pizza orderPizza(String pizzaType) {
        Pizza pizza =  createPizza(pizzaType);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
