package com.muse.patterns.creater.factory.pizza.v3;

import com.muse.patterns.creater.factory.PizzaIngredientFactory;

/**
 * @description 奶酪披萨
 * @author: muse
 **/
public class VeggiePizza extends Pizza {

    public VeggiePizza(PizzaIngredientFactory pizzaIngredientFactory) {
        this.pizzaIngredientFactory = pizzaIngredientFactory;
    }

    public void prepare() {
        System.out.println("VeggiePizza prepare()");
        dough = pizzaIngredientFactory.createDough();
        sauce = pizzaIngredientFactory.createSauce();
        cheese = pizzaIngredientFactory.createCheese();
    }
}
