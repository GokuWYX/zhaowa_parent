package com.muse.patterns.strategy.fly;

/**
 * @description 实现鸭子的飞行
 * @author: muse
 **/
public class FlyWithWings implements FlyBehavior {
    public void fly() {
        System.out.println("实现鸭子的飞行");
    }
}
