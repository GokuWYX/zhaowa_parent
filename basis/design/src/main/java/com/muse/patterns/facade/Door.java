package com.muse.patterns.facade;

/**
 * @description
 * @author: muse
 **/
public class Door {
    public void lock() {
        System.out.println("Door lock");
    }
}
