package com.muse.patterns.creater.factory;

import com.muse.patterns.creater.factory.ingredient.Cheese;
import com.muse.patterns.creater.factory.ingredient.Clams;
import com.muse.patterns.creater.factory.ingredient.Dough;
import com.muse.patterns.creater.factory.ingredient.FreshClams;
import com.muse.patterns.creater.factory.ingredient.MarinaraSauce;
import com.muse.patterns.creater.factory.ingredient.ReggianoCheese;
import com.muse.patterns.creater.factory.ingredient.Sauce;
import com.muse.patterns.creater.factory.ingredient.ThinCrustDough;

/**
 * @description 芝加哥原料工厂
 * @author: muse
 **/
public class ChicagoPizzaingredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new ThinCrustDough();
    }

    public Sauce createSauce() {
        return new MarinaraSauce();
    }

    public Cheese createCheese() {
        return new ReggianoCheese();
    }

    public Clams createClams() {
        return new FreshClams();
    }
}
