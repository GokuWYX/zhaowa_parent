package com.muse.patterns.creater.factory.ingredient;

/**
 * @description 薄的面包皮生面团
 * @author: muse
 **/
public class ThinCrustDough implements Dough {

    public void description() {
        System.out.println("ThinCrustDough");
    }
}
