package com.muse.patterns.creater.factory;

import com.muse.patterns.creater.factory.ingredient.Cheese;
import com.muse.patterns.creater.factory.ingredient.Clams;
import com.muse.patterns.creater.factory.ingredient.Dough;
import com.muse.patterns.creater.factory.ingredient.FrozenClams;
import com.muse.patterns.creater.factory.ingredient.MozzarellaCheese;
import com.muse.patterns.creater.factory.ingredient.PlumTomatoSauce;
import com.muse.patterns.creater.factory.ingredient.Sauce;
import com.muse.patterns.creater.factory.ingredient.ThickCrustDough;

/**
 * @description 纽约原料工厂
 * @author: muse
 **/
public class NYPizzaingredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new ThickCrustDough();
    }

    public Sauce createSauce() {
        return new PlumTomatoSauce();
    }

    public Cheese createCheese() {
        return new MozzarellaCheese();
    }

    public Clams createClams() {
        return new FrozenClams();
    }
}
