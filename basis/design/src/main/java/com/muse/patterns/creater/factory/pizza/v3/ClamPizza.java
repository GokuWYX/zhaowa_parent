package com.muse.patterns.creater.factory.pizza.v3;

import com.muse.patterns.creater.factory.PizzaIngredientFactory;

/**
 * @description 蛤蜊披萨
 * @author: muse
 **/
public class ClamPizza extends Pizza {

    public ClamPizza(PizzaIngredientFactory pizzaIngredientFactory) {
        this.pizzaIngredientFactory = pizzaIngredientFactory;
    }

    public void prepare() {
        System.out.println("ClamPizza prepare()");
        dough = pizzaIngredientFactory.createDough();
        sauce = pizzaIngredientFactory.createSauce();
        cheese = pizzaIngredientFactory.createCheese();
        // 加入蛤蜊配料
        clams = pizzaIngredientFactory.createClams();
    }
}
