package com.muse.patterns.creater.prototype;

/**
 * 通过clone方法创建的Prototype对象不会执行构造方法
 */
public class Prototype implements Cloneable {
    public static void main(String[] args) {
        Prototype prototype = new Prototype();

        /** 执行clone方法创建的Prototype对象 */
        Prototype clonePrototype = prototype.clone();
    }

    public Prototype() {
        System.out.println("-----Prototype的构造方法被执行了！-----");
    }

    @Override
    protected Prototype clone() {
        try {
            return (Prototype)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
