package com.muse.patterns.creater.factory.ingredient;

/**
 * @description 莫泽雷勒干酪
 * @author: muse
 **/
public class MozzarellaCheese implements Cheese {

    public void description() {
        System.out.println("MozzarellaCheese");
    }
}
