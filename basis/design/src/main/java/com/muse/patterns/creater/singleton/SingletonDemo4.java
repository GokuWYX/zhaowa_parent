package com.muse.patterns.creater.singleton;

/**
 * @description 静态内部类（线程安全，调用效率高，可以延时加载）
 * @author: Gogoku
 **/
public class SingletonDemo4 {

    /** 静态内部类 */
    private static class SingletonClassInstance {
        private static final SingletonDemo4 instance = new SingletonDemo4();
    }

    private SingletonDemo4(){}

    public static SingletonDemo4 getInstance(){
        return SingletonClassInstance.instance;
    }
}
