package com.muse.patterns.strategy.fly;

/**
 * @description 不会飞行
 * @author: muse
 **/
public class FlyNoWay implements FlyBehavior {
    public void fly() {
        System.out.println("不会飞行");
    }
}
