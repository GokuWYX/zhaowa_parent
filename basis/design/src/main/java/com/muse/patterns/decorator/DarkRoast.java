package com.muse.patterns.decorator;

/**
 * @description 深焙咖啡
 * @author: muse
 **/
public class DarkRoast extends Beverage {

    public DarkRoast() {
        description = "Dark Roast Coffee";
    }

    public double cost() {
        System.out.println("DarkRoast case：" + 1.99);
        return 1.99;
    }
}
