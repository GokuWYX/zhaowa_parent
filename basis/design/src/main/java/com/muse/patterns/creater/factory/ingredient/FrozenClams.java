package com.muse.patterns.creater.factory.ingredient;

/**
 * @description 冷冻蛤蜊
 * @author: muse
 * @create: 202d1-08-11 12:16
 **/
public class FrozenClams implements Clams{

    public void description() {
        System.out.println("FrozenClams");
    }
}
