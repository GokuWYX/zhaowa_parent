package com.muse.patterns.proxy;

public interface Subject {

    void request();
}
