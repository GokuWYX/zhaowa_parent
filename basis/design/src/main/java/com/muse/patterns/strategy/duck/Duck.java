package com.muse.patterns.strategy.duck;

import com.muse.patterns.strategy.fly.FlyBehavior;
import com.muse.patterns.strategy.quack.QuackBehaviorBehavior;

/**
 * @description
 * @author: muse
 **/
public abstract class Duck {

    protected FlyBehavior flyBehavior;

    protected QuackBehaviorBehavior quackBehaviorBehavior;

    public abstract void display();

    public void swin() {
        System.out.println("鸭子游泳");
    }

    public void performQuack() {
        quackBehaviorBehavior.quack();
    }

    public void performFly() {
        flyBehavior.fly();
    }

    public void setFlyBehavior(FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public void setQuackBehaviorBehavior(QuackBehaviorBehavior quackBehaviorBehavior) {
        this.quackBehaviorBehavior = quackBehaviorBehavior;
    }
}
