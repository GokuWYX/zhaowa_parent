package com.muse.patterns.creater.factory.ingredient;

/**
 * @description 生面团
 * @author: muse
 **/
public interface Dough {

    void description();
}
