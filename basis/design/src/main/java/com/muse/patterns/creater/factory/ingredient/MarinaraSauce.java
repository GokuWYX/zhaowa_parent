package com.muse.patterns.creater.factory.ingredient;

/**
 * @description 番茄酱调味汁
 * @author: muse
 **/
public class MarinaraSauce implements Sauce {

    public void description() {
        System.out.println("MarinaraSauce");
    }
}
