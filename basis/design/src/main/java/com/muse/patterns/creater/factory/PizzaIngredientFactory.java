package com.muse.patterns.creater.factory;

import com.muse.patterns.creater.factory.ingredient.Cheese;
import com.muse.patterns.creater.factory.ingredient.Clams;
import com.muse.patterns.creater.factory.ingredient.Dough;
import com.muse.patterns.creater.factory.ingredient.Sauce;

/**
 * @description 披萨原料抽象工厂
 * @author: muse
 **/
public interface PizzaIngredientFactory {

    Dough createDough();

    Sauce createSauce();

    Cheese createCheese();

    Clams createClams();
}
