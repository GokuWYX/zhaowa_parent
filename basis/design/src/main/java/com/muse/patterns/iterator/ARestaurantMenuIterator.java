package com.muse.patterns.iterator;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @description
 * @author: muse
 **/
public class ARestaurantMenuIterator implements MenuIterator {

    private ArrayList<MenuItem> menuItems;
    private Iterator<MenuItem> iterator;

    public ARestaurantMenuIterator(ArrayList<MenuItem> menuItems) {
        this.menuItems = menuItems;
        iterator = menuItems.iterator();
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public MenuItem next() {
        return iterator.next();
    }
}
