package com.wyx.es;

import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: Doce_Mutiplay_del
 * @Author: GoKu
 * @Description:
 * @Date: 2022/1/13 22:14
 * @Version: 1.0
 */
public class Doce_Mutiplay_del {
    public static void main(String[] args) throws Exception {
        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")));
        BulkRequest request = new BulkRequest();
        request.add(new DeleteRequest().index("user").id("9001"));
        request.add(new DeleteRequest().index("user").id("9002"));
        request.add(new DeleteRequest().index("user").id("9003"));
        request.add(new DeleteRequest().index("user").id("9004"));
        BulkResponse response = client.bulk(request, RequestOptions.DEFAULT);
        System.out.println("took===="+response.getTook());
        System.out.println("items==="+response.getItems());

    }
}
