package com.wyx.es.po;

import java.util.Objects;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es.po
 * @ClassName: User
 * @Author: GoKu
 * @Description:
 * @Date: 2022/1/13 19:43
 * @Version: 1.0
 */
public class User {
    private String name;
    private Integer age;
    private String sex;



    public User() {
    }

    public User(String name, Integer age, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
