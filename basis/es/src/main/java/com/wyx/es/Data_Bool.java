package com.wyx.es;

import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: Data_Bool
 * @Author: GoKu
 * @Description:
 * @Date: 2022/1/16 19:10
 * @Version: 1.0
 */
public class Data_Bool {
    public static void main(String[] args) throws Exception {

        //创建es客户端
        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")));
        //创建走索请求对象
        SearchRequest request = new SearchRequest();
        request.indices();

        //构建查询的请求体
        SearchSourceBuilder builder = new SearchSourceBuilder();

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();



        //必须包含
        boolQueryBuilder.must(QueryBuilders.matchQuery("sex","male"));
        boolQueryBuilder.mustNot(QueryBuilders.matchQuery("age","23"));
        boolQueryBuilder.should(QueryBuilders.matchQuery("name","zs"));

        //查询匹配
        builder.query(boolQueryBuilder);
        request.source(builder);
        SearchResponse resonse = client.search(request, RequestOptions.DEFAULT);
        SearchHits hits = resonse.getHits();
        System.out.println("toook"+resonse.getTook());
        System.out.println("timeout"+resonse.isTimedOut());
        System.out.println("total"+hits.getTotalHits());
        System.out.println("maxsource"+hits.getMaxScore());
        System.out.println("--------------------------");

        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
        System.out.println("--------------------------");

    }
}
