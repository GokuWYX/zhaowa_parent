package com.wyx.es;

import org.apache.http.HttpHost;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: Doc_Search
 * @Author: GoKu
 * @Description:
 * @Date: 2022/1/13 21:45
 * @Version: 1.0
 */
public class Doc_Search {
    public static void main(String[] args) throws Exception{
        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")));

        GetRequest searchRequest = new GetRequest().index("user").id("1001");
        GetResponse getResponse = client.get(searchRequest, RequestOptions.DEFAULT);
        System.out.println("_index==="+getResponse.getIndex());
        System.out.println("_type===="+getResponse.getType());
        System.out.println("_id======"+getResponse.getId());
        System.out.println("source:"+getResponse.getSourceAsString());

    }
}
