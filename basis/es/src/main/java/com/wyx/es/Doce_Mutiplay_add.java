package com.wyx.es;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.regexp.internal.RE;
import com.wyx.es.po.User;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: Doce_Mutiplay
 * @Author: GoKu
 * @Description:
 * @Date: 2022/1/13 22:09
 * @Version: 1.0
 */
public class Doce_Mutiplay_add {
    public static void main(String[] args) throws Exception {
        /*

        request.source();
        * */

        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")));
        BulkRequest request = new BulkRequest();
        request.add(new IndexRequest().index("user").id("9001").source(new ObjectMapper().writeValueAsString(new User("zss", 10, "male")), XContentType.JSON));
        request.add(new IndexRequest().index("user").id("9002").source(new ObjectMapper().writeValueAsString(new User("lss", 21, "female")), XContentType.JSON));
        request.add(new IndexRequest().index("user").id("9003").source(new ObjectMapper().writeValueAsString(new User("www", 22, "male")), XContentType.JSON));
        request.add(new IndexRequest().index("user").id("9004").source(new ObjectMapper().writeValueAsString(new User("zll", 23, "male")), XContentType.JSON));

        userCreateUser("9005",new User("sda",123,"female"));
        BulkResponse response = client.bulk(request, RequestOptions.DEFAULT);
        System.out.println("-----------------------------------------------------------------------");
        System.out.println("took===="+response.getTook());
        System.out.println("items==="+response.getItems());
        System.out.println("-----------------------------------------------------------------------");

    }


    public static void userCreateUser(String id,User user) throws Exception{
        CreateUser("user",id,user);
    }


    public static void CreateUser(String index,String id,User user) throws Exception{
        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")));
        IndexRequest request = new IndexRequest();
        request.index(index).id(id);
        String productJson = new ObjectMapper().writeValueAsString(user);
        request.source(productJson, XContentType.JSON);
        client.index(request, RequestOptions.DEFAULT);
        client.close();
    }




}
