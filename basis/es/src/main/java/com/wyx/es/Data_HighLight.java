package com.wyx.es;

import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;

import java.util.Map;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx
 * @ClassName: Data_HighLight
 * @Author: GoKu
 * @Description:
 * @Date: 2022/1/17 9:27
 * @Version: 1.0
 */
public class Data_HighLight {
    public static void main(String[] args) throws  Exception{
        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")));
        SearchRequest request = new SearchRequest().indices("user");

        //創建查詢请求体构建器
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //构建高亮查询方式
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", "sda");
        //设置查询方式
        searchSourceBuilder.query(termQueryBuilder);
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        //设置标签前缀
        highlightBuilder.preTags("<font color='red'>");
        //设置标签后缀
        highlightBuilder.postTags("</font>");
        //设置高亮字段
        highlightBuilder.field("name");
        //设置高亮构建对象
        searchSourceBuilder.highlighter(highlightBuilder);
        //设置请求体
        request.source(searchSourceBuilder);
        //客户端发送请求，获取响应对象
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //打印响应结果
        SearchHits hits = response.getHits();
        System.out.println("tokk====="+response.getTook());
        System.out.println("timeout=="+response.isTimedOut());
        System.out.println("total===="+hits.getTotalHits());
        System.out.println("max score"+hits.getMaxScore());
        System.out.println("===============");

        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
            //打印高亮结果
            Map<String, HighlightField> highlightFields =
                    hit.getHighlightFields();
            System.out.println(highlightFields);
        }

        System.out.println("===============");





    }
}
