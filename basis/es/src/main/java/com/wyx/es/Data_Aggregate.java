package com.wyx.es;

import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: Data_Aggregate
 * @Author: GoKu
 * @Description: 聚合查询
 * @Date: 2022/1/17 10:17
 * @Version: 1.0
 */
public class Data_Aggregate {
    public static void main(String[] args) throws Exception {
        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")));
        //创建请求对象
        SearchRequest request = new SearchRequest().indices("user");

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.aggregation(AggregationBuilders.max("maxAge").field("age"));

        //设置请求体
        request.source(searchSourceBuilder);

        //客户端发送请求，获取相应对象
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);

        //打印响应结果
        SearchHits hits = response.getHits();

        System.out.println(response);


    }
}
