package com.wyx.es;

import org.apache.http.HttpHost;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: Doc_Update
 * @Author: GoKu
 * @Description: 文档更新
 * @Date: 2022/1/13 20:04
 * @Version: 1.0
 */
public class Doc_Update {
    public static void main(String[] args) throws Exception{
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost", 9200, "http")));
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.index("user").id("1001");
        updateRequest.doc(XContentType.JSON,"sex","女");
        UpdateResponse response = client.update(updateRequest, RequestOptions.DEFAULT);
        System.out.println("_index==="+response.getIndex());
        System.out.println("_id======"+response.getId());
        System.out.println("_result=="+response.getResult());
        client.close();
    }
}
