package com.wyx.es;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wyx.es.po.User;
import org.apache.http.HttpHost;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: Doc_Create
 * @Author: GoKu
 * @Description:
 * @Date: 2022/1/13 19:49
 * @Version: 1.0
 */
public class Doc_Create {
    public static void main(String[] args) throws Exception {
        //创建es客户端
        RestHighLevelClient client=new RestHighLevelClient(RestClient.builder(new HttpHost("localhost",9200,"http")));

        IndexRequest request = new IndexRequest();
        request.index("user").id("1001");
        String productJson = new ObjectMapper().writeValueAsString(new User("zs", 20, "male"));
        request.source(productJson, XContentType.JSON);
        IndexResponse response = client.index(request, RequestOptions.DEFAULT);
        System.out.println("index===="+response.getIndex());
        System.out.println("id======"+response.getId());
        System.out.println("result=="+response.getResult());
        client.close();
    }

}
