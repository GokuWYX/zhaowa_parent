package com.wyx.es;

import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: Index_Delete
 * @Author: GoKu
 * @Description:
 * @Date: 2022/1/13 19:16
 * @Version: 1.0
 */
public class Index_Delete {
    public static void main(String[] args) throws Exception {

        //创建es客户端
        RestHighLevelClient client=new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost",9200,"http"))
        );

        DeleteIndexRequest request = new DeleteIndexRequest("user");

        AcknowledgedResponse delete = client.indices().delete(request, RequestOptions.DEFAULT);


        System.out.println("--------------------");
        System.out.println(delete.isAcknowledged());

        //关闭es客户端

        client.close();


    }
}
