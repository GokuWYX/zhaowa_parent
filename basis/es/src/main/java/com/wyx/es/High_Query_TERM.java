package com.wyx.es;

import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: High_Query_TERM
 * @Author: GoKu
 * @Description:
 * @Date: 2022/1/14 9:19
 * @Version: 1.0
 */
public class High_Query_TERM {
    public static void main(String[] args) throws Exception {
        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")));

        SearchRequest request = new SearchRequest();
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.termQuery("age",20));
        request.source(sourceBuilder);

        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        SearchHits hits = response.getHits();
        System.out.println("took======"+response.getTook());
        System.out.println("timeout==="+response.isTimedOut());
        System.out.println("total====="+hits.getTotalHits());
        System.out.println("MaxScore=="+hits.getMaxScore());
        System.out.println("hits======>>");
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
        System.out.println("hits======>>");

    }
}
