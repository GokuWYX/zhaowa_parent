package com.wyx.es;

import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: Data_Filter
 * @Author: GoKu
 * @Description:
 * @Date: 2022/1/14 16:34
 * @Version: 1.0
 */
public class Data_Filter {
    public static void main(String[] args) throws Exception {
        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")));
        //创建搜索请求对象
        SearchRequest request = new SearchRequest().indices("user");
        //创建查询请求体
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder().query(QueryBuilders.matchAllQuery());
        //查询字段过滤
        String[] excludes={};
        String[] includes={"name","age"};
        sourceBuilder.fetchSource(includes,excludes);
        request.source(sourceBuilder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //查询匹配
        SearchHits hits = response.getHits();



    }
}
