package com.wyx.es;

import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import javax.naming.directory.SearchResult;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: Data_Fuzzy
 * @Author: GoKu
 * @Description: 模糊查询
 * @Date: 2022/1/17 9:11
 * @Version: 1.0
 */
public class Data_Fuzzy {
    public static void main(String[] args) throws Exception{

        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")));
        //创建请求对象
        SearchRequest request = new SearchRequest().indices("user");

        //构建查询请求体
        SearchSourceBuilder sourceBuiler = new SearchSourceBuilder();
        sourceBuiler.query(QueryBuilders.fuzzyQuery("name","s").fuzziness(Fuzziness.ONE));
        request.source(sourceBuiler);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //查询匹配几何体
        SearchHits hits = response.getHits();
        System.out.println("took====="+response.getTook());
        System.out.println("timeout=="+response.isTimedOut());
        System.out.println("total===="+hits.getTotalHits());
        System.out.println("MaxScore="+hits.getMaxScore());
        System.out.println("=============");
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
        System.out.println("        \n        \n        \n        \n        \n        \n        \n        \n");


    }
}
