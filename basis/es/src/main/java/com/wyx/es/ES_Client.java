package com.wyx.es;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.IOException;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: ES_Client
 * @Author: GoKu
 * @Description: es客户端
 * @Date: 2022/1/13 15:14
 * @Version: 1.0
 */


public class ES_Client {
    public static void main(String[] args) {
        //创建es客户端
        RestHighLevelClient client=new RestHighLevelClient(
        RestClient.builder(new HttpHost("localhost",9200,"http"))
        );
        //关闭es客户端
        try {
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
