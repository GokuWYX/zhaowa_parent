package com.wyx.es;


import org.apache.http.HttpHost;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;




/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: Create
 * @Author: GoKu
 * @Description:
 * @Date: 2022/1/13 17:09
 * @Version: 1.0
 */
public class Index_Create {
    public static void main(String[] args) throws Exception {

        //创建es客户端
        RestHighLevelClient client=new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost",9200,"http"))
        );

        CreateIndexRequest request = new CreateIndexRequest("user");

        CreateIndexResponse response = client.indices().create(request, RequestOptions.DEFAULT);
        boolean acknowledged = response.isAcknowledged();
        System.out.println("操作状态"+acknowledged);
        //关闭es客户端

        client.close();


    }
}
