package com.wyx.es;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.es
 * @ClassName: Index_Searche
 * @Author: GoKu
 * @Description:
 * @Date: 2022/1/13 18:58
 * @Version: 1.0
 */
public class Index_Search {
    public static void main(String[] args) throws Exception {

        //创建es客户端
        RestHighLevelClient client=new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost",9200,"http"))
        );

        GetIndexRequest request = new GetIndexRequest("users");

        GetIndexResponse response = client.indices().get(request, RequestOptions.DEFAULT);


        System.out.println("--------------------");
        System.out.println(response.getAliases());
        System.out.println(response.getSettings());
        System.out.println(response.getMappings());


        //关闭es客户端

        client.close();


    }
}
