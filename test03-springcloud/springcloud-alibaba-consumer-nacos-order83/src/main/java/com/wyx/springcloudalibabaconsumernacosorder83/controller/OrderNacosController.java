package com.wyx.springcloudalibabaconsumernacosorder83.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RestController
public class OrderNacosController {
    @Resource
    private RestTemplate restTemplate;

    /*service-url:nacos-user-service: http://nacos-payment-provider*/
    @Value("${service-url.nacos-user-service}")
    private String serverURL;

    @GetMapping("/consumer/payment/nacos/{id}")
    public String getServerURL(@PathVariable("id") Integer id){
        System.out.println("-------------------------------------------------------------------");
        System.out.println(serverURL);


        //url=service-url.nacos-user-service/payment/nacos/id
        // ||r如下 ,其中 //192.168.124.1/9001与//192.168.124.1/9002都是service-url.nacos-user-service
        //并且   @LoadBalanced在RestTemplate上，所以负载均衡实现访问不同的两个地址
        return restTemplate.getForObject(serverURL+"/payment/nacos/"+id,String.class);
    }



}
