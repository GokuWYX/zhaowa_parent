package com.wyx.springcloudalibabaconsumernacosorder83;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;

@EnableDiscoveryClient
@SpringBootApplication
public class SpringcloudAlibabaConsumerNacosOrder83Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudAlibabaConsumerNacosOrder83Application.class, args);

    }

}
