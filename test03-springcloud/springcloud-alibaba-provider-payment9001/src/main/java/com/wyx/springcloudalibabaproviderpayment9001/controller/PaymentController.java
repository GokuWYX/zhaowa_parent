package com.wyx.springcloudalibabaproviderpayment9001.controller;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {
    @Value("${server.port}")
    public String serverPort;
    @GetMapping("/payment/nacos/{id}")
    public String getServerPort(@PathVariable("id") Integer id){
        return "这是一个provider，她的编号是"+id+"，她的端口是"+serverPort+",EDGNB";
    }
}
