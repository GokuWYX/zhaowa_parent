package com.wyx.springcloudalibabaproviderpayment9001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SpringcloudAlibabaProviderPayment9001Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudAlibabaProviderPayment9001Application.class, args);
    }

}
