package com.wyx.heap;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.heap
 * @ClassName: Heap
 * @Author: GoKu
 * @Description: 堆得模拟
 * @Date: 2022/3/26 15:00
 * @Version: 1.0
 */
public class Heap<T extends Comparable> {
    //存储堆中的元素
    private T[] items;
    //记录堆中元素个数
    private int N;

    public Heap(int capacity) {
        this.items=(T[])new Comparable[capacity+1];
        this.N = 0;
    }

    //判断堆中索引i处的元素是否小于索引j处的元素
    private boolean less(int i,int j){
        return items[i].compareTo(items[j])>0 ? false:true;
    }

    //交换堆中i索引和j索引处的值
    private void swap(int i,int j){
        T tmp = items[j];
        items[j]=items[i];
        items[i]=tmp;
    }

    //往堆中插入一个元素
    public void insert(T t){
        items[++N]=t;
        swim(N);
    }
    public T deleteMax(){
        T max = items[1];

        //交换索引1处的元素和最大索引处的元素，让完全二叉树中最右侧的元素变为临时根结点
        swap(1,N);
        //最大索引处的元素删除掉
        items[N]=null;
        //元素个数-1
        N--;
        //通过下沉调整堆，让堆重新有序
        sink(1);
        return max;
    }

    //使用下沉算法，使索引k处的元素能在堆中处于一个正确的位置
    private void sink(int k){
        while (k*2<=N){

            //最大值下标
            int maxIndex;
            //使用2k与2k+1比较的目的是先比较出左右节点的最大值，然后用最大值再于k对比
            if (2*k+1<=N){
                if (less(2*k,2*k+1)){
                    maxIndex=2*k+1;
                }else {
                    maxIndex=2*k;
                }
            }else {
                maxIndex=2*k;
            }

            if (!less(k,maxIndex)){
                break;
            }
            swap(k,maxIndex);
            k=maxIndex;
        }
    }

    //使用上浮算法，使索引k处的元素能在堆中处于一个正确的位置
    private void swim(int k){
        while (k>1){
            if (less(k/2,k)){
                swap(k/2,k);
            }
            k=k/2;
        }
    }

    public Heap() {
    }

    public static void main(String[] args) {
        Heap<String> heap = new Heap<String>(20);
        heap.insert("A");
        heap.insert("B");
        heap.insert("C");
        heap.insert("D");
        heap.insert("E");
        heap.insert("F");
        heap.insert("G");


        String del;
        while((del=heap.deleteMax())!=null){
            System.out.print(del+"  ");
        }
    }
}
