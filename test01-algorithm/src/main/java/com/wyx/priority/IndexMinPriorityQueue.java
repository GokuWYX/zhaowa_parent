package com.wyx.priority;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.priority
 * @ClassName: IndexPriorityQueue
 * @Author: GoKu
 * @Description: 索引优先队列
 * @Date: 2022/3/29 11:17
 * @Version: 1.0
 */
public class IndexMinPriorityQueue<T extends Comparable> {
    //存储堆中的元素
    private T[] items;
    //保存每个元素在items数组中的索引，pq数组需要堆有序
    private int[] pq;
    //保存qp的逆序，pq的值作为索引，pq的索引作为值
    private int[] qp;
    //记录堆中元素的个数
    private int N;


    public IndexMinPriorityQueue(int capacity) {
        this.items=(T[])new Comparable[capacity+1];
        this.pq=new int[capacity+1];
        this.qp=new int[capacity+1];
        this.N=0;
        for (int i = 0; i < pq.length; i++) {

            qp[i]=-1;
        }
    }

    //获取队列中元素的个数
    public int size() {
        return N;
    }

    //判断队列是否为空
    public boolean isEmpty() {
        return N==0;
    }

    //判断堆中索引i处的元素是否小于索引j处的元素
    private boolean less(int i, int j) {
        return items[pq[i]].compareTo(items[pq[j]])<0;

    }

    //交换堆中i索引和j索引处的值
    private void exch(int i, int j) {
        int tmp = pq[i];
        pq[i]=pq[j];
        pq[j]=tmp;

        qp[pq[i]]=i;
        qp[pq[j]]=j;
    }

    //判断k对应的元素是否存在
    public boolean contains(int k) {
        return pq[k]!=-1;
    }

    //最小元素关联的索引
    public int minIndex() {
        return pq[1];
    }


    //往队列中插入一个元素,并关联索引i
    public void insert(int i, T t) {
        if (contains(i)){
            throw new RuntimeException("存在");
        }
        N++;
        //item添加
        items[i]=t;
        //pq添加
        pq[N]=i;
        //qp添加
        qp[i]=N;
        swim(qp[i]);
    }

    //删除队列中最小的元素,并返回该元素关联的索引
    public int delMin() {
        int miniIndex = pq[1];
        exch(1,N);
        qp[pq[N]]=-1;
        pq[N]=-1;
        items[miniIndex]=null;
        N--;
        sink(1);
        return miniIndex;
    }

    //删除索引i关联的元素
    public void delete(int i) {
        int k = qp[i];
        exch(k,N);

        items[pq[k]]=null;
        qp[pq[i]]=-1;
        pq[N]=-1;
        N--;
        sink(i);
    }

    //把与索引i关联的元素修改为为t
    public void changeItem(int i, T t) {
        items[i]=t;
        int k=qp[i];
        swim(k);
        sink(k);
    }


    //使用上浮算法，使索引k处的元素能在堆中处于一个正确的位置
    private void swim(int k) {
        while (k>1){
            if (less(k,k/2)){
                exch(k,k/2);
            }
            k/=2;
        }
    }


    //使用下沉算法，使索引k处的元素能在堆中处于一个正确的位置
    private void sink(int k) {
        int min=0;
        while (k<N){
            if (less(2*k+1,2*k)){
                min=2*k;
            }else {
                min=2*k+1;
            }
            if (less(k,min)){
                break;
            }
            exch(k,min);
            k=min;
        }

    }

}

