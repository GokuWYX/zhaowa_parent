package com.wyx.graph;

import com.wyx.linear.Queue;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.graph
 * @ClassName: BFS
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/7 19:43
 * @Version: 1.0
 */
public class BreadthFirstSearch {
    public static void main(String[] args) {

    }
    //索引代表顶点，值表示当前顶点是否已经被搜索
    private boolean[] marked;

    //记录有多少个顶点与s顶点相通
    private int count;

    //用来存储待搜索邻接表的点
    private Queue<Integer> waitSearch;

    //构造广度优先搜索对象，使用广度优先搜索找出G图中s顶点的所有相邻顶点
    public BreadthFirstSearch(Graph G, int s) {
        this.marked = new boolean[G.V()];
        this.count=0;
        this.waitSearch = new Queue<Integer>();

        bfs(G,s);
    }

    //使用广度优先搜索找出G图中v顶点的所有相邻顶点
    private void bfs(Graph G, int v) {
        if (!marked[v]) {
            marked[v] = true;
            count++;
        }
        Queue<Integer> adj = G.adj(v);
        for (Integer w : adj) {
            if ((marked[w] == true)) {
                continue;
            } else {
                marked[w] = true;
                waitSearch.enqueue(w);
                count++;
            }
        }
        while (!waitSearch.isEmpty()) {
            Integer dequeue = waitSearch.dequeue();
            bfs(G, dequeue);
        }
    }
}
