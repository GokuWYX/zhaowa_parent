package com.wyx.zhaowa.queue;


import java.util.ArrayList;
import java.util.LinkedList;

public class LinkedListQueue_unCommitment {
    private int inIndex=0;
    private int outIndex=0;
    private int size=0;
    private int temSize=0;
    private LinkedList<Object> linkedList;
    //true is full ,false is empty
    private Boolean flag=false;

    public LinkedListQueue_unCommitment(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public int getTemSize() {
        return temSize;
    }

    public void inQueue(Object item){

        if (inIndex==outIndex && flag){
            throw new RuntimeException("queue is full");
        }
        linkedList.add(item);
        inIndex++;
        temSize++;
        if (temSize==size){
            //满了
            flag=true;
        }
    }
    public Object outQueue(){

        if (inIndex==outIndex && flag==false){
            throw new RuntimeException("queue is empty");
        }

        Object remove = linkedList.remove(inIndex);
        outIndex++;
        temSize--;
        if (temSize<=0){
            flag=false;
        }
        return remove;

    }



}
