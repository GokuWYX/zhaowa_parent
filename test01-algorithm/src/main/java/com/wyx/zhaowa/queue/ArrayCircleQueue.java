package com.wyx.zhaowa.queue;


public class ArrayCircleQueue {
    private Object[] items;
    private int intoIndex=0;
    private int outIndex=0;
    private int size=0;
    //t is full，f is empty
    private boolean flag;

    //构造
    public ArrayCircleQueue(int size) {
        items=new Object[size];
        setSize(size); }
    //进队列
    public void enqueue(Object item){
        if (intoIndex==size && flag){
            throw new RuntimeException("queue is full");
        }
        intoIndex=intoIndex%size;
        items[intoIndex]=item;
        intoIndex++;
        if (intoIndex==outIndex){
            flag=true;
        }
    }
    //出队列
    public Object dequeue(){
        if (outIndex==size && !flag){
            throw new RuntimeException("queue is empty");
        }
        outIndex=outIndex%size;
        Object item = items[outIndex];
        outIndex++;
        if (intoIndex==outIndex){
            flag=false;
        }
        return item;
    }

    //获得队列的空间大小
    public int getSize() {
        return size;
    }
    //通过此方法对队列的空间进行初始化
    public void setSize(int size) {
        this.size = size;
    }
}
