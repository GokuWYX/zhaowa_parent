package com.wyx.zhaowa.queue;

public class ArrayQueue {
    private Object[] items;
    private int size=0;
    private int inIndex=0;
    private int outIndex=0;

    //t is full，f is empty
    private Boolean flag=false;
    public ArrayQueue(int size) {
        this.items=new Object[size];
        setSize(size);
    }

    public void inQueue(Object item){
        if (inIndex==outIndex && flag){
            throw new RuntimeException("queue is full");
        }
        inIndex=inIndex%size;
        items[inIndex]=item;
        inIndex++;
        if (inIndex==outIndex){
            flag=true;
        }
    }
    public Object outQueue(){
        if (inIndex==outIndex && flag==false){
            throw new RuntimeException("queue is empty");
        }

        outIndex=outIndex%size;

        Object item =items[outIndex];
        outIndex++;
        if (inIndex==outIndex){
            flag=false;
        }

        return item;

    }




    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
