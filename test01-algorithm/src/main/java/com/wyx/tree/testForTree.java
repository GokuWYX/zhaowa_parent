//package com.wyx.tree;
//
//
//import com.wyx.linear.Queue;
//import org.junit.Test;
//
//
//
///**
// * @ProjectName: zhaowa_parent
// * @Package: test.tree
// * @ClassName: testForTree
// * @Author: GoKu
// * @Description: asd
// * @Date: 2022/3/6 15:07
// * @Version: 1.0
// */
//public class testForTree {
//
//
//
//    @Test
//    public void testErgodic(){
//        BinaryTree<String, String> bt = new BinaryTree<String,String>();
//        bt.put("E", "5");
//        bt.put("B", "2");
//        bt.put("G", "7");
//        bt.put("A", "1");
//        bt.put("D", "4");
//        bt.put("F", "6");
//        bt.put("H", "8");
//        bt.put("C", "3");
//        Queue<String> queue = bt.layerErgodic();
//        System.out.print("层序遍历");
//        System.out.println("");
//
//        for (Object o : queue) {
//            System.out.println(o+"="+bt.get((String) o));
//        }
//        System.out.println("");
//
//        System.out.println("前序遍历");
//        Queue<String> pre = bt.preErgodic();
//        for (Object o : pre) {
//            System.out.print(o);
//        }
//        System.out.println("");
//        System.out.println("");
//
//        System.out.println("中序遍历");
//        Queue<String> mid = bt.midErgodic();
//        for (Object o : mid) {
//            System.out.print(o);
//        }
//        System.out.println("");
//        System.out.println("");
//
//        System.out.println("后序遍历");
//        Queue<String> last = bt.afterErgodic();
//        for (Object o : last) {
//            System.out.print(o);
//        }
//        System.out.println("");
//        System.out.println("最大深度"+bt.maxDepth());
//
//    }
//    @Test
//    public void testCENGXU() {
//        BinaryTree<String, String> bt = new BinaryTree<>();
//        bt.put("E", "5");
//        bt.put("B", "2");
//        bt.put("G", "7");
//        bt.put("A", "1");
//        bt.put("D", "4");
//        bt.put("F", "6");
//        bt.put("H", "8");
//        bt.put("C", "3");
//
//        bt.testCengxu();
//
//
//    }
//}
