package com.wyx.tree;


import com.wyx.linear.Queue;

import java.util.ArrayList;


/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.tree
 * @ClassName: BinaryTree
 * @Author: Administrator
 * @Description: 二叉查找树
 * @Date: 2021/11/30 22:20
 * @Version: 1.0
 */


public class BinaryTree<Key extends Comparable<Key>, Value> {
    //记录根结点
    private Node root;
    //记录树中元素的个数
    private int N;

    public Node getRoot(){
        return root;
    }

    private class Node {
        //存储键
        public Key key;
        //存储值
        private Value value;
        //记录左子结点
        public Node left;
        //记录右子结点
        public Node right;

        public Node(Key key, Value value, Node left, Node right) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
        }
    }


    //获取树中元素的个数
    public int size() {
        return N;
    }

    /**
     * 插入方法put实现思想：
     * 1.如果当前树中没有任何一个结点，则直接把新结点当做根结点使用
     * 2.如果当前树不为空，则从根结点开始：
     * 2.1如果新结点的key小于当前结点的key，则继续找当前结点的左子结点；
     * 2.2如果新结点的key大于当前结点的key，则继续找当前结点的右子结点；
     * 2.3如果新结点的key等于当前结点的key，则树中已经存在这样的结点，替换该结点的value值即可。
     */
    //向树中添加元素key-value
    public void put(Key key, Value value) {
        root = put(root, key, value);
    }

    //向指定的树x中添加key-value,并返回添加元素后新的树
    private Node put(Node x, Key key, Value value) {
        //如果x子树为空，
        if (x==null){
            N++;
            return new Node(key,value, null,null);
        }

        //如果x子树不为空
        //比较x结点的键和key的大小：

        int cmp = key.compareTo(x.key);
        if (cmp>0){
            //如果key大于x结点的键，则继续找x结点的右子树
            x.right = put(x.right,key,value);

        }else if(cmp<0){
            //如果key小于x结点的键，则继续找x结点的左子树
            x.left = put(x.left,key,value);
        }else{
            //如果key等于x结点的键，则替换x结点的值为value即可
            x.value = value;
        }
        return x;
    }

    /**
     * 查询方法get实现思想：
     * 从根节点开始：
     * 1.如果要查询的key小于当前结点的key，则继续找当前结点的左子结点；
     * 2.如果要查询的key大于当前结点的key，则继续找当前结点的右子结点；
     * 3.如果要查询的key等于当前结点的key，则树中返回当前结点的value。
     */
    //查询树中指定key对应的value
    public Value get(Key key) {
        return get(root,key);
    }

    //从指定的树x中，查找key对应的值
    public Value get(Node x, Key key) {
        //x树为null
        if (x==null){
            return null;
        }

        //x树不为null

        //比较key和x结点的键的大小
        int cmp = key.compareTo(x.key);
        if (cmp>0){
            //如果key大于x结点的键，则继续找x结点的右子树
            return get(x.right,key);

        }else if(cmp<0){
            //如果key小于x结点的键，则继续找x结点的左子树
            return get(x.left,key);
        }else{
            //如果key等于x结点的键，就找到了键为key的结点，只需要返回x结点的值即可
            return x.value;
        }

    }

    /**
     * 删除方法delete实现思想：
     * 1.找到被删除结点；
     * 2.找到被删除结点右子树中的最小结点minNode
     * 3.删除右子树中的最小结点
     * 4.让被删除结点的左子树称为最小结点minNode的左子树，让被删除结点的右子树称为最小结点minNode的右子树
     * 5.让被删除结点的父节点指向最小结点minNode
     */
    //删除树中key对应的value
    public void delete(Key key) {
        delete(root, key);
    }

    //删除指定树x中的key对应的value，并返回删除后的新树
    public Node delete(Node x, Key key) {
        //x树为null
        if (x==null){
            return null;
        }

        //x树不为null
        int cmp = key.compareTo(x.key);
        if (cmp>0){
            //如果key大于x结点的键，则继续找x结点的右子树
            x.right = delete(x.right,key);

        }else if(cmp<0){
            //如果key小于x结点的键，则继续找x结点的左子树
            x.left = delete(x.left,key);
        }else{
            //如果key等于x结点的键，完成真正的删除结点动作，要删除的结点就是x；

            //让元素个数-1
            N--;
            //得找到右子树中最小的结点
            //1.如果当前结点的右子树不存在，则直接返回当前结点的左子结点
            if (x.right==null){
                return x.left;
            }

            //2.如果当前结点的左子树不存在，则直接返回当前结点的右子结点
            if (x.left==null){
                return x.right;
            }

            //3.当前结点的左右子树都存在
            //3.1找到右子树中最小的结点
            Node minNode = x.right;
            while(minNode.left!=null){
                minNode = minNode.left;
            }

            //3.2删除右子树中最小的结点
            Node n = x.right;
            while(n.left!=null){
                if (n.left.left==null){
                    n.left=null;
                }else{
                    //变换n结点即可
                    n = n.left;
                }
            }

            //让x结点的左子树成为minNode的左子树
            minNode.left = x.left;
            //让x结点的右子树成为minNode的右子树
            minNode.right = x.right;
            //让x结点的父结点指向minNode
            x = minNode;
        }
        return x;
    }

    //查找二叉树中最小的键
    public Key min(){
        return min(root).key;
    }

    //找出指定树x中最小的键所在的结点
    public Node min(Node x){
        if (x.left!=null){
            return min(x.left);
        }else {
            return x;
        }
    }

    //查找二叉树中最大的键
    public Key max(){
        return max(root).key;
    }
    //找出指定树x中最大的键所在的结点
    public Node max(Node x){
        if (x.right!=null){
            return max(x.right);
        }else {
            return x;
        }
    }

    /**
     * 前序遍历
     * 1.把当前结点的key放入到队列中;
     * 2.找到当前结点的左子树，如果不为空，递归遍历左子树
     * 3.找到当前结点的右子树，如果不为空，递归遍历右子树
     */

    //使用前序遍历，获取整个树中的所有键
     public Queue<Key> preErgodic(){
         Queue<Key> keys = new Queue<>();
         preErgodic(root,keys);
         return keys;
     }
    //使用前序遍历，把指定树x中的所有键放入到keys队列中
    public void preErgodic(Node x,Queue<Key> keys){
        if (x==null){
            return;
        }
        //1.把当前结点的key放入到队列中;
        keys.enqueue(x.key);
        //2.找到当前结点的左子树，如果不为空，递归遍历左子树
        if (x.left != null) {
            preErgodic(x.left, keys);
        }
        //3.找到当前结点的右子树，如果不为空，递归遍历右子树
        if (x.right != null) {
            preErgodic(x.right, keys);
        }
    }

    /**
     * 中序遍历
     * 1.找到当前结点的左子树，如果不为空，递归遍历左子树
     * 2.把当前结点的key放入到队列中;
     * 3.找到当前结点的右子树，如果不为空，递归遍历右子树
     *
     */
    //使用中序遍历，获取整个树中的所有键
    public Queue<Key> midErgodic(){
            Queue<Key> keys = new Queue<>();
            midErgodic(root,keys);
            return keys;
         }
         //使用中序遍历，把指定树x中的所有键放入到keys队列中
         private void midErgodic(Node x,Queue<Key> keys){
        if (x==null){
            return;
        }
        //1.找到当前结点的左子树，如果不为空，递归遍历左子树
        if (x.left!=null){
            midErgodic(x.left,keys);
        }

        //2.把当前结点的key放入到队列中;
        keys.enqueue(x.key);

        //3.找到当前结点的右子树，如果不为空，递归遍历右子树
        if (x.right!=null){
            midErgodic(x.right,keys);
        }
    }
    /**
     * 后序遍历
     * 1.找到当前结点的左子树，如果不为空，递归遍历左子树
     * 2.找到当前结点的右子树，如果不为空，递归遍历右子树
     * 3.把当前结点的key放入到队列中;
     */
    //使用后序遍历，获取整个树中的所有键
    public Queue<Key> afterErgodic(){
        Queue<Key> keys = new Queue<>();
        afterErgodic(root,keys);
        return keys;
    }
    //使用后序遍历，把指定树x中的所有键放入到keys队列中
    private void afterErgodic(Node x,Queue<Key> keys){
        if (x==null){
            return;
        }
        //1.找到当前结点的左子树，如果不为空，递归遍历左子树
        if (x.left!=null){
            afterErgodic(x.left,keys);
        }
        //2.找到当前结点的右子树，如果不为空，递归遍历右子树
        if (x.right!=null){
            afterErgodic(x.right,keys);

        }
        //3.把当前结点的key放入到队列中;
        keys.enqueue(x.key);
    }
    //层序遍历-----对于任何情况都可以使用的层序遍历，也是正规的层序遍历
//    public List<Key> layerErgodic() {
//        ArrayList<Key> keys = new ArrayList<>();
//        ArrayList<Node> nodes = new ArrayList<>();
//        nodes.add(root);
//        while (!nodes.isEmpty()){
//            for (int i = 0; i < nodes.size(); i++) {
//                Node node = nodes.remove(i);
//                keys.add(node.key);
//                if (node.left!=null){
//                    nodes.add(node.left);
//                }if (node.right!=null){
//                    nodes.add(node.right);
//                }
//            }
//
//        }
//        return keys;
//    }
    //层序遍历-----对于任何情况都可以使用的层序遍历，也是正规的层序遍历

    //使用层序遍历得到树中所有的键
    public Queue<Key> layerErgodic(){
        Queue<Key> keys = new Queue<>();
        Queue<Node> nodes = new Queue<>();
        nodes.enqueue(root);
        while(!nodes.isEmpty()){
            Node x = nodes.dequeue();
            keys.enqueue(x.key);
            if (x.left!=null){
                nodes.enqueue(x.left);
            }
            if (x.right!=null){
                nodes.enqueue(x.right);
            }
        }return keys;
    }

    //计算整个树的最大深度
    //递归每个节点的左右树，比较每个节点的左右两树的长度
    public int maxDepth(){
        return maxDepth(root);
    }
    //计算指定树x的最大深度
    private int maxDepth(Node x){
        if (x==null){
            return 0;
        }
        int maxL=0;
        int maxR=0;

        if (x.left!=null){
            maxL= maxDepth(x.left);
        }
        if (x.right!=null){
            maxR= maxDepth(x.right);
        }
        return maxL>maxR?maxL+1:maxR+1;
    }

    public void testCengxu(){
        //创建两个容器，一个存放Node，一个存放key
        Queue<Node> nodes = new Queue<>();

        nodes.enqueue(root);

        while (!nodes.isEmpty()){
            Node node = nodes.dequeue();
            System.out.println(node.key);

            if (node.left!=null){
                nodes.enqueue(node.left);
            }
            if (node.right!=null){
                nodes.enqueue(node.right);
            }
        }

    }



}










