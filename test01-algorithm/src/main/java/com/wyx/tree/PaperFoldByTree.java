package com.wyx.tree;

import com.wyx.linear.Queue;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.tree
 * @ClassName: PaperFoldByTree
 * @Author: GoKu
 * @Description: 折纸问题，通过二叉树模拟
 * @Date: 2022/3/16 14:32
 * @Version: 1.0
 */
public class PaperFoldByTree {
    public static void main(String[] args) {
        PaperFoldByTree tree = new PaperFoldByTree();
        Node node = tree.createTree(2);
        tree.printTree(node);

    }

    public void printTree(Node root){
        if (root==null){
            return;
        }
        if (root.left!=null){
            printTree(root.left);
        }
        System.out.println(root.item);
        if (root.right!=null){
            printTree(root.right);
        }
    }


    public  Node createTree(int h){
        if (h<=0 ){
            return null;
        }
        Node<String> root=null;
        for (int i = 0; i < h; i++) {
            if (i==0){
                root = new Node<>("down",null,null);
                continue;
            }
            Queue<Node<String>> queue = new Queue<>();
            queue.enqueue(root);
            while (!queue.isEmpty()){
                Node<String> node = queue.dequeue();
                if (node.left!=null){
                    queue.enqueue(node.left);
                }
                if (node.right!=null){
                    queue.enqueue(node.right);
                }
                if (node.left==null && node.right==null){
                    node.left=new Node<String>("down",null,null);
                    node.right=new Node<String>("up",null,null);
                }
            }

        }
        return root;
    }
    private class Node<T>{
        public T item;
        public Node left;
        public Node right;

        public Node(T item, Node left, Node right) {
            this.item = item;
            this.left = left;
            this.right = right;
        }
    }

}
