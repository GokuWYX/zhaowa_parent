package com.wyx.tree;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.tree
 * @ClassName: Paper
 * @Author: GoKu
 * @Description: 折纸问题
 * @Date: 2022/3/12 11:14
 * @Version: 1.0
 */
public class PaperFolding {
    public static void printPaperFoldingMark(int N){
        mid(N,1,true);
    }
    private static void mid(int n,int floor,boolean down){
        if (floor>n){return;}
        mid(n,floor+1,true);
        System.out.print((down?"down":"up")+"\t");
        mid(n,floor+1,false);
    }

    public static void main(String[] args) {
        printPaperFoldingMark(1);
        System.out.println();
        printPaperFoldingMark(2);
        System.out.println();
        printPaperFoldingMark(3);
        System.out.println();
        printPaperFoldingMark(4);
    }

}
