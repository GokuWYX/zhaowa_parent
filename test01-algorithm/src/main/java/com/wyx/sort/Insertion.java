package com.wyx.sort;

public class Insertion implements Sort{
    /*
    *   1.把所有的元素分为两组，已经排序的和未排序的；
        2.找到未排序的组中的第一个元素,插入到已经排序的组中的最后一个元素(已排序组数量+1)；
        3.倒叙遍历已经排序的元素，依次倒叙将每两个元素进行比较，如果前一个大于后一个就交换，负责继续将下表前移
          那么就把待插入元素放到这个位置，其他的元素向后移动一位
    */
    //排序方法
    public void sort(Comparable[] a){
        for (int i = 1; i <= a.length-1; i++) {
            //当前元素为a[i],依次和i前面的元素比较，找到一个小于等于a[i]的元素
            for (int j = i; j > 0; j--) {
                if (greater(a[j-1],a[j])){
                    swap(a,j-1,j);
                }else {
                    break;
                }
            }
        }
    }
    //两值比较
    private static boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    //交换方法
    private static void swap(Comparable[] a,int i,int j){
        Comparable tem=a[i];
        a[i]=a[j];
        a[j]=tem;
    }
}