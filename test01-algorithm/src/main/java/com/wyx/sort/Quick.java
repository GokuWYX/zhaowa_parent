package com.wyx.sort;

import java.util.Arrays;

public class Quick implements Sort{
    public static void main(String[] args) {

        Comparable[] a = {1,9, 2, 8, 5, 4, 7, 3};
        Quick quick = new Quick();
        quick.sort(a);

        System.out.println(Arrays.toString(a));
    }
    //排序方法
    public  void sort(Comparable[] a){
        int lo=0;
        int hi=a.length-1;
        sort(a,lo,hi);
    }
    public void sort(Comparable[] a,int lo,int hi){
        if (lo>=hi){
            return;
        }
        //对a数组中，从lo到hi的元素进行切分
        int partition = partition(a, lo, hi);

        //对左边分组的元素排序
        sort(a,lo,partition-1);
        sort(a,partition+1,hi);
    }
    public int partition(Comparable[] a,int lo, int hi){
        //把最左边的元素当做基准值
        Comparable key=a[lo];
        //定义一个左侧指针，初始指向最左边的元素
        int left=lo;
        //定义一个右侧指针，初始指向左右侧的元素下一个位置
        int right=hi+1;
        //开始切分
        while (true){
            //先从右往左扫描，找到一个比基准值小的元素就循环停止
            while (greater(a[--right],key)){
                if (right==lo){
                    break;
                }
            }
            //再从左往右扫描，找一个比基准值大的元素就循环停止
            while (greater(key,a[++left])){
                if (left==hi){
                    break;
                }
            }

            if (left>=right){
                //扫描完了所有元素，结束循环
                break;
            }else {
                //交换left和right索引处的元素
                swap(a,left,right);
            }
        }
        //交换最后right索引处和基准值所在的索引处的值
        swap(a, lo, right);
        //right就是切分的界限
        return right;
    }


    //两值比较
    private static boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    //交换方法
    private static void swap(Comparable[] a,int i,int j){
        Comparable tem=a[i];
        a[i]=a[j];
        a[j]=tem;
    }

}
