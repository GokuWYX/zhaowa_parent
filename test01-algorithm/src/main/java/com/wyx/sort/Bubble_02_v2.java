package com.wyx.sort;


import java.util.Arrays;

/**
 * @ProjectName: heima
 * @Package: com.wyx.basic_01
 * @ClassName: Bubble_02_v2
 * @Author: GoKu
 * @Description: 冒泡排序的升级版本
 * @Date: 2022/2/21 8:34
 * @Version: 1.0
 */
public class Bubble_02_v2 implements Sort {



        public static void swap(Comparable [] a,int j, int k){
            Comparable tmp=a[j];
            a[j]=a[k];
            a[k]=tmp;
        }

    @Override
    public void sort(Comparable[] a) {
        int n=a.length-1;
        while (true){
            int lastIndex=0;
            for (int i = 0; i <n; i++) {
                if (a[i].compareTo(a[i+1])>0){
                    swap(a,i,i+1);
                    lastIndex=i;
                }
            }
            n=lastIndex;
            if (n==0){
                break;
            }
        }
    }
}
