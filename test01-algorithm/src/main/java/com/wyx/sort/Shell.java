package com.wyx.sort;

public class Shell implements Sort{
    /*
    *   1.选定一个增长量h，按照增长量h作为数据分组的依据，对数据进行分组；
        2.对分好组的每一组数据完成插入排序；
        3.减小增长量，最小减为1，重复第二步操作。*/


    //排序方法
    public void sort(Comparable[] a){
        int h=1;
        int N=a.length;

        while (h<N/2){
            h=h*2+1;
        }
        //当增长量h小于1，排序结束
        while (h>=1){
            //找到待插入的元素
            for (int i = h; i <N ; i++) {
                //a[i]就是待插入的元素
                //把a[i]插入到a[i-h],a[i-2h],a[i-3h]...序列中

                for (int j = i; j >= h; j-=h) {

                    //a[j]就是待插入元素，依次和a[j-h],a[j-2h],a[j-3h]进行比较，如果a[j]小，那么
                    // 交换位置，如果不小于，a[j]大，则插入完成。

                    if (greater(a[j-h],a[j])){
                        swap(a,j,j-h);
                    }
                    else{
                        //break语句在循环结构终止本层循环体，从而提前结束本层循环。因为j-h前面的元素肯定比j-h小，前面的元素就算没有排序也要默认排序好了
                        break;
                    }
                }
            }
            h/=2;
        }
    }
    //两值比较
    private static boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    //交换方法
    private static void swap(Comparable[] a,int i,int j){
        Comparable tem=a[i];
        a[i]=a[j];
        a[j]=tem;
    }
}
