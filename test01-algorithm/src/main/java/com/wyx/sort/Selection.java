package com.wyx.sort;

public class Selection implements Sort{
    //排序方法
    public void  sort(Comparable[] a){
         /*1.每一次遍历的过程中，都假定第一个索引处的元素是最小值，和其他索引处的值依次进行比较，
             如果当前索引处的值大于其他某个索引处的值，则假定其他某个索引出的值为最小值，最后可以找到最小值所在的索引
           2.交换第一个索引处和最小值所在的索引处的值*/
        for (int i = 0; i < a.length-1; i++) {
            //假定本次遍历，最小值所在的索引是i
            int minIndex=i;
            for (int j = i+1; j < a.length; j++) {
                if (greater(a[minIndex],a[j])){
                    //交换最小值所在的索引
                    minIndex=j;
                }
            }
            swap(a,i,minIndex);
        }
    }
    //两值比较
    private static boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    //交换方法
    private static void swap(Comparable[] a,int i,int j){
        Comparable tem=a[i];
        a[i]=a[j];
        a[j]=tem;
    }

}
