package com.wyx.sort;


public class Bubble implements Sort {

    /*
     * 冒泡的核心思路:找到最大的元素放到最后面，依次便利下去
     * 两两对比，大的放后面，直到对比到最后一个，最后一个就是最大的，
     * 下一次对比就无需再次比较最后一个元素
     * */
    //排序方法
    public void sort(Comparable[] a) {
        for (int i = a.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (greater(a[j], a[j + 1])) {
                    swap(a, j, j + 1);
                }
            }
        }
    }

    //两值比较
    private static boolean greater(Comparable a, Comparable b) {
        return a.compareTo(b) > 0;
    }

    //交换方法
    private static void swap(Comparable[] a, int i, int j) {
        Comparable tem = a[i];
        a[i] = a[j];
        a[j] = tem;
    }
}
