package com.wyx.sort;

import java.util.Arrays;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.sort
 * @ClassName: Insertion_05
 * @Author: GoKu
 * @Description: 插入排序的第二种方案
 * @Date: 2022/2/21 15:04
 * @Version: 1.0
 */
public class Insertion_05 implements Sort{



    //交换方法
    private static void swap(Comparable[] a,int i,int j){
        Comparable tem=a[i];
        a[i]=a[j];
        a[j]=tem;
    }

    @Override
    public void sort(Comparable[] a) {
        // i 代表待插入元素的索引
        for (int i = 1; i < a.length; i++) {
            Comparable t = a[i]; // 代表待插入的元素值
            int j = i;
            //System.out.println(j);
            while (j >= 1) {
                if (t.compareTo(a[j - 1])<0) { // j-1 是上一个元素索引，如果 > t，后移
                    a[j] = a[j - 1];
                    j--;
                } else { // 如果 j-1 已经 <= t, 则 j 就是插入位置
                    break;
                }
            }
            a[j] = t;
            //System.out.println(Arrays.toString(a) + " " + j);
        }
    }
}
