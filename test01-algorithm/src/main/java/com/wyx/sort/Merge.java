package com.wyx.sort;

public class Merge implements Sort {
    //归并所需要的辅助数组
    private static Comparable[] assist;


    //排序方法
    public void sort(Comparable[] a){
        //1.初始化辅助数组assist；
        assist=new Comparable[a.length];
        //2.定义一个lo变量，和hi变量，分别记录数组中最小的索引和最大的索引；
        int lo=0;
        int hi=a.length-1;
        //3.调用sort重载方法完成数组a中，从索引lo到索引hi的元素的排序
        sort(a,lo,hi);
    }

    //对数组a中从lo到hi的元素进行排序
    public  void sort(Comparable[] a,int lo,int hi){
        if (hi<=lo){
            return;
        }
        //对lo到hi之间的数据进行分为两个组
        int mid=lo+(hi-lo)/2;

        //对lo到mid之间元素进行排序
        sort(a,lo,mid);

        //对mid+1到hi之间元素进行排序
        sort(a,mid+1,hi);

        //对lo到mid这组数据和mid到hi这组数据进行归并
        merge(a,lo,mid,hi);
    }

    //对数组中，从lo到mid为一组，从mid+1到hi为一组，对这两组数据进行归并
    public static void merge(Comparable[] a,int lo,int mid,int hi){
        //从lo到mid这组数据和mid+1到hi这组数据归并到辅助数组assist对应的所引处

        //定义一个指针，指向assist数据中开始填充数据的索引
        int i=lo;
        //定义一个指针，指向第一组数据的第一个元素
        int p1=lo;
        //定义一个指针，指向第二组数据的第一个元素
        int p2=mid+1;

        //比较左边小组和右边小组中元素大小，哪个小就把哪个数据填充到assist数组中
        //遍历，移动p1指针和p2指针，比较对应索引处的值，找出小的那个，放到辅助数组的对应索引处
        while (p1<=mid && p2<=hi){
            if (greater(a[p1],a[p2])){
                assist[i++]=a[p2++];
            }else {
                assist[i++]=a[p1++];
            }
        }
        //遍历，如果p1的指针没有走完，那么顺序移动p1指针，把对应的元素放到辅助数组的对应索引处
        while (p1<=mid){
            assist[i++]=a[p1++];
        }

        //遍历，如果p2的指针没有走完，那么顺序移动p2指针，把对应的元素放到辅助数组的对应索引处
        while (p2<=hi){
            assist[i++]=a[p2++];
        }
        //把辅助数组中的元素拷贝到原数组中
        for (int index = lo; index <= hi; index++) {
            a[index]=assist[index];
        }

    }

    //两值比较
    private static boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    //交换方法
    private static void swap(Comparable[] a,int i,int j){
        Comparable tem=a[i];
        a[i]=a[j];
        a[j]=tem;
    }

}
