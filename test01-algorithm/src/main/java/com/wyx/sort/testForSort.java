package com.wyx.sort;

import org.junit.Test;

public class testForSort {
    Integer[] a={9,7,8,4,6,5,2,3,1};

    @Test
    public void testForBubble(){
        testSelect(new Bubble());
    }
    @Test
    public void testForSelection(){
        testSelect(new Selection());
    }
    @Test
    public void testForInsertion(){
        testSelect(new Insertion());
    }
    @Test
    public void testForShell(){
        testSelect(new Shell());
    }

    @Test
    public void testForMerge(){
        testSelect(new Merge());
    }

    @Test
    public void testForQuick(){
        testSelect(new Quick());
    }

    @Test
    public void testForBigData(){
//        //冒泡排序
//        testForBigData(new Bubble());
//        //新版冒泡
//        testForBigData(new Bubble_02_v2());
//
//        //插入排序
//        testForBigData(new Insertion());
//
//        //插入排序增强版
//        testForBigData(new Insertion_05());
//
//
//        //选择排序
//        testForBigData(new Selection());
//        //归并排序
//        testForBigData(new Merge());
        //希尔排序
        testForBigData(new Shell());
//        //快速排序
//        testForBigData(new Quick());
    }


    public void testSelect(Sort sort){
        sort.sort(a);
        System.out.println(sort.getClass().getName());
        for (Integer i : a) {
            System.out.print(i +"  ");
        }
    }
    //测试十万逆序数据排序为正序
    public void testForBigData(Sort sort){
        Integer[] b=new Integer[100000];
        Integer max=99999;
        for (int i = 0; i <100000; i++) {
            b[i]=max--;
        }
        long start = System.currentTimeMillis();
        sort.sort(b);
        long end = System.currentTimeMillis();
        //使用时间
        long time = end - start;
        System.out.println(sort.getClass().getName()+"用时"+time+"s");
    }


}
