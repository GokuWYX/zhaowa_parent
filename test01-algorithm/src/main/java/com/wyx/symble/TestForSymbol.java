//package com.wyx.symble;
//
//import org.junit.Test;
//
///**
// * @ProjectName: zhaowa_parent
// * @Package: com.wyx.symble
// * @ClassName: TestForSymbol
// * @Author: Administrator
// * @Description: 各类符号表的集中测试类
// * @Date: 2021/11/30 10:16
// * @Version: 1.0
// */
//public class TestForSymbol {
//    @Test
//    public void testSymbolTable(){
//        SymbolTable<Integer, String> symboltable = new SymbolTable<Integer, String>();
//        symboltable.put(0,"0");
//        symboltable.put(1,"1");
//        symboltable.put(2,"2");
//
//
//        System.out.println("0是"+symboltable.get(0));
//        System.out.println("1是"+symboltable.get(1));
//        System.out.println("2是"+symboltable.get(2));
//        System.out.println("删除后");
//        symboltable.delete(2);
//        System.out.println("2是"+symboltable.get(2));
//    }
//    @Test
//    public void testOrderSymbokTable(){
//        OrderSymbolTable<Integer, String> symbolTable = new OrderSymbolTable<>();
//        symbolTable.put(4,"4");
//        symbolTable.put(0,"0");
//        symbolTable.put(1,"1");
//        symbolTable.put(3,"3");
//        symbolTable.put(20,"20");
//        symbolTable.put(15,"15");
//
//    }
//
//}
