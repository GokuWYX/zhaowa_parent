package com.wyx.linear;

import org.junit.Test;

public class TestForLinear {
    @Test
    public void testForSequenceList(){
        SequenceList<String> strings = new SequenceList<String>(5);
        //测试未插入元素时元素个数
        System.out.println(strings.length());

        //尾插
        strings.insert("GoKu00");
        strings.insert("GoKu01");
        strings.insert("GoKu02");
        strings.insert("GoKu03");

        //顺序定向插入
        strings.insert(2,"Mu");

        //非定向插入
        //java.lang.RuntimeException: insert index is disorder
        //strings.insert(5,"翔");

        System.out.println("此顺序表长度为"+strings.length());

        //超越容量插入测试插入
        //java.lang.RuntimeException: insert index is illegal
        //strings.insert(17,"我就想看看插不插的进去");

        //测试删除
        System.out.println(strings.remove(1).toString());

        //测试通过下标获得值
        for (int i = 0; i < strings.length(); i++) {
            System.out.println(i+":"+strings.get(i).toString());
        }

        //测试遍历
        for (String s : strings) {
            System.out.println(s);
        }
        strings.clear();
        System.out.println("清除之后的长度: "+strings.length());

        //SequenceList<String> strings1 = new SequenceList<>(4);
        //测试容量变化
        System.out.println("一开始内部容器大小"+strings.getCapacity());
        strings.insert("1");
        strings.insert("1");
        strings.insert("1");
        strings.insert("1");
        strings.insert("1");
        strings.insert("1");
        strings.insert("1");
        System.out.println("插入7个元素后内部容器大小"+strings.getCapacity());
        strings.remove(1);
        strings.remove(1);
        strings.remove(1);
        strings.remove(1);
        strings.remove(1);
        strings.remove(1);
        System.out.println("移除6个元素后内部容器大小"+strings.getCapacity());



    }
    @Test
    public void testForLinkedListSingle(){
        LinkedListSingle linkedListSingle = new LinkedListSingle<String>();
        linkedListSingle.insert("0");
        linkedListSingle.insert("1");
        linkedListSingle.insert("3");
        linkedListSingle.insert("3");
        linkedListSingle.insert("4");

        linkedListSingle.insert(2,"2");
        System.out.println("坐标为2的item为"+linkedListSingle.get(2));
        System.out.println("判断是否为空："+linkedListSingle.isEmpty());
        System.out.println("此单链表长度"+linkedListSingle.length());
        linkedListSingle.remove(2);
        System.out.println("坐标为2的item为"+linkedListSingle.get(2));
        linkedListSingle.insert(3,"2");
        System.out.println(linkedListSingle.indexOf("2"));
        System.out.println("反转之前");
        for (Object node : linkedListSingle) {
            System.out.println(node.toString());
        }
        linkedListSingle.reverse();
        System.out.println("反转之后----------------");
        for (Object node : linkedListSingle) {
            System.out.println(node.toString());
        }

    }

    @Test
    public void testForLinkedListDouble(){
        LinkedListDouble linkedListDouble = new LinkedListDouble<String>();
        linkedListDouble.insert("0");
        linkedListDouble.insert("1");
        linkedListDouble.insert("3");
        linkedListDouble.insert("4");
        linkedListDouble.insert("5");

        linkedListDouble.insert(2,"2");
        linkedListDouble.insert(6,"6");
        System.out.println("坐标为2的item为"+linkedListDouble.get(2));

        System.out.println("判断是否为空："+linkedListDouble.isEmpty());
        System.out.println("此单链表长度"+linkedListDouble.length());

        System.out.println("坐标为2的item为"+linkedListDouble.get(2));
        System.out.println("首个元素为："+linkedListDouble.getFirst());
        System.out.println("尾个元素为："+linkedListDouble.getLast());
        //System.out.println("移除的元素为："+linkedListDouble.remove(4));
        System.out.println("移除的元素为："+linkedListDouble.remove(6));

        System.out.println("便利开始");
        for (Object node : linkedListDouble) {
            System.out.println(node.toString());
        }
        System.out.println("元素为3的元素下标是："+linkedListDouble.indexOf("3"));
    }


    @Test
    public void testForStack(){
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(0);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        System.out.println("便利开始");
        for (Object o : stack) {
            System.out.println(o);
        }
        stack.pop();
        System.out.println("便利开始");
        for (Object o : stack) {
            System.out.println(o);
        }
    }

    @Test
    public void testForQueue(){
        Queue<String> queue = new Queue<>();
        queue.enqueue("0");
        queue.enqueue("1");
        queue.enqueue("2");
        System.out.println("出队前大小"+queue.size());
        System.out.println("队列是否为空"+queue.isEmpty());
        System.out.println("出队元素"+queue.dequeue());
        System.out.println("出队后大小"+queue.size());
        for (Object msg : queue) {
            System.out.println(msg);
        }
    }


}
