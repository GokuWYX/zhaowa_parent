package com.wyx.linear;

import java.util.Stack;

public class SymbolMatch {


/*      1.创建一个栈用来存储左括号
        2.从左往右遍历字符串，拿到每一个字符
        3.判断该字符是不是左括号，如果是，放入栈中存储
        4.判断该字符是不是右括号，如果不是，继续下一次循环
        5.如果该字符是右括号，则从栈中弹出一个元素t；
        6.判断元素t是否为null，如果不是，则证明有对应的左括号，如果不是，则证明没有对应的左括号
        7.循环结束后，判断栈中还有没有剩余的左括号，如果有，则不匹配，如果没有，则匹配*/

    public static void main(String[] args) {
        String str1="(fdafds(fafds)())";
        String str2="(fdafds(fafds)()";
        System.out.println(issMatch(str1));
        System.out.println(issMatch(str2));
    }
    public static boolean issMatch(String str){
        //1.创建一个栈用来存储左括号
        Stack<String> stack = new Stack<>();

        //2.从左往右遍历字符串，拿到每一个字符
        for (int i = 0; i < str.length(); i++) {
            String tmp = str.charAt(i) + "";

            //3.判断该字符是不是左括号，如果是，放入栈中存储
            if (tmp.equals("(")){
                stack.push(tmp);

                //4.判断该字符是不是右括号，如果不是，继续下一次循环
            //5.如果该字符是右括号，则从栈中弹出一个元素t；
            }else  if (tmp.equals(")")){

                String t = stack.pop();
                //6.判断元素t是否为null，如果不是，则证明有对应的左括号，如果不是，则证明没有对应的左括号
                if (t==null){
                    return false;
                }
            }
        }
        //7.循环结束后，判断栈中还有没有剩余的左括号，如果有，则不匹配，如果没有，则匹配
        if (stack.size()==0){
            return true;
        }else {
            return false;
        }
    }
}