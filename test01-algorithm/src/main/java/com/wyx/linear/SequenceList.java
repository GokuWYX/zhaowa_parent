package com.wyx.linear;


import java.util.Iterator;

public class SequenceList<T> implements Iterable<T>{
    //存储元素的数组
    private T[] elements;

    //记录当前顺序表中的元素个数
    private int N;

    //构造方法
    public  SequenceList(int capacity){
        elements=(T[]) new Object[capacity];
        N=0;
    }

    //将一个线性表置为空表
    public void clear(){
        N=0;
    }

    //判断是否为空
    public boolean isEmpty(){
        return N==0;
    }

    //获取线性表的长度
    public int length(){
        return N;
    }



    //获取指定位置的元素
    public T get(int i){
        if (i<0 || i>N-1){
            throw  new RuntimeException("index is illegal");
        }else {
            return elements[i];
        }
    }



    //向线性表中添加元素t
    public void insert(T t){

        if (N==getCapacity()){
            //元素已经放满了数组，需要扩容
            resize(getCapacity()*2);
        }else {
            elements[N++]=t;
        }
    }



    //向线性表中指定下标添加元素t
    public void insert(int i,T t){
        if (N==getCapacity()){
            //元素已经放满了数组，需要扩容
            resize(getCapacity()*2);
        }
        if (i<0 ){
            throw new RuntimeException("insert index is illegal");
        }
        if (i>N){
            throw new RuntimeException("insert index is disorder");
        }


        for (int index = N; index > i ; index--) {
            elements[index]=elements[index-1];
        }
        //把t放到i位置处
        elements[i]=t;
        //元素数量+1
        N++;
    }
    //删除指定位置i处的元素，并返回改元素
    public T remove(int i){
        if (i<0 || i>N){
            throw new RuntimeException("delete index is illegal");
        }
        //待删除元素备份
        T result = elements[i];
        //把i位置后面的元素向前移动一位
        for (int index = i; index < N-1; index++) {
            elements[index]=elements[index+1];
        }
        //当前元素数量-1
        N--;
        if (N>0 && N<getCapacity()/4){
            resize(getCapacity()/2);
        }

        return result;
    }
    //根据元素查找下标
    public int indexOf(T t){
        if (t==null){
            throw new RuntimeException("element is illegal");
        }
        for (int i = 0; i < N; i++) {
            if (elements[i].equals(t)){
                return i;
            }
        }
        return -1;
    }

    //打印当前线性表的元素
    public void showElements(){
        for (int i = 0; i < N; i++) {
            System.out.print(elements[i]+"");
        }
        System.out.println();
    }



    //顺序表的遍历
    @Override
    public Iterator<T> iterator() {
        return new SIterator();
    }

    private class SIterator implements Iterator{
        private int cur;

        public SIterator(){
            this.cur=0;
        }


        @Override
        public boolean hasNext() {
            return cur<N;
        }

        @Override
        public Object next() {
            return elements[cur++];
        }
    }

    //顺序表的容量修改
    private void resize(int newSize){
        //记录旧数组
        T[] tmp=elements;
        //创建新数组
        elements = (T[]) new Object[newSize];
        //把旧数据放入新数组
        for (int i = 0; i < N; i++) {
            elements[i]=tmp[i];
        }
    }
    //获得容器容量大小
    public int getCapacity(){
        return elements.length;
    }



}