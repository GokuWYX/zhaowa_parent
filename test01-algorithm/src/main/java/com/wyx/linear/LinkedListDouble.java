package com.wyx.linear;


import java.util.Iterator;

public class LinkedListDouble<T> implements Iterable<T> {
    //首节点
    private Node head;

    //最后一个节点
    private Node last;

    //链表的长度
    private int N;

    public LinkedListDouble() {
        last= null;
        head = new Node(null,null,null);
        N = 0;
    }

    //清空链表
    public void clear(){
        last=null;
        head.next=last;
        head.pre=null;
        head.item=null;
        N=0;
    }
    //获取链表长度
    public int length(){
        return N;
    }

    //判断链表是否为空
    public boolean isEmpty(){
        return N==0;
    }

    //插入元素t
    public void insert(T t){
        if (last==null){
            last = new Node(head, t,null);
            head.next=last;
        }else {
            Node oldLast=last;
            Node node = new Node(oldLast, t, null);
            oldLast.next=node;
            last=node;
        }
        //长度+1
        N++;
    }
    //向指定位置i插入元素t
    public  void insert(int i,T t){
        if (i<0 || i>N){
            throw new RuntimeException("illegal index");
        }

        //找到位置i的前一个节点
        Node pre = head;
        for (int index = 0; index < i; index++) {
            pre=pre.next;
        }
        if (i==N){
            insert(t);
        }else {
            //当前节点
            Node curr=pre.next;
            //构建新节点
            Node newNode = new Node(pre, t, curr);
            curr.pre=newNode;
            pre.next=newNode;
            //长度+1
            N++;
        }


    }

    //获取指定下标元素
    public T get(int i){
        if (i<0 || i>=N){
            throw new RuntimeException("illegal index");
        }
        Node result=head.next;
        for (int j = 0; j < i; j++) {
            result=result.next;
        }
        return result.item;
    }
    //删除i处的元素，并返回此元素
    public T remove(int i){
        if (i<0 || i>=N){
            throw new RuntimeException("illegal index");
        }
        //i位置的前一个元素
        Node pre=head;
        for (int j = 0; j < i; j++) {
            pre =pre.next;
        }
        T result;
        if (i==N-1){
            result=last.item;
            last=last.pre;
            last.next=null;
            N--;

        }else {
            //i位置的元素
            Node curr = pre.next;
            result=curr.item;

            //i位置的下一个元素
            Node nextNode=curr.next;
            pre.next=nextNode;
            nextNode.pre=pre;
            N--;
        }


        return result;
    }

    //获取第一个元素
    public T getFirst(){
        if (isEmpty()){
            return null;
        }
        return head.next.item;
    }

    //获得最后一个元素
    public T getLast(){
        if (isEmpty()){
            return null;
        }
        return last.item;
    }


    //根据元素获得节点下标
    public int indexOf(T t){
        int result=-1;
        Node node=head;
        for (int i = 0; node.next!=null ; i++) {
            node=node.next;
            if (node.item.equals(t)){
                result=i;
                break;

            }

        }

        return result;
    }



    @Override
    public Iterator<T> iterator() {
        return new TIterator();
    }
    public class TIterator implements Iterator{
        private Node n=head;

        @Override
        public boolean hasNext() {
            return n.next!=null;
        }

        @Override
        public Object next() {
            n=n.next;
            return n.item;
        }
    }


    //节点类
    private class Node{
        public Node(Node pre,T item,Node next){
            this.item=item;
            this.pre=pre;
            this.next=next;
        }

        //存储数据
        public T item;

        //指向上一个节点
        public Node pre;

        //指向下一个节点
        public Node next;
    }



}
