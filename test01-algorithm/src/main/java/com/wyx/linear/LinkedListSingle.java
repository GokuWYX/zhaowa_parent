package com.wyx.linear;

import java.util.Iterator;

public class LinkedListSingle<T> implements Iterable<T>{
    //头结点
    private Node head;
    //记录链表的长度
    private int N;

    public LinkedListSingle() {
        head=new Node(null,null);
        N = 0;
    }

    //清空链表
    public void clear(){
        head.next=null;
        head.item=null;
        N=0;
    }

    //获取链表长度
    public int length(){
        return N;
    }

    //判断是否为空
    public boolean isEmpty(){
        return N==0;
    }

    //获取指定位置的元素
    public T get(int i){
        if (i<0 || i>=N){
            throw new RuntimeException("illegal index ");
        }
        Node n = head.next;
        for (int index = 0; index < i; index++) {
            n=n.next;
        }

        return n.item;
    }

    //向链表添加元素t
    public void insert(T t){
        //找到最后一个节点
        Node n = head;
        while (n.next != null) {
            n=n.next;
        }

        n.next=new Node(t,null);
        //链表长度+1
        N++;
    }
    //指定下标插入
    public void insert(int i,T t){
        if (i<0 || i>=N){
            throw new RuntimeException("illegal index ");
        }
        Node pre = this.head;
        //寻找i前面的那个元素
        for (int index=0;index<i;index++){
            pre=pre.next;
        }

        //i位置未替换的节点

        Node cur = pre.next;
        //构建新的结点，让新结点指向位置i的结点
        Node newNode = new Node(t,cur);

        //让之前的结点指向新结点
        pre.next = newNode;
        //长度+1
        N++;
    }
    //删除指定位置i处的元素，并返回被删除的元素
    public T remove(int i){
        if (i<0 || i>=N){
            throw new RuntimeException("illegal index ");
        }
        Node pre = head;
        //寻找i前面的那个元
        for (int index=0;index<i;index++){
            pre=pre.next;
        }
        Node cur = pre.next;
        //前一个结点指向下一个结点，删除当前结点
        pre.next=cur.next;
        //长度-1
        N--;
        return cur.item;
    }
    //查找元素t在链表中第一次出现的位置
    public int indexOf(T t){
        Node node=head;
        for (int i = 0; node.next!=null; i++) {

            node=node.next;
            if (node.item.equals(t)){
                return i;
            }
        }
        return -1;

    }



    //结点类
    private class Node{
        //数据
        T item;

        //下一个节点
        Node next;

        public Node(T item, Node next) {
            this.item = item;
            this.next = next;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new LIterator();
    }
    private class LIterator implements Iterator<T>{
        private Node n;

        public LIterator() {
            this.n=head;
        }

        @Override
        public boolean hasNext() {
            return n.next!=null;
        }

        @Override
        public T next() {
            n=n.next;
            return n.item;
        }
    }

    //用来反转整个链表
    public void reverse(){

        //判断当前链表是否为空链表，如果是空链表，则结束运行，如果不是，则调用重载的reverse方法完成反转
        if (isEmpty()){
            return;
        }

        reverse(head.next);
    }

    /**
     *
     * @param curr 当前遍历的结点
     * @return 反转后当前结点上一个结点
     */
    //反转指定的结点curr，并把反转后的结点返回
    public Node reverse(Node curr){
        if (curr.next==null){
            head.next=curr;
            return curr;
        }
        //递归的反转当前结点curr的下一个结点；返回值就是链表反转后，当前结点的上一个结点
        Node pre = reverse(curr.next);
        //让返回的结点的下一个结点变为当前结点curr；
        pre.next=curr;
        //把当前结点的下一个结点变为null
        curr.next=null;
        return curr;
    }


}
