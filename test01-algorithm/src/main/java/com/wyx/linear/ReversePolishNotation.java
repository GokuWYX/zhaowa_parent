package com.wyx.linear;
import  java.util.Stack;
public class ReversePolishNotation {
    /*
    *   1.创建一个栈对象oprands存储操作数
        2.从左往右遍历逆波兰表达式，得到每一个字符串
        3.判断该字符串是不是运算符，如果不是，把该该操作数压入oprands栈中
        4.如果是运算符，则从oprands栈中弹出两个操作数o1,o2
        5.使用该运算符计算o1和o2，得到结果result
        6.把该结果压入oprands栈中
        7.遍历结束后，拿出栈中最终的结果返回
    * */


    public static void main(String[] args) {
        //中缀表达式3*（17-15）+18/6的逆波兰表达式如下
        String[] notation = {"3", "17", "15", "-", "*","18", "6","/","+"};
        int result = caculate(notation);
        System.out.println("逆波兰表达式的结果为："+result);
    }


    /**
     *
     * @param notaion
     * @return
     */
    public static int caculate(String[] notaion){
        Stack<Integer> oprands = new Stack<Integer>();
        for (int i = 0; i <notaion.length ; i++) {
            String curr = notaion[i];
            Integer o1;
            Integer o2;
            Integer result;
            switch (curr){
                case "+":
                    o1=oprands.pop();
                    o2=oprands.pop();
                    result=o2+o1;
                    oprands.push(result);
                    break;
                case "-":
                    o1=oprands.pop();
                    o2=oprands.pop();
                    result=o2-o1;
                    oprands.push(result);
                    break;
                case "*":
                    o1=oprands.pop();
                    o2=oprands.pop();
                    result=o1*o2;
                    oprands.push(result);
                    break;
                case "/":
                    o1=oprands.pop();
                    o2=oprands.pop();
                    result=o2/o1;
                    oprands.push(result);
                    break;
            }


        }
        return oprands.pop();
    }
}