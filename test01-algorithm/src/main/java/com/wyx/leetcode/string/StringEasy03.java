package com.wyx.leetcode.string;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.string
 * @ClassName: StringEasy03
 * @Author: Goku
 * @Description: 字符串的统计字符串
 * @Date: 2021/12/2 10:02
 * @Version: 1.0
 */
public class StringEasy03 {
    /**
     * 【题目】
     * 给定一个字符串 str，返回str 的统计字符串。例如，"aaabbadddffc"的统计字符串为
     * "a_3_b_2_a_1_d_3_f_2_c_1"。
     * 补充问题：给定一个字符串的统计字符串cstr，再给定一个整数index，返回cstr 所代表的
     * 原始字符串上的第index 个字符。例如，"a_1_b_100"所代表的原始字符串上第0 个字符是'a'，
     * 第50 个字符是'b'。
     * 【难度】
     * 士 ★☆☆☆
     * 【解答】
     * 原问题。解决原问题的方法有很多，本书仅提供一种供读者参考。具体过程如下：
     * 1．如果str 为空，那么统计字符串不存在。
     * 2．如果str 不为空。首先生成String 类型的变量res，表示统计字符串，还有整型变量num，
     * 代表当前字符的数量。初始时字符串res 只包含str 的第0 个字符（str[0]），同时num=1。
     * 3．从str[1]位置开始，从左到右遍历str，假设遍历到i 位置。如果str[i]==str[i-1]，说明当
     * 前连续出现的字符（str[i-1]）还没结束，令num++，然后继续遍历下一个字符。如果str[i]!=str[i-1]，
     * 说明当前连续出现的字符（str[i-1]）已经结束，令res=res+"_"+num+"_"+str[i]，然后令num=1，
     * 继续遍历下一个字符。以题目给出的例子进行说明，在开始遍历"aaabbadddffc"之前，res="a"，
     * num=1。遍历str[1~2]时，字符'a'一直处在连续的状态，所以num 增加到3。遍历str[3]时，字
     * 符'a'连续状态停止，令res=res+"_"+"3"+"_"+"b"（即"a_3_b"），num=1。遍历str[4]，字符'b'在连
     * 续状态，num 增加到2。遍历str[5]时，字符'a'连续状态停止，令res 为"a_3_b_2_a"，num=1。
     * 依此类推，当遍历到最后一个字符时，res 为"a_3_b_2_a_1_d_3_f_2_c"，num=1。
     * 4．对于步骤3 中的每一个字符，无论是连续还是不连续，都是在发现一个新字符时再将这
     * 个字符连续出现的次数放在res 的最后。当遍历结束时，最后字符的次数还没有放入res，所以，
     * 最后令res=res+"_"+num。在步骤3 的例子中，当遍历结束时，res 为"a_3_b_2_a_1_d_3_f_2_c"，
     * num=1，最后需要把num 加在res 后面，令res 变为"a_3_b_2_a_1_d_3_f_2_c_1"，然后再返回。
     */

}
