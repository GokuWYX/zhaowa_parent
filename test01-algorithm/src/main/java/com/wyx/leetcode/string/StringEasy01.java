package com.wyx.leetcode.string;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.string
 * @ClassName: TestStringEasy01
 * @Author: Administrator
 * @Description: 判断两个字符串是否互为变形词
 * @Date: 2021/12/1 17:15
 * @Version: 1.0
 */
public class StringEasy01 {
    /**
     * 【题目】
     * 给定两个字符串str1 和str2，如果str1 和str2 中出现的字符种类一样且每种字符出现的次
     * 数也一样，那么str1 与str2 互为变形词。请实现函数判断两个字符串是否互为变形词。
     * 【举例】
     * str1="123"，str2="231"，返回true。
     * str1="123"，str2="2331"，返回false。
     * 【难度】
     * ★☆☆☆
     * 【解答】
     * 如果字符串str1 和str2 长度不同，直接返回false。如果长度相同，假设出现字符的编码值
     * 在0~255 之间，那么先申请一个长度为256 的整型数组map，map[a]=b 代表字符编码为a 的字
     * 符出现了b 次，初始时map[0..255]的值都是0。然后遍历字符串str1，统计每种字符出现的数
     * 量，比如遍历到字符'a'，其编码值为97，则令map[97]++。这样map 就成了str1 中每种字符的
     * 词频统计表。然后遍历字符串str2，每遍历到一个字符，都在map 中把词频减下来，比如遍历
     * 到字符'a'，其编码值为97，则令map[97]--，如果减少之后的值小于0，直接返回false。如果遍
     * 历完str2，map 中的值也没出现负值，则返回true。
     */
    public static void main(String[] args) {
        String a = "a2344565687697890";
        System.out.println(isDeformation(a, a));
    }

    public static boolean isDeformation(String str1, String str2) {
        if (str1 == null || str2 == null || str1.length() == 0 || str2.length() == 0) {
            return false;
        }
        char[] chars1 = str1.toCharArray();
        char[] chars2 = str2.toCharArray();
        int[] map = new int[256];
        for (int i = 0; i < chars1.length; i++) {
            map[chars1[i]]++;
        }
        for (int i = 0; i < chars2.length; i++) {
            if (map[chars2[i]]-- == 0) {
                return false;
            }
        }
        return true;
    }

}
