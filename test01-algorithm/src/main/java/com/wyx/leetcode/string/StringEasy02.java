package com.wyx.leetcode.string;

import java.math.BigInteger;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.string
 * @ClassName: StringEasy02
 * @Author: Administrator
 * @Description:将整数字符串转成整数值
 * @Date: 2021/12/1 19:09
 * @Version: 1.0
 */
public class StringEasy02 {
    /**
     * 【题目】
     * 给定一个字符串str，如果str 符合日常书写的整数形式，并且属于32 位整数的范围，返回
     * str 所代表的整数值，否则返回0。
     * 【举例】
     * str="123"，返回123。
     * str="023"，因为"023"不符合日常的书写习惯，所以返回0。
     *str="A13"，返回0。
     * str="0"，返回0。
     * str="2147483647"，返回2147483647。
     * str="2147483648"，因为溢出了，所以返回0。
     * str="-123"，返回-123。
     * 【难度】
     * 尉 ★★☆☆
     * 【解答】
     * 解决本题的方法有很多，本书仅提供一种供读者参考。首先检查str 是否符合日常书写的整
     * 数形式，具体判断如下：
     * 1．如果str 不以“-”开头，也不以数字字符开头，例如，str=="A12"，返回false。
     * 2．如果str 以“-”开头，但是str 的长度为1，即str=="-"，返回false。如果str 的长度大
     * 于1，但是“-”的后面紧跟着“0”，例如，str=="-0"或"-012"，返回false。
     * 3．如果str 以“0”开头，但是str 的长度大于1，例如，str=="023"，返回false。
     * 4．如果经过步骤1~步骤3 都没有返回，接下来检查str[1..N-1]是否都是数字字符，如果有
     * 一个不是数字字符，则返回false。如果都是数字字符，说明str 符合日常书写，返回true。
     */
    public static void main(String[] args) {
        StringEasy02 stringEasy02 = new StringEasy02();
        System.out.println(stringEasy02.convert("23424") - 12);
        System.out.println(Integer.MIN_VALUE);
    }

    public  boolean isValid(char[] chars){
        //chars个数是否合法
        if (chars.length==0){
            return false;
        }
        //  字符不合法
        if (chars[0] != '-' && (chars[0] < '0' || chars[0]>'9')){
            return false;
        }
        // - 或者  -013  或者 -0
        if (chars[0]=='-' && (chars.length==1 || chars[1]=='0')){
            return false;
        }
        // 023  012  043
        if (chars[0]=='0' && chars.length >1){
            return false;
        }
        //循环检测每个字符是不是合法的
        for (int i = 0; i < chars.length; i++) {
            if (chars[i]<'0' || chars[i]>'9'){
                return false;
            }
        }
        return true;
    }
    //如果 str 不符合日常书写的整数形式，根据题目要求，直接返回0 即可。如果符合，则进行以下

    /**
     *  1．生成4 个变量。
     * 布尔型常量posi，表示转换的结果是负数还是非负数，这完全由str 开头的字符决定，如果以“-”开头，那么转换的结果一定是负数，则posi 为false，否则posi 为true。
     * 整型常量minq，minq 等于Integer.MIN_VALUE/10，即32 位整数最小值除以10 得到的商，其意义稍后说明。
     * 整型常量minr，minr 等于Integer.MIN_VALUE%10，即32 位整数最小值除以10 得到的余数，其意义稍后说明。
     * 整型变量res，转换的结果，初始时res=0。
     *
     *
     * 2．32 位整数的最小值为-2147483648，32 位整数的最大值为2147483647。可以看出，最小值的绝对值比最大值的绝对值大1，所以转换过程中的绝对值一律以负数的形式出现，然后
     * 根据posi 决定最后返回什么。比如str="123"，转换完成后的结果是-123，posi=true，所以最后返回123。再如str="-123"，转换完成后的结果是-123，posi=false，所以最后返回-123。比如 str="-2147483648"，转换完成后的结果是-2147483648，posi=false，所以最后返回-2147483648。
     *
     * 比如str="2147483648"，转换完成后的结果是-2147483648，posi=true，此时发现-2147483648 变
     * 成2147483648 会产生溢出，所以返回0。也就是说，既然负数比正数拥有更大的绝对值范围，
     * 那么转换过程中一律以负数的形式记录绝对值，最后再决定返回的数到底是什么。
     *
     *
     *  3．如果str 以'-'开头，从str[1]开始从左往右遍历str，否则从str[0]开始从左往右遍历str。
     * 举例说明转换过程，比如str="123"，遍历到'1'时， res=res*10+(-1)==-1，遍历到'2'时，
     * res=res*10+(-2)==-12，遍历到'3'时，res=res*10+(-3)==-123。比如str="-123"，字符'-'跳过，从字
     * 符'1' 开始遍历， res=res*10+(-1)==-1 ， 遍历到'2' 时， res=res*10+(-2)==-12 ， 遍历到'3' 时，res=res*10+(-3)==-123。
     *
     * ★★★★★★★★★★★★★★
     * 遍历的过程中如何判断res 已经溢出了？假设当前字符为a，那么'0'-a
     * 就是当前字符所代表的数字的负数形式，记为cur。如果在res 加上cur 之前，发现res 已经小
     * 于minq，那么res 加上cur 之后一定会溢出，比如str="3333333333"，遍历完倒数第二个字符后，
     * res==-333333333 < minq==-214748364，所以当遍历到最后一个字符时，res*10 肯定会产生溢出。
     * 如果在res 加上cur 之前，发现res 等于minq，但又发现cur 小于minr，那么res 加上cur 之后
     * 一定会溢出，比如str="2147483649"，遍历完倒数第二个字符后，res==-214748364 == minq，当
     * 遍历到最后一个字符时发现有res==minq，同时也发现cur== -9 < minr==-8，那么res 加上cur 之
     * 后一定会溢出。出现任何一种溢出情况时，直接返回0。
     *
     *
     *  4．遍历后得到的res 根据posi 的符号决定返回值。如果posi 为true，说明结果应该返回正，
     * 否则说明应该返回负。如果res 正好是32 位整数的最小值，同时又有posi 为true，说明溢出，直接返回0。
     * 全部过程请参看如下代码中的convert 方法。
     */
    public  int convert(String str) {
        if (str == null || str.equals("")) {
            return 0; // 不能转
        }
        char[] chas = str.toCharArray();
        if (!isValid(chas)) {
            return 0; // 不能转
        }
        boolean posi = chas[0] == '-' ? false : true;
        int minq = Integer.MIN_VALUE / 10;
        int minr = Integer.MIN_VALUE % 10;
        int res = 0;
        int cur = 0;
        for (int i = posi ? 0 : 1; i < chas.length; i++) {
            cur = '0' - chas[i];
            if ((res < minq) || (res == minq && cur < minr)) {
                return 0; // 不能转
            }
            res = res * 10 + cur;
        }
        if (posi && res == Integer.MIN_VALUE) {
            return 0; // 不能转
        }
        return posi ? -res : res;
    }


}
















    // 这是错误的，2021年12月2日 这个日期你的能力自己能想到的代码就这质量
    //思路  找到string或者chars的legth然后while
/*    public static int my(String a) {

        char[] chars = a.toCharArray();

        int length = chars.length;
        int[] tmp=new int[length];
        int i=0;
        while (length>0){
            int value = Character.getNumericValue(chars[i]);

            //最后一个数字无需乘10
            for (int j = length; j >1; j--) {
                value*=10;
            }
            tmp[i]=value;
            i++;
            length--;
        }
        BigInteger result= new BigInteger("0");
        BigInteger maxInt= new BigInteger("2147483648");

        for (int i1 : tmp) {
            result=result.add(new BigInteger(String.valueOf(i1)));
        }

        if (result.max(maxInt).equals(result)){
            return 0;
        }else {

            return result.intValue();

        }}*/


















