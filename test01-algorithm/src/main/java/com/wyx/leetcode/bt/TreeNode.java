package com.wyx.leetcode.bt;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bt
 * @ClassName: TreeNode
 * @Author: GoKu
 * @Description:
 * @Date: 2022/3/2 22:22
 * @Version: 1.0
 */
public class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode() {}
      TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
}
