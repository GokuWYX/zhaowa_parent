package com.wyx.leetcode.bt;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bt
 * @ClassName: bt01
 * @Author: GoKu
 * @Description: 回溯算法
 * @Date: 2022/3/2 22:22
 * @Version: 1.0
 */
public class bt01 {
    private void dfs(List<String> res, TreeNode node, StringBuilder s) {

        s.append(node.val);
        if (node.left == null && node.right == null) {
            res.add(s.toString());
            return;
        }
        if (node.left != null) {
            dfs(res, node.left, new StringBuilder(s).append("->"));
        }
        if (node.right != null) {
            dfs(res, node.right, new StringBuilder(s).append("->"));
        }





    }
}