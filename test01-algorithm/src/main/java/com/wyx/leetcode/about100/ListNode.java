package com.wyx.leetcode.about100;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-10-21 16:48
 * @description:
 **/
public class ListNode {

    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) {
        this.val = val;
    }
    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}