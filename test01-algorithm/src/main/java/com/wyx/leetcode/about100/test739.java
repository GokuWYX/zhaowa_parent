package com.wyx.leetcode.about100;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.about100
 * @ClassName: test739
 * @Author: GoKu
 * @Description: 给定一个整数数组 temperatures ，表示每天的温度，
 * 返回一个数组 answer ，其中 answer[i] 是指对于第 i 天，下一个更高温度出现在几天后。
 * 如果气温在这之后都不会升高，请在该位置用 0 来代替。
 * @Date: 2022/6/30 8:43
 * @Version: 1.0
 */
public class test739 {
    public int[] dailyTemperatures(int[] temperatures) {
        int length = temperatures.length;

        int[] answer=new int[length];
        for (int i = 0; i <= length-1; i++) {
            int days =0;

            for (int j =i+1; j <= length-1; j++) {
                if (temperatures[i]>=temperatures[j]){
                    days++;
                }else {
                    days++;
                    answer[i]=days;
                    break;
                }
            }

        }
        answer[length-1]=0;
        return answer;
    }
}


class a {

    public static void main(String[] args) {
        test739 test739 = new test739();
        int[] a={73,74,75,71,69,72,76,73};
        int[] ints = test739.dailyTemperatures(a);
        for (int anInt : ints) {
            System.out.print(anInt+"  ");
        }
    }
        }