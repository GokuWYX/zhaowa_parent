package com.wyx.leetcode.about100;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.about100
 * @ClassName: test560
 * @Author: GoKu
 * @Description:给你一个整数数组 nums 和一个整数 k ，请你统计并返回 该数组中和为 k 的连续子数组的个数 。
 * @Date: 2022/7/15 11:02
 * @Version: 1.0
 */
public class test560 {
    public int subarraySum(int[] nums, int k) {
        int result=0;
        for (int i = 0; i < nums.length; i++) {
            int count=0;
            for (int j=i;j>=0;j--){
                count+=nums[j];
                if (count==k){
                    result ++;
                }
            }
        }
        return result;
    }
}

