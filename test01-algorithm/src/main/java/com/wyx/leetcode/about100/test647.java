package com.wyx.leetcode.about100;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.about100
 * @ClassName: test647
 * @Author: GoKu
 * @Description:给你一个字符串 s ，请你统计并返回这个字符串中 回文子串 的数目。
 *
 * 回文字符串 是正着读和倒过来读一样的字符串。
 *
 * 子字符串 是字符串中的由连续字符组成的一个序列。
 *
 * 具有不同开始位置或结束位置的子串，即使是由相同的字符组成，也会被视作不同的子串。
 *
 *
 * @Date: 2022/7/1 10:31
 * @Version: 1.0
 */
public class test647 {
    public int countSubstrings(String s) {
        int result=0;
        for (int i = 0; i < s.length(); i++) {
            for (int j = i; j < s.length(); j++) {
                if (i==j){
                    result++;
                }else if (s.charAt(i)==s.charAt(j)){
                    int startIndex=i;
                    int endIndex=j;

                    while (s.charAt(startIndex)==s.charAt(endIndex)){
                        startIndex++;
                        endIndex--;
                        if (startIndex>=endIndex){
                            result++;
                            break;
                        }
                    }
                }
            }
        }

        return result;

    }
}

class testa647{
    public static void main(String[] args) {
        test647 test647 = new test647();
        System.out.println(test647.countSubstrings("abc"));
    }
}
