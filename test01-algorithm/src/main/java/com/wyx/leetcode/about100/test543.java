package com.wyx.leetcode.about100;

import java.util.ArrayList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.about100
 * @ClassName: test543
 * @Author: GoKu
 * @Description:
 * @Date: 2022/7/21 9:04
 * @Version: 1.0
 */
public class test543 {
    int ans=0;
    public int diameterOfBinaryTree(TreeNode root) {
        getMax(root);
        return ans;
    }

    public int getMax(TreeNode node){
        if (node==null){
            return 0;
        }
        int left = getMax(node.left);
        int right = getMax(node.right);
        ans=Math.max(ans,left+right);
        return Math.max(left,right)+1;
    }



}


