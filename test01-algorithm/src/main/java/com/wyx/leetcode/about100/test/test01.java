package com.wyx.leetcode.about100.test;

import com.sun.jmx.snmp.SnmpNull;
import com.sun.xml.internal.messaging.saaj.soap.GifDataContentHandler;

import java.util.HashMap;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.about100.test
 * @ClassName: test01For01
 * @Author: GoKu
 * @Description: 第一题
 * @Date: 2022/4/2 14:19
 * @Version: 1.0
 */
public class test01 {
    public static void main(String[] args) {

    }
}
class test01For01{
    public int[] twoSunm(int[] nums,int target){
        HashMap<Integer,Integer> table = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (table.containsKey(target-nums[i])){
                return new int[]{table.put(nums[i],i)};
            }
            table.put(nums[i],i);
        }
        return new int[-1];
    }
}
class test01For02{
    public int[] a(int[] nums,int target){
        HashMap<Integer,Integer> hashMap=new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (hashMap.containsKey(target-nums[i])){
                return new int[]{hashMap.get(target-nums[i]),i};
            }
                hashMap.put(nums[i],i);

        }
        return new int[-1];
    }
}
class test01For03{
    public int[] getIndex(int[] nums ,int target){
        HashMap<Integer,Integer> hashMap =new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if(hashMap.containsKey(target-nums[i])){
                return new int[]{hashMap.get(target-nums[i]),i};
            }
            hashMap.put(nums[i],i);
        }
        return new int[-1];
    }
}
class test01For04{
    public int[] getindex(int[] nums ,int target){
        HashMap<Integer,Integer> hash=new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (hash.containsKey(target- nums[i])){
                return new int[]{hash.get(target-nums[i]),i};
            }
            hash.put(nums[i],i);
        }
        return new int[-1];
    }
}
class test01For05{
    public int[] getIndwex(int[] a,int target){
        HashMap<Integer,Integer> b=new HashMap<>();
        for (int i = 0; i < a.length; i++) {
            if (b.containsKey(target-a[i])){
                return new int[]{i,b.get(target-a[i])};
            }
            b.put(a[i],i);
        }
        return new int[-1];
    }
}
class test01For06 {
    public int[] getIOndex(int[] nums ,int target) {
        HashMap<Integer,Integer> hashMap=new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            if (hashMap.containsKey(target-nums[i])){
                return new int[]{i,hashMap.get(target-nums[i])};
        }
            hashMap.put(nums[i],i);
    }
        return new int[-1];
    }
}
class test01For07{
    public int[] getIndex(int[] nums,int target){
        HashMap<Integer,Integer> hashMap=new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (hashMap.containsKey(target-nums[i])){
                return new int[]{i,hashMap.get(target-nums[i])};
            }
            hashMap.put(nums[i],i);
        }
        return new int[-1];
    }
}

class test01For08{
    public int[] getIndex(int[] nums,int target){
        HashMap<Integer,Integer> a=new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (a.containsKey(target-nums[i]))
            {
                return new int[]{i,a.get(target-nums[i])};
            }
            a.put(nums[i],i);
        }
        return new int[-1];
    }
}
class test01For09{
    public int[] getIndex(int[] nums,int target){

        HashMap<Integer,Integer> hashMap=new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (hashMap.containsKey(target-nums[i])){
                return new int[]{i,hashMap.get(nums[i])};
            }
            hashMap.put(nums[i],i);
        }

        return new int[-1];
    }
}


class test01For10{
    public int[] getIndex(int[] nums ,int taget){
        HashMap<Integer,Integer> hashMap=new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (hashMap.containsKey(taget-nums[i])){
                return new int[]{i,hashMap.get(taget-nums[i])};
            }
            hashMap.put(nums[i],i);
        }
        return new int[-1];
    }
}



























