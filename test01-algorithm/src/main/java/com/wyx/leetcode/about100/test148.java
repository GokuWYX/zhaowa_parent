package com.wyx.leetcode.about100;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-10-21 16:45
 * @description:给你链表的头结点 head ，请将其按 升序 排列并返回 排序后的链表 。
 * 输入：head = [4,2,1,3]
 * 输出：[1,2,3,4]
 *
 * 示例 2：
 *
 * 输入：head = [-1,5,3,4,0]
 * 输出：[-1,0,3,4,5]
 *
 * 示例 3：
 *
 * 输入：head = []
 * 输出：[]
 **/
public class test148 {

    public static void main(String[] args) {
        ListNode node = new ListNode(4);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(1);
        ListNode node3 = new ListNode(3);


        node.next=node1;
        node1.next=node2;
        node2.next=node3;
        Solution solution = new Solution();
        solution.sortList(node);
        ListNode head=node;



        while (head.next!=null){
            System.out.println(head.val);
            head=head.next;
        }




//
//        swap(node,node1);
//        System.out.println(node.val);
//        System.out.println(node1.val);
//        System.out.println(node2.val);
//        System.out.println(node3.val);



    }    public static void swap(ListNode a,ListNode b){
        int valA = a.val;
        a.val=b.val;
        b.val=valA;
    }
}

class Solution {
    public ListNode sortList(ListNode head) {
        return sortList(head,null);
    }

    public ListNode sortList(ListNode head, ListNode tail) {
        if (head==null){
            return head;
        }

        if (head.next==tail){
            head.next=null;
            return head;
        }
        ListNode slow=head,fast=head;

        while (fast!=tail){
            slow=slow.next;
            fast=fast.next;
            if (fast.next!=tail){
                fast=fast.next;
            }
        }
        ListNode node01 = sortList(head, slow);
        ListNode node02 = sortList(slow,fast);
        return merge(node01,node02);
    }

    public ListNode merge(ListNode node1,ListNode node2) {
        ListNode resultNode = new ListNode(0);
        while (node1.next!=null && node2.next!=null){
            if (node1.val<node2.val){
                resultNode.val=node1.val;
                node1=node1.next;
            }else {
                resultNode.val=node1.val;
                node2=node2.next;
            }
            resultNode=resultNode.next;
        }
        if (node1.next!=null){
            resultNode.next=node1;
        }else if(node2.next!=null){
            resultNode.next=node2;
        }


        return resultNode.next;
    }
}






/**
 * 下面这种复杂度太高，无法使用
 */

//class Solution {
//    public ListNode sortList(ListNode head) {
//        boolean br=false;
//        ListNode node = head;
//
//        while (node.next!=null){
//
//            if (br){
//                node=head;
//                br=false;
//                continue;
//            }
//            if (node.val>node.next.val){
//                br=true;
//                swap(node,node.next);
//            }
//            node=node.next;
//
//        }
//
//
//        return head;
//
//    }
//
//    public void swap(ListNode a,ListNode b){
//        int valA = a.val;
//        a.val=b.val;
//        b.val=valA;
//    }
//
//}

