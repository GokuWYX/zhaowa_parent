package com.wyx.leetcode.about100;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.about100
 * @ClassName: test461
 * @Author: GoKu
 * @Description:两个整数之间的 汉明距离 指的是这两个数字对应二进制位不同的位置的数目。
 *
 * 给你两个整数 x 和 y，计算并返回它们之间的汉明距离。
 * @Date: 2022/7/29 11:01
 * @Version: 1.0
 */
public class test461 {
    public int hamingDistance(int x,int y){
        int s=x^y,ret=0;
        while (s != 0) {

            ret+=s&1;
            s>>= 1;
        }
        return ret;
    }


    //get
    //1010 0101



}
