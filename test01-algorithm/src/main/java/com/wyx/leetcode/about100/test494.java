package com.wyx.leetcode.about100;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.about100
 * @ClassName: test494
 * @Author: GoKu
 * @Description:给你一个整数数组 nums 和一个整数 target 。
 *
 * 向数组中的每个整数前添加 '+' 或 '-' ，然后串联起所有整数，可以构造一个 表达式 ：
 *
 *     例如，nums = [2, 1] ，可以在 2 之前添加 '+' ，在 1 之前添加 '-' ，然后串联起来得到表达式 "+2-1" 。
 *
 * 返回可以通过上述方法构造的、运算结果等于 target 的不同 表达式 的数目。
 *

 * @Date: 2022/7/26 15:38
 * @Version: 1.0
 */
public class test494 {
    int count =0;
    public int getNum(int[] nums,int target){
        backtrack(nums,target,0,0);
        return count;
    }
    public void  backtrack(int[] nums,int target,int index,int sum){
        if (index==nums.length) {
            if (target==sum) {
                count++;
            }
        }else {
            backtrack(nums,target,index+1,sum+nums[index]);
            backtrack(nums,target,index+1,sum-nums[index]);
        }
    }
}







