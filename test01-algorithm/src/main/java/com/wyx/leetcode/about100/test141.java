package com.wyx.leetcode.about100;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.about100
 * @ClassName: test141
 * @Author: Gogoku
 * @Description:
 *给你一个链表的头节点 head ，判断链表中是否有环。
 * 如果链表中有某个节点，可以通过连续跟踪 next 指针再次到达，则链表中存在环。 为了表示给定链表中的环，
 * 评测系统内部使用整数 pos 来表示链表尾连接到链表中的位置（索引从 0 开始）。注意：pos 不作为参数进行传递 。仅仅是为了标识链表的实际情况。
 * 如果链表中存在环 ，则返回 true 。 否则，返回 false 。
 *
 * 输入：head = [3,2,0,-4], pos = 1
 * 输出：true
 * 解释：链表中有一个环，其尾部连接到第二个节点
 *
 *
 * @Date: 2022/9/8 16:47
 * @Version: 1.0
 */
public class test141 {
    public boolean hasCycle(ListNode_A head) {
        //创建一个
        HashSet<Integer> nums = new HashSet<>();
        ListNode_A node=head;
        while(node.next!=null){
            if (nums.add(node.val)) {
                node=node.next;
            }else {
                return true;
            }
        }
        return false;
    }



    @Test
    public void testHashSet(){

        HashSet<Integer> integers = new HashSet<>();
        System.out.println(integers.add(3));
        System.out.println(integers.add(3));

    }



}


class ListNode_A {

    int val;

    ListNode_A next;

    ListNode_A(int x) {
        val = x;
        next = null;
    }
}
