//package com.wyx.leetcode.about100;
//
//import java.util.HashMap;
//
///**
// * @ProjectName: zhaowa_parent
// * @Package: com.wyx.leetcode.about100
// * @ClassName: test01
// * @Author: GoKu
// * @Description:
// * @Date: 2022/4/1 15:23
// * @Version: 1.0
// */
//public class test01 {
//    public static void main(String[] args) {
//       int[] nums ={2,7,11,15};
//       int targety=9;
//        Solution01 solution = new Solution01();
//        for (int i : solution.twoSum(nums, targety)) {
//            System.out.println(i);
//        }
//        Solution01 solution01 = new Solution01();
//        for (int i : solution01.twoSum(nums, targety)) {
//            System.out.println(i);
//        }
//    }
//}
////啥办法，无需动脑
//class Solution01 {
//    public int[] twoSum(int[] nums, int target) {
//        for (int i = 0; i < nums.length; i++) {
//            if (nums[i]>target){
//                continue;
//            }
//
//            for (int j = i+1; j < nums.length; j++) {
//
//                if (nums[i]+nums[j]==target){
//                    int[] ints = {i,j};
//                    return ints;
//                }
//
//            }
//
//        }
//        System.out.println("妈的没有重复的");
//        return null;
//    }
//}
////聪明人的选择，也就可以骗骗面试官了
//class Solution11{
//    public int[] twoSum(int[] nums, int target) {
//        HashMap<Integer, Integer> hashtable = new HashMap<>();
//        for (int i = 0; i < nums.length; ++i) {
//            if (hashtable.containsKey(target-nums[i])){
//                return new int[]{hashtable.get(target-nums[i]),i};
//            }
//            hashtable.put(nums[i],i);
//        }
//        return new int[-1];
//    }
//}