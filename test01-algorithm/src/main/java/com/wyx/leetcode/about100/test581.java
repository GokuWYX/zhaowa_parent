package com.wyx.leetcode.about100;

import java.util.Arrays;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.about100
 * @ClassName: test581
 * @Author: GoKu
 * @Description:给你一个整数数组 nums ，你需要找出一个 连续子数组 ，如果对这个子数组进行升序排序，那么整个数组都会变为升序排序。
 * @Date: 2022/7/11 14:50
 * @Version: 1.0
 */
public class test581 {
    public int findUnsortedSubarray(int[] nums) {
        if (scan(nums)){
            return 0;
        }
        int[] ok=new int[nums.length];
        System.arraycopy(nums,0,ok,0,nums.length);
        Arrays.sort(ok);
        int begin=0;
        int last=nums.length-1;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i]!=ok[i]){
                begin=i;
                break;
            }
        }
        for (int i=nums.length-1; i>=0 ;i--){
            if (nums[i]!=ok[i]){
                last=i;
                break;
            }

        }

        return last-begin;
    }

    public boolean scan(int[] nums){
        for (int i = 1; i < nums.length; i++) {
            if (nums[i-1]>nums[i]){
                    return false;
            }
        }
        return true;
    }


    public static void main(String[] args) {

    }
}