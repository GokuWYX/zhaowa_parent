package com.wyx.leetcode.about100;

import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.about100
 * @ClassName: test139
 * @Author: Gogoku
 * @Description:
 * 给你一个字符串 s 和一个字符串列表 wordDict 作为字典。请你判断是否可以利用字典中出现的单词拼接出 s 。
 *
 * 注意：不要求字典中出现的单词全部都使用，并且字典中的单词可以重复使用。
 *
 *
 *
 * 示例 1：
 *
 * 输入: s = "leetcode", wordDict = ["leet", "code"]
 * 输出: true
 * 解释: 返回 true 因为 "leetcode" 可以由 "leet" 和 "code" 拼接成。
 *
 * 示例 2：
 *
 * 输入: s = "applepenapple", wordDict = ["apple", "pen"]
 * 输出: true
 * 解释: 返回 true 因为 "applepenapple" 可以由 "apple" "pen" "apple" 拼接成。
 *      注意，你可以重复使用字典中的单词。
 *
 * 示例 3：
 *
 * 输入: s = "catsandog", wordDict = ["cats", "dog", "sand", "and", "cat"]
 * 输出: false
 *
 *
 *
 * 提示：
 *
 *     1 <= s.length <= 300
 *     1 <= wordDict.length <= 1000
 *     1 <= wordDict[i].length <= 20
 *     s 和 wordDict[i] 仅有小写英文字母组成

 * @Date: 2022/9/9 9:36
 * @Version: 1.0
 */
public class test139 {
    public boolean wordBreak(String s, List<String> wordDict) {
        int indexx=0;
        //便利s的每一个字符
        while (indexx<=s.length()-1){

            for (String s1 : wordDict) {
                for (int i = 0; i < s1.length(); i++) {
                    int start=indexx;

                    if (indexx==s.length()-1){
                        return true;
                    }

                    if (s.charAt(indexx)==s1.charAt(i)){
                        start++;
                    }else if (i==s1.length()){
                        //负责删除index前几位的值
                        s.substring(indexx);
                    }

                }
            }
        }



    return false;
    }

    public static void main(String[] args) {
        String a="abcdefg";
        System.out.println(a.substring(2));
    }

}
