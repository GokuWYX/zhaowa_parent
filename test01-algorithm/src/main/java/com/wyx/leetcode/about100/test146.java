package com.wyx.leetcode.about100;

import javax.print.attribute.standard.NumberUp;
import java.util.LinkedHashMap;
import java.util.Set;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-10-16 14:43
 * @description: 请你设计并实现一个满足  LRU (最近最少使用) 缓存 约束的数据结构。 实现 LRUCache 类：      LRUCache(int capacity) 以 正整数 作为容量 capacity 初始化 LRU 缓存     int get(int key) 如果关键字 key 存在于缓存中，则返回关键字的值，否则返回 -1 。     void put(int key, int value) 如果关键字 key 已经存在，则变更其数据值 value ；如果不存在，则向缓存中插入该组 key-value 。如果插入操作导致关键字数量超过 capacity ，则应该 逐出 最久未使用的关键字。  函数 get 和 put 必须以 O(1) 的平均时间复杂度运行。
 **/
public class test146 {

    public static void main(String[] args) {
        LRUCache lruCache = new LRUCache(2);
        lruCache.put(1,1);
        System.out.println("null");
        lruCache.put(2,2);
        System.out.println("null");
        System.out.println(lruCache.get(1));

        lruCache.put(3,3);
        System.out.println("null");
        System.out.println(lruCache.get(2));
        lruCache.put(4,4);
        System.out.println("null");
        System.out.println(lruCache.get(1));
        System.out.println(lruCache.get(3));
        System.out.println(lruCache.get(4));

//        LinkedHashMap<Integer,Integer> linkedHashMap = new LinkedHashMap<>();
//        for (int i = 1; i < 11; i++) {
//            linkedHashMap.put(i,i+i);
//        }
//        Set<Integer> integers = linkedHashMap.keySet();
//        for (Integer integer : integers) {
//            System.out.println(integer);
//        }
//        System.out.println(linkedHashMap.keySet().iterator().next());

    }

}
class LRUCache {
    int cap;
    LinkedHashMap<Integer,Integer> cache;


    public LRUCache(int length){
        this.cap=length;
        cache=new LinkedHashMap<>();
    }

    public void put(int key,int value){
        if (cache.containsKey(key)) {
            makeRencent(key);

            cache.put(key,value);
            return;
        }
        cache.put(key,value);

        if (cap<cache.size()) {
            cache.remove(cache.keySet().iterator().next());
        }
    }

    public int get(int key){
        if (cache.containsKey(key)) {
            makeRencent(key);
            return cache.get(key);
        }else {
            return -1;
        }
    }


    private void makeRencent(int key){
        Integer value = cache.get(key);
        cache.remove(key);
        cache.put(key,value);
    }


}