package com.wyx.leetcode.about100;

import java.util.ArrayList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.about100
 * @ClassName: test03
 * @Author: GoKu
 * @Description: 第三道算法
 * @Date: 2022/4/9 16:08
 * @Version: 1.0
 */
public class test03 {
    public int lengthOfLongestSubstring(String s) {
        // 记录字符上一次出现的位置
        int[] last = new int[10000000];
        int n = s.length();

        int res = 0;
        int start = 0; // 窗口开始位置
        for(int i = 0; i < n; i++) {
            int index = s.charAt(i);
            start = Math.max(start, last[index]);
            res   = Math.max(res, i - start + 1);
            last[index] = i+1;
        }

        return res;
    }

    public static void main(String[] args) {
        test03 test = new test03();
        System.out.println(test.lengthOfLongestSubstring("奥术大师奥dasdas"));
    }
}
