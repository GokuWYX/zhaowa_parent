package com.wyx.leetcode.about100;


/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.about100
 * @ClassName: test02
 * @Author: GoKu
 * @Description:
 * @Date: 2022/4/1 16:15
 * @Version: 1.0
 */
public class test02 {
    public static void main(String[] args) {
        Solution02 solution = new Solution02();
        ListNode a03 = new ListNode(3, null);
        ListNode a02 = new ListNode(4, a03);
        ListNode a01 = new ListNode(2, a02);

        ListNode b03 = new ListNode(4, null);
        ListNode b02 = new ListNode(6, b03);
        ListNode b01 = new ListNode(5, b02);


        System.out.println(solution.addTwoNumbers(a01, b01).val);
    }
}
class Solution02 {
        public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
            ListNode result = null; // 结果链表
            ListNode temp = new ListNode(0); // 当前相加节点的下一个节点 主要就是存储进位
            while (l1 != null || l2 != null) {
                // 防止某一条链表已经为空的情况
                int a = l1 != null ? l1.val : 0;
                int b = l2 != null ? l2.val : 0;
                int sum = a + b;

                if (result == null) { // 判断是不是第一个节点
                    int t = sum / 10; // 取得进位     比如a+b=31 那么进位就是3
                    sum = t > 0 ? sum % 10 : sum; // 如果有进位 说明和大于10了 取模拿到各位 31 % 10 = 1
                    result = new ListNode(sum); // 将第一个节点设置为sum
                    temp.val = t; // 存储进位 没有进位存0也没影响

                } else { // 不是第一个节点
                    sum += temp.val; // 加上上一轮留下的进位
                    int t = sum / 10; // 取得进位
                    sum = t > 0 ? sum % 10 : sum; // 如果有进位
                    addNode(result, new ListNode(sum)); // 加入到结果集链表
                    temp = new ListNode(t);

                }
                l1 = l1 != null ? l1.next : null;
                l2 = l2 != null ? l2.next : null;
            }
            // 运算结束 如果进位节点存的值不为0  追加到尾部
            if (temp.val > 0) {
                addNode(result, temp);
            }
            return result;
        }

        public void addNode(ListNode result, ListNode newNode) {
            if (result.next == null) {
                result.next = newNode;
            }else {
                addNode(result.next, newNode);
            }
        }
    }