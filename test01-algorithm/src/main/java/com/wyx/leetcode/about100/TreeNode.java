package com.wyx.leetcode.about100;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.about100
 * @ClassName: TreeNode
 * @Author: GoKu
 * @Description:
 * @Date: 2022/7/21 9:16
 * @Version: 1.0
 */
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right){
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
