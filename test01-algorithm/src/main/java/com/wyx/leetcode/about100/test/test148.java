package com.wyx.leetcode.about100.test;

/**
 * @program: zhaowa_parent
 * @author: Gogoku
 * @create: 2022-10-24 15:48
 * @description: dawa
 **/
public class test148 {
    
    public static void main(String[] args) {
        Mergerr mergerr = new Mergerr();
        Integer[] a={9,7,8,4,6,5,2,3,1};
        mergerr.sort(a);
        for (Integer integer : a) {
            System.out.println(integer);
        }
    }
}
class Mergerr{
    private Comparable[] datas;

    /**
     *
     * @param a 传入可比较数组
     */
    public void sort(Comparable[] a){
        datas=new Comparable[a.length];
        int lo=0;
        int hi=a.length-1;
        sort(a,lo,hi);
    }

    public void sort(Comparable[] a,int lo,int hi){
        if (lo>=hi){
            return;
        }
        int mid=(lo+hi)/2;
        sort(a,lo,mid);
        sort(a,mid+1,hi);
        merge(a,lo,mid,hi);
    }

    public void merge(Comparable[] a,int lo,int mid,int hi){

        int leftIndex=lo;
        int rightIndex=mid+1;
        int datIndex=lo;

        while (leftIndex<=mid && rightIndex<=hi){
            if (more(a[leftIndex],a[rightIndex])){
                datas[datIndex++]=a[rightIndex++];
            }else {
                datas[datIndex++]=a[leftIndex++];
            }
        }

        while (leftIndex<=mid){
            datas[datIndex++]=a[leftIndex++];
        }
        while (rightIndex<=hi){
            datas[datIndex++]=a[rightIndex++];
        }
        for (int index=lo;index<=hi;index++){
            a[index]=datas[index];
        }
    }

    private static void swap(Comparable[] a,int left,int right){
        Comparable tem=a[left];
        a[left]=a[right];
        a[right]=tem;
    }
    private static boolean more(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }


}
