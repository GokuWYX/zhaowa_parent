package com.wyx.leetcode.dfs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: DFS07
 * @Author: GoKu
 * @Description: 二叉树的最小深度-理解递归结束条件
 * @Date: 2021/12/22 9:09
 * @Version: 1.0
 */

/*
* 标签：DFS
终止条件、返回值和递归过程：

    当前节点 root 为空时，说明此处树的高度为 0，0 也是最小值
    当前节点 root 的左子树和右子树都为空时，说明此处树的高度为 1，1 也是最小值
    如果为其他情况，则说明当前节点有值，且需要分别计算其左右子树的最小深度，返回最小深度 +1，+1 表示当前节点存在有 1 个深度

时间复杂度：O(n)O(n)O(n)，nnn为树的节点数量

作者：guanpengchn
链接：https://leetcode-cn.com/problems/minimum-depth-of-binary-tree/solution/hua-jie-suan-fa-111-er-cha-shu-de-zui-xiao-shen-du/
来源：力扣（LeetCode）
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
* */
public class DFS07 {
    public int minDepth(TreeNode root) {
        if(root == null) {
            return 0;
        }
        if(root.left == null && root.right == null) {
            return 1;
        }
        int ans = Integer.MAX_VALUE;
        if(root.left != null) {
            ans = Math.min(minDepth(root.left), ans);
        }
        if(root.right != null) {
            ans = Math.min(minDepth(root.right), ans);
        }
        return ans + 1;
    }
}


