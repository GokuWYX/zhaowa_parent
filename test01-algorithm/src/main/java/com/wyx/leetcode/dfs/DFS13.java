package com.wyx.leetcode.dfs;

import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: DFS13
 * @Author: GoKu
 * @Description: 二叉搜索树中的众数
 * @Date: 2021/12/24 9:07
 * @Version: 1.0
 */
public class DFS13 {
    List<Integer> answer=new ArrayList<Integer>();
    int base,count,maxCount;

    public int[] findNode(TreeNode root){
        dfs(root);
        int[] node = new int[answer.size()];
        for (int i = 0; i < answer.size(); ++i) {
            node[i]=answer.get(i);
        }
        return node;
    }
    public void dfs(TreeNode root){
        if (root==null){
            return;
        }
        dfs(root.left);
        update(root.val);
        dfs(root.right);
    }
    public void update(int x){
        if (x==base){
            ++count;
        }else {
            count=1;
            base=x;
        }

        if (count==maxCount){
            answer.add(base);
        }
        if (count>maxCount){
            maxCount=count;
            answer.clear();
            answer.add(base);
        }

    }




}
