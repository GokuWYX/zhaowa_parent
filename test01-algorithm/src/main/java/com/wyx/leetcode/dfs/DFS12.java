package com.wyx.leetcode.dfs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: DFS12
 * @Author: GoKu
 * @Description: 计算给定二叉树的所有左叶子之和。
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 *
 * 在这个二叉树中，有两个左叶子，分别是 9 和 15，所以返回 24
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/sum-of-left-leaves
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @Date: 2021/12/23 10:52
 * @Version: 1.0
 */
public class DFS12 {
    int sum=0;
    public int sumOfLeftLeaves(TreeNode root){
        if (root==null) return 0;
        if (root.left!=null && root.left.left==null && root.left.right==null){
            sum+=root.left.val;
        }
        sumOfLeftLeaves(root.left);
        sumOfLeftLeaves(root.right);
        return sum;
    }




















}
