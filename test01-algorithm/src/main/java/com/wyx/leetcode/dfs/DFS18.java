package com.wyx.leetcode.dfs;

import java.util.List;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: DFS18
 * @Author: GoKu
 * @Description: 给定一个 N 叉树，找到其最大深度。  最大深度是指从根节点到最远叶子节点的最长路径上的节点总数。
 * @Date: 2021/12/27 11:00
 * @Version: 1.0
 */
public class DFS18 {
    public int maxDepth(Node root) {
        if (root == null) {
            return 0;
        }
        int maxChildDepth = 0;
        List<Node> children = root.children;
        for (Node child : children) {
            int childDepth = maxDepth(child);
            maxChildDepth = Math.max(maxChildDepth, childDepth);
        }
        return maxChildDepth + 1;
    }
    class Node {
        public int val;
        public List<Node> children;

        public Node() {}

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    };
}
