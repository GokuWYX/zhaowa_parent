package com.wyx.leetcode.dfs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: DFS04
 * @Author: GoKu
 * @Description: 二叉树的对称
 * @Date: 2021/12/21 8:40
 * @Version: 1.0
 */
public class DFS04 {
    public boolean isBalance(TreeNode treeNode){
        return check(treeNode.left,treeNode.right);
    }
    public boolean check(TreeNode p,TreeNode q){
        if (q==null && p==null){
            return true;
        }
        if (q==null || p==null){
            return false;
        }
        return p.value==q.value && check(p.left,q.right) && check(p.right,q.left);
    }



    class TreeNode{
        public TreeNode left;
        public TreeNode right;
        public Integer value;

        public TreeNode(Integer value, TreeNode left, TreeNode right) {
            this.left = left;
            this.right = right;
            this.value = value;
        }
    }
}
