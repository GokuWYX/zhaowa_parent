package com.wyx.leetcode.dfs;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;

import java.util.ArrayList;
import java.util.List;


/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: Soultion
 * @Author: GoKu
 * @Description: 中序遍历
 * @Date: 2021/12/19 8:57
 * @Version: 1.0
 */
public class DFS01 {

    public List<Integer> inorderTravelsal(TreeNode root){
        List<Integer> res=new ArrayList<Integer>();
        dfs(res,root);
        return res;
    }
    void dfs(List<Integer> res, TreeNode root){
        if (root==null){
            return;
        }
        dfs(res,root.left);
        res.add(root.value);
        dfs(res,root.right);

    }
    public class TreeNode{
        public TreeNode left;
        public TreeNode right;
        public Integer value;

        public TreeNode(Integer value,TreeNode left, TreeNode right) {
            this.left = left;
            this.right = right;
            this.value = value;
        }
    }

}
