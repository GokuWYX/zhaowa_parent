package com.wyx.leetcode.dfs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: DFS14
 * @Author: GoKu
 * @Description:给你一个二叉搜索树的根节点 root ，返回 树中任意两不同节点值之间的最小差值 。
 *
 * 差值是一个正数，其数值等于两值之差的绝对值。
 * @Date: 2021/12/24 10:25
 * @Version: 1.0
 */
public class DFS14 {
    int res=Integer.MAX_VALUE;
    TreeNode pre=null;
    public int getMinimunDifference(TreeNode root){
        dfs(root);
        return res;
    }
    private void dfs(TreeNode root){
        if (root==null) return;
        dfs(root.left);
        if (pre!=null){
            res=Math.min(res,root.val-pre.val);
        }
        pre=root;
        dfs(root.right);
    }
}
