//package com.wyx.leetcode.dfs;
//
//import java.util.ArrayList;
//
//
///**
// * @ProjectName: zhaowa_parent
// * @Package: com.wyx.leetcode.dfs
// * @ClassName: DFS09
// * @Author: GoKu
// * @Description: 二叉树的后续遍历
// * @Date: 2021/12/22 9:52
// * @Version: 1.0
// */
//public class DFS09 {
//    public void postTraversal(TreeNode root){
//        ArrayList<Integer> list = new ArrayList<>();
//        postTraversal(root,list);
//        for (int i = 0; i < list.size(); i++) {
//            System.out.println(list.get(i));
//        }
//
//    }
//    public void postTraversal(TreeNode root, ArrayList list){
//        if (root==null){
//            return;
//        }
//        postTraversal(root.left,list);
//        postTraversal(root.right,list);
//        list.add(root.val);
//    }
//
//}
