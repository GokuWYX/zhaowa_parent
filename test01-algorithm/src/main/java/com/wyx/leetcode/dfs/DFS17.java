package com.wyx.leetcode.dfs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: DFS17
 * @Author: GoKu
 * @Description: 572. 另一棵树的子树
 * 给你两棵二叉树 root 和 subRoot 。检验 root 中是否包含和 subRoot 具有相同结构和节点值的子树。如果存在，返回 true ；否则，返回 false 。
 *
 * 二叉树 tree 的一棵子树包括 tree 的某个节点和这个节点的所有后代节点。tree 也可以看做它自身的一棵子树。
 *
 * 提示：
 *     root 树上的节点数量范围是 [1, 2000]
 *     subRoot 树上的节点数量范围是 [1, 1000]
 *     -104 <= root.val <= 104
 *     -104 <= subRoot.val <= 104
 *
 * 通过次数94,651
 * 提交次数199,703

 * @Date: 2021/12/27 10:04
 * @Version: 1.0
 */
public class DFS17 {
    public boolean isSubtree(TreeNode s, TreeNode t) {
        return dfs(s, t);
    }

    public boolean dfs(TreeNode s, TreeNode t) {
        if (s == null) {
            return false;
        }
        return check(s, t) || dfs(s.left, t) || dfs(s.right, t);
    }

    public boolean check(TreeNode s, TreeNode t) {
        if (s == null && t == null) {
            return true;
        }
        if (s == null || t == null || s.val != t.val) {
            return false;
        }
        return check(s.left, t.left) && check(s.right, t.right);
    }
}

