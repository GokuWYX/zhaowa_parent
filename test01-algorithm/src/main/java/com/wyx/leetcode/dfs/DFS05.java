package com.wyx.leetcode.dfs;

import java.util.ArrayList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: DFS05
 * @Author: GoKu
 * @Description: 二叉樹的最大深度
 * @Date: 2021/12/21 9:05
 * @Version: 1.0
 */
public class DFS05 {
    public int maxDepth(TreeNode root) {
        if(root == null) {
            return 0;
        } else {
            int left = maxDepth(root.left);
            int right = maxDepth(root.right);
            return Math.max(left, right) + 1;
        }
    }




    class TreeNode{
        public TreeNode left;
        public TreeNode right;
        int val;

        TreeNode(int x) { val = x; }
    }
}
