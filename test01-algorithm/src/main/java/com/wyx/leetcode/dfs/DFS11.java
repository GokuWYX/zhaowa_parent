package com.wyx.leetcode.dfs;

import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: DFS11
 * @Author: GoKu
 * @Description: 二叉树的所有路径
 * @Date: 2021/12/23 9:25
 * @Version: 1.0
 */
public class DFS11 {

    public List<String> binaryTreePaths(TreeNode root){
        List<String> paths=new ArrayList<String>();
        constructPaths(root,"",paths);
        return paths;

    }

    public void constructPaths(TreeNode root,String path,List<String> paths){
        if (root!=null){
            StringBuffer pathSB = new StringBuffer(path);
            pathSB.append(Integer.toString(root.val));
            if (root.left==null && root.right==null){
                paths.add(pathSB.toString());
            }else {
                pathSB.append("->");
                constructPaths(root.left,pathSB.toString(),paths);
                constructPaths(root.right,pathSB.toString(),paths);

            }
        }

    }
}
