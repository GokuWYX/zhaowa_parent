package com.wyx.leetcode.dfs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: DFS10
 * @Author: GoKu
 * @Description: 翻转一棵二叉树。
 * @Date: 2021/12/23 8:35
 * @Version: 1.0
 */
public class DFS10 {
    public TreeNode invserseTree(TreeNode root){
        if (root==null) return null;

        TreeNode tmp=root.left;
        root.left=root.right;
        root.right=tmp;
        invserseTree(root.left);
        invserseTree(root.right);
        return root;
    }
}
