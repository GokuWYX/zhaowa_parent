package com.wyx.leetcode.dfs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: DFS06
 * @Author: GoKu
 * @Description: 判断是否为平衡二叉树
 * @Date: 2021/12/21 9:54
 * @Version: 1.0
 */
public class DFS06 {
    public boolean isBalanced(TreeNode root) {
        if (root == null) {
            return true;
        } else {
            return Math.abs(height(root.left) - height(root.right)) <= 1 && isBalanced(root.left) && isBalanced(root.right);
        }
    }

    public int height(TreeNode root) {
        if (root == null) {
            return 0;
        } else {
            return Math.max(height(root.left), height(root.right)) + 1;
        }
    }

    class TreeNode{
        public TreeNode left;
        public TreeNode right;
        int val;

        TreeNode(int x) { val = x; }
    }
}

