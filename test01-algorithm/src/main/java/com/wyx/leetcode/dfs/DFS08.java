package com.wyx.leetcode.dfs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: DFS08
 * @Author: GoKu
 * @Description: 前序遍历
 * @Date: 2021/12/22 9:27
 * @Version: 1.0
 */
public class DFS08 {
    public static void preOrderRecur(TreeNode head) {
        if (head == null) {
            return;
        }
        System.out.print(head.value + " ");
        preOrderRecur(head.left);
        preOrderRecur(head.right);
    }


    public static void midOrderRecur(TreeNode head) {
        if (head == null) {
            return;
        }
        midOrderRecur(head.left);
        System.out.print(head.value + " ");
        midOrderRecur(head.right);
    }

    public static void postOrderRecur(TreeNode head) {
        if (head == null) {
            return;
        }
        postOrderRecur(head.left);
        postOrderRecur(head.right);
        System.out.print(head.value + " ");
    }

    class TreeNode{
        public TreeNode left;
        public TreeNode right;
        public Integer value;

        public TreeNode(Integer value, TreeNode left, TreeNode right) {
            this.left = left;
            this.right = right;
            this.value = value;
        }
    }
}
