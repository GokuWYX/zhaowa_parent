package com.wyx.leetcode.dfs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.dfs
 * @ClassName: DFS02
 * @Author: GoKu
 * @Description: 递归便利比较两棵树
 * @Date: 2021/12/20 8:31
 * @Version: 1.0
 */

    public class DFS02 {
        public boolean isSame(TreeNode node01,TreeNode node02){
            if (node01==null && node02==null)
                return true;
            if (node01==null || node02==null)
                return false;
            if (node01.val!=node02.val)
                return false;
            return  isSame(node01.left,node02.left) && isSame(node01.right,node02.right);
        }
    }


class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode(int x) { val = x; }
}