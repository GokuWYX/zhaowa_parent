package com.wyx.leetcode.bfs;

import java.util.LinkedList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bfs
 * @ClassName: BFS01
 * @Author: GoKu
 * @Description: 相同的树https://leetcode-cn.com/problems/same-tree/
 * @Date: 2022/1/17 16:14
 * @Version: 1.0
 */
public class BFS01 {

    public boolean isSameTree(TreeNode p,TreeNode q){
        if (p==null && q==null){
            return true;
        }else if (p==null || q==null){
            return false;
        }
        LinkedList<TreeNode> queue1 = new LinkedList<>();
        LinkedList<TreeNode> queue2 = new LinkedList<>();
        queue1.offer(p);
        queue2.offer(q);
        while (!queue1.isEmpty() && !queue2.isEmpty()) {
            TreeNode node1 = queue1.poll();
            TreeNode node2 = queue2.poll();
            if (node1.val!=node2.val){
                return false;
            }
            TreeNode left1 = node1.left,
                     right1=node1.right,
                     left2=node2.left,
                     right2=node2.right;
            if (left1==null ^ left2==null){
                return false;
            }
            if (right1==null ^ right2==null){
                return false;
            }
            if (left1!=null){
                queue1.offer(left1);
            }
            if (right1!=null){
                queue1.offer(right1);
            }
            if (left2!=null){
                queue2.offer(left2);
            }
            if (right2!=null){
                queue2.offer(right2);
            }
        }
        return queue1.isEmpty() && queue2.isEmpty();
    }
}
