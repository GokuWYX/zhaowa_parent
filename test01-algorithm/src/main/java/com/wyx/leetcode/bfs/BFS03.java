package com.wyx.leetcode.bfs;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bfs
 * @ClassName: BFS03
 * @Author: GoKu
 * @Description: 02d的另外一种解法
 * @Date: 2022/1/22 8:59
 * @Version: 1.0
 */
public class BFS03 {
    public boolean isSymetric(TreeNode root){
        return check(root,root);
    }
    public boolean check(TreeNode u, TreeNode v) {
        Queue<TreeNode> q = new LinkedList<TreeNode>();
        q.offer(u);
        q.offer(v);
        while (!q.isEmpty()) {
            u = q.poll();
            v = q.poll();
            if (u == null && v == null) {
                continue;
            }
            if ((u == null || v == null) || (u.val != v.val)) {
                return false;
            }

            q.offer(u.left);
            q.offer(v.right);

            q.offer(u.right);
            q.offer(v.left);
        }
        return true;
    }
}
