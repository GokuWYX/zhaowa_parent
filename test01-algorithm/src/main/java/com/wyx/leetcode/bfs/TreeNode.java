package com.wyx.leetcode.bfs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bfs
 * @ClassName: TreeNode
 * @Author: GoKu
 * @Description:
 * @Date: 2022/1/17 16:31
 * @Version: 1.0
 */
public class TreeNode {

    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}

    TreeNode(int val) { this.val = val; }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

