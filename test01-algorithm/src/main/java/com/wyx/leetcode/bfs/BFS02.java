package com.wyx.leetcode.bfs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bfs
 * @ClassName: BFS02
 * @Author: GoKu
 * @Description: 给你一个二叉树的根节点 root ， 检查它是否轴对称。
 * @Date: 2022/1/21 19:31
 * @Version: 1.0
 */
public class BFS02 {

    public boolean isBanleance(TreeNode node){
        return compare(node.left,node.right);
    }
    public boolean compare(TreeNode left,TreeNode right){
        if (left==null && right==null){
            return true;
        }
        if (left==null || right==null){
            return false;
        }
        return left.val==right.val && compare(left.left,right.right) && compare(left.right,right.left) ;
    }

}
