package com.wyx.leetcode.bfs;

import java.util.LinkedList;


/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bfs
 * @ClassName: BFS04
 * @Author: GoKu
 * @Description:
 * @Date: 2022/2/7 11:08
 * @Version: 1.0
 */
public class BFS04 {
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }

        LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        int ans = 0;

        while (!queue.isEmpty()) {
            int size = queue.size();
            while (size > 0) {
                TreeNode node = queue.poll();
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
                size--;
            }
            ans++;
        }
        return ans;
    }
}

