package com.wyx.leetcode.bs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bs
 * @ClassName: BS02
 * @Author: 国产2022款美男子--王宇翔
 * @Description: 算数平方根
 * @Date: 2022/2/28 21:51
 * @Version: 1.0
 */
public class BS02 {
    public static void main(String[] args) {
//        for (int i = 0; i < 100; i++) {
//            System.out.println(mySqrt(i));
//        }
        System.out.println(mySqrt(26));
    }

    public static int mySqrt(int x) {
        int l=0;
        int r=x;
        int ans=0;
        while (l<=r){
            int mid=l-(l-r)/2;
            if ((long)mid*mid<=x){
                ans=mid;
                l=mid+1;
            }else{
                r=mid-1;
            }
        }
        return ans;
    }
}
