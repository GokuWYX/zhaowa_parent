package com.wyx.leetcode.bs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bs
 * @ClassName: BS09
 * @Author: GoKu
 * @Description: 9
 * @Date: 2022/3/2 15:06
 * @Version: 1.0
 */
public class BS09 {
    public static void main(String[] args) {
        int[]a={0,1,2,3,4,5,6,7,8,9,10,11,12};
        for (int i = 0; i <=12 ; i++) {
            System.out.println(new BS09().search(a, i));
        }
    }
    public int search(int[] nums, int target) {
        int l=0;
        int r=nums.length-1;
        int mid=0;
        while (l<=r){

            //mid=(l+r)>>1;
            mid=l+(r-l+1)/2;

            if (nums[mid]<target){
                l=mid+1;
            }else if ((nums[mid]>target)){
                r=mid-1;
            }else {
                return mid;
            }
        }
        return -1;
    }
}
