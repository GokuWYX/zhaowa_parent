package com.wyx.leetcode.bs;

import java.util.Arrays;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bs
 * @ClassName: BS05
 * @Author: GoKu
 * @Description:
 * @Date: 2022/3/1 19:25
 * @Version: 1.0
 */
public class BS05 {
    public int[] intersect(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        //数组1的下标
        int index1=0;
        //数组2的下标
        int index2=0;
        //返回结果数组下标
        int indexResult=0;


        //返回结果数组的长度
        int resultLength=0;

        if (nums1.length>=nums2.length){
            resultLength=nums2.length;
        }else {
            resultLength=nums1.length;
        }

        //返回的结果数组
        int[] result=new int[resultLength];



        while (index1<nums1.length && index2<nums2.length){
            if (nums1[index1]==nums2[index2]){
                    result[indexResult]=nums1[index1];
                    indexResult++;
                    index1++;
                    index2++;
            }else if (nums1[index1]>nums2[index2]){
                index2++;
            }else {
                index1++;
            }
        }
        return Arrays.copyOfRange(result,0,indexResult);
        //return result;

    }
}
