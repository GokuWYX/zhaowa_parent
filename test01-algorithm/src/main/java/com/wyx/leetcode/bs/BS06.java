package com.wyx.leetcode.bs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bs
 * @ClassName: BS06
 * @Author: GoKu
 * @Description: 有效完全平方数
 * @Date: 2022/3/2 8:51
 * @Version: 1.0
 */
public class BS06 {
    public boolean isPerfectSquare(int num) {
        int l=0;
        int r=num;
        int ans;
        int mid=0;
        while (l<=r){
            mid=(l+r)>>1;
            if ((long)mid*mid==num){
                return true;
            }else if ((long)mid*mid<=num){
                l=mid+1;
            }else {
                r=mid-1;
            }

        }
        return false;
    }
}
