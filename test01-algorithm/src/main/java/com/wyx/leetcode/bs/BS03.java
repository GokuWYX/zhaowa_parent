package com.wyx.leetcode.bs;

import java.util.Arrays;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bs
 * @ClassName: BS03
 * @Author: GoKu
 * @Description: 两个数组的交集
 * @Date: 2022/2/28 22:50
 * @Version: 1.0
 */
public class BS03 {

    public int[] intersection(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);

        int length1=nums1.length;
        int length2=nums2.length;

        int[] insertion=new int[length1+length2];
        int index=0;
        int index1=0;
        int index2=0;

        while (index1<length1 && index2<length2){
            int num1=nums1[index1],num2=nums2[index2];
            if (num1==num2){
                if (index==0 || num1!=insertion[index-1]){
                    insertion[index++]=num1;
                }
                index1++;
                index2++;
            }else if (num1<num2){
                index++;
            }else {
                index2++;
            }
        }return Arrays.copyOfRange(insertion,0,index);

    }
}
