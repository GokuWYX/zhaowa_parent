package com.wyx.leetcode.bs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bs
 * @ClassName: BS07
 * @Author: GoKu
 * @Description:BS07在力扣
 * @Date: 2022/3/2 12:09
 * @Version: 1.0
 */
public class BS08 {
    public static void main(String[] args) {
        BS08 bs08 = new BS08();
        System.out.println(bs08.arrangeCoins(5));
    }
    public int arrangeCoins(int n) {
        int l=1;
        int r=n;

        while (l<r){

            int mid=(r-l+1)/2+l;
            if ((long)(mid*mid/2+1)<=(long)n){
                l=mid;
            }else {
                r=mid-1;
            }
        }
        return l-1;
    }

        //错误思路3
        //        int sum=0;
//        int i;
//        for (i = 1; sum <= n; i++) {
//            sum+=i;
//        }
//        return i-2;
//    }
//        int l=0;
//        int r=n;
//        int mid=0;
//        while (l<=r){
//            mid=(int)((long)(l+r)>>1);
//            if (getForResult(mid)<n){
//                l=mid+1;
//            }else {
//                return mid;
//            }
//        }
//        return -1;
//
//    }
//    public static  int getForResult(int a){
//        if (a==0){
//            return 0;
//        }
//        if (a<0){
//            return -1;
//        }
//        int sum=0;
//        for (int i = 0; i <= a; i++) {
//            sum+=i;
//        }
//        return sum;
//    }
//






//     错误思路的参数
//        int index=1;
//        int sum=0;


        //错误思路1
//        while (true){
//            if (sum<n){
//                sum+=index;
//                index++;
//            }else if (sum==n){
//                return index;
//            }else {
//                return index-2;
//            }
//        }

        //错误思路2
//        for (int i = 1 ; i <n; i++) {
//            if (sum<n){
//                sum+=i;
//            }else if (sum>=n){
//                return i;
//            }
//        }
//        return -1;



}
