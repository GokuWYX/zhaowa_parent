package com.wyx.leetcode.bs;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bs
 * @ClassName: BS01
 * @Author: GoKu
 * @Description: 第一次二分
 * @Date: 2022/2/28 19:40
 * @Version: 1.0
 */
public class BS01 {

    public static void main(String[] args) {

        int[] a={0,1,2,3,5,6,7,8,9,10,21};
        System.out.println(searchInsert(a, 0));
    }

    public static int searchInsert(int[] nums, int target) {
        int left=0;
        int right=nums.length-1;
        while (left<=right){
            int mid=(left+right)>>1;
            if (target<nums[mid]){
                right=mid-1;
            }else if (target>nums[mid]){
                left=mid+1;
            }else {
                return mid;
            }
        }

        return left;
    }

}
