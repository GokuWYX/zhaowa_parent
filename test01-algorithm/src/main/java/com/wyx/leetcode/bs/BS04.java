package com.wyx.leetcode.bs;

import java.util.HashSet;
import java.util.Set;

/**
 * @ProjectName: zhaowa_parent
 * @Package: com.wyx.leetcode.bs
 * @ClassName: BS04
 * @Author: GoKu
 * @Description: 双集合交集
 * @Date: 2022/3/1 16:58
 * @Version: 1.0
 */
public class BS04 {
    public int[] intesection(int[] nums1,int[] nums2){
        HashSet<Integer> set1 = new HashSet<Integer>();
        HashSet<Integer> set2 = new HashSet<Integer>();
        for (int i : nums1) {
            set1.add(i);
        }
        for (int i : nums2) {
            set2.add(i);
        }

        return getIntersection(set1,set2);
    }
    public int[] getIntersection(Set<Integer> set1,Set<Integer> set2){
        if (set1.size()>set2.size()){
            return getIntersection(set2,set1);
        }
        Set<Integer> interserctionSet=new HashSet<Integer>();
        for (Integer num : set1) {
            if (set2.contains(num)){
                interserctionSet.add(num);
            }
        }

        int[] intersection=new int[interserctionSet.size()];
        int index=0;
        for (Integer num : interserctionSet) {
            intersection[index++]=num;
        }
        return intersection;

    }


}
