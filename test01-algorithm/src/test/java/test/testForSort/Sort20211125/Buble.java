package test.testForSort.Sort20211125;

import com.wyx.sort.Sort;

public class Buble implements Sort {
    /*  1. 比较相邻的元素。如果前一个元素比后一个元素大，就交换这两个元素的位置。
        2. 对每一对相邻元素做同样的工作，从开始第一对元素到结尾的最后一对元素。最终最后位置的元素就是最大值。
    */
    public void sort(Comparable[] a){
        for (int i = a.length-1; i >= 0 ; i--) {
            for (int j = 0; j < i; j++) {
                if (greater(a[j],a[i+1])){
                    swap(a,j,j+1);
                }
            }
        }

    }


    public boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    public void swap(Comparable[] a,int j,int k){
        Comparable tmp=a[j];
        a[j]=a[k];
        a[k]=tmp;
    }



}
