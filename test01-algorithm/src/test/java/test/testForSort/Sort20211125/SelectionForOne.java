package test.testForSort.Sort20211125;

import com.wyx.sort.Sort;

public class SelectionForOne implements Sort {

    /*
    * 1.每一次遍历的过程中，都假定第一个索引处的元素是最小值，和其他索引处的值依次进行比较，如果当前索引处
的值大于其他某个索引处的值，则假定其他某个索引出的值为最小值，最后可以找到最小值所在的索引
2.交换第一个索引处和最小值所在的索引处的值
    * */

/*    @Override
    public void sort(Comparable[] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j=i;j<a.length;j++){
                if (greater(a[i],a[j])){
                    swap(a,i,j);
                }
            }
        }
    }*/

    @Override
    public void sort(Comparable[] a) {
        for (int i = 0; i < a.length; i++) {
            int minIndex=i;
            for (int j = i; j < a.length; j++) {
                if (greater(a[minIndex],a[j])){
                    minIndex=j;
                }
            }
            swap(a,minIndex,i);
        }
    }




    public boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    public void swap(Comparable[] a,int j,int k){
        Comparable tmp=a[j];
        a[j]=a[k];
        a[k]=tmp;
    }


}
