package test.testForSort.Sort20211125;

import com.wyx.sort.Sort;

public class Selection implements Sort {
    @Override
    public void sort(Comparable[] a) {
        for (int i=0;i<=a.length-2;i++){
            //假定本次遍历，最小值所在的索引是i
            int minIndex=i;
            for (int j=i+1;j<a.length;j++){
                if (greater(a[minIndex],a[j])){
                    //跟换最小值所在的索引
                    minIndex=j;
                }
            }
            //交换i索引处和minIndex索引处的值
            swap(a,i,minIndex);
        }
    }



    public boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    public void swap(Comparable[] a,int j,int k){
        Comparable tmp=a[j];
        a[j]=a[k];
        a[k]=tmp;
    }


}
