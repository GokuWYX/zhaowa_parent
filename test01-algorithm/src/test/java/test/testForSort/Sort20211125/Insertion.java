package test.testForSort.Sort20211125;

import com.wyx.sort.Sort;

public class Insertion implements Sort {
    /*
    *   1.把所有的元素分为两组，已经排序的和未排序的；
        2.找到未排序的组中的第一个元素，向已经排序的组中进行插入；
        3.倒叙遍历已经排序的元素，依次和待插入的元素进行比较，直到找到一个元素小于等于待插入元素，
          那么就把待插入元素放到这个位置，其他的元素向后移动一位；
    *
    * */

    @Override
    public void sort(Comparable[] a) {
        //未排序的组的第一个元素
        for (int i = 1; i < a.length; i++) {

            for (int j=i; j>0;j--){
                if (greater(a[j-1],a[j])){
                    swap(a,j-1,j);
                }else {
                    break;
                }
            }
        }
    }

    public boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    public void swap(Comparable[] a,int j,int k){
        Comparable tmp=a[j];
        a[j]=a[k];
        a[k]=tmp;
    }
}
