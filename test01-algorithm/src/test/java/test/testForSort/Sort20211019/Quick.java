package test.testForSort.Sort20211019;

import com.wyx.sort.Sort;

public class Quick implements Sort {
    public void sort(Comparable[] a){
        int lo=0;
        int hi=a.length-1;
        sort(a,lo,hi);
    }
    public void sort(Comparable[] a,int lo, int hi){
        if (lo>=hi){
            return;
        }
        int partition = partition(a, lo, hi);
        sort(a, lo, partition-1);
        sort(a,partition+1,hi);

    }
    public int partition(Comparable[] a,int lo,int hi){
        Comparable key=a[lo];
        int left=lo;
        int right=hi+1;
        while (true){
            while (greater(a[--right],key)){
                if (right==lo){
                    break;
                }
            }
            while (greater(key,a[++left])){
                if (left==hi){
                    break;
                }
            }
            if (left>=right){
                break;
            }else {
                swap(a,left,right);
            }

        }
        swap(a,lo,right);
        return right;
    }



    //两值比较
    private static boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    //交换方法
    private static void swap(Comparable[] a,int i,int j){
        Comparable tem=a[i];
        a[i]=a[j];
        a[j]=tem;
    }
}
