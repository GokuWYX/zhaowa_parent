package test.testForSort.Sort20211019;

public class Shell {

    public static void sort(Comparable[] a){
        int l=a.length;
        int n=1;
        while (n<l/2){
            n=2*n+1;
        }
        while (n>=1){

            for (int i = n; i < l; i++) {
                for (int j = i; j >=n ; j-=n) {
                    if (greater(a[j-n],a[j])){
                        swap(a,j-n,j);
                    }else {
                        break;
                    }
                }
            }

            n/=2;
        }

    }




    //两值比较
    private static boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    //交换方法
    private static void swap(Comparable[] a,int i,int j){
        Comparable tem=a[i];
        a[i]=a[j];
        a[j]=tem;
    }

}
