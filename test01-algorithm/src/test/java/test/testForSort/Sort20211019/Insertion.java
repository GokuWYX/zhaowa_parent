package test.testForSort.Sort20211019;

public class Insertion {
    //排序方法
    public static void sort(Comparable[] a){
        for (int i = 1; i < a.length; i++)
            for (int j = i; j > 0; j--) {
                if (greater(a[j - 1], a[j])) {
                    swap(a, j - 1, j);
                }
            }
    }
    //两值比较
    private static boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    //交换方法
    private static void swap(Comparable[] a,int i,int j){
        Comparable tem=a[i];
        a[i]=a[j];
        a[j]=tem;
    }



}
