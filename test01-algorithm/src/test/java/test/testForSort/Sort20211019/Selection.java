package test.testForSort.Sort20211019;

public class Selection {
    //排序方法
    public static void sort(Comparable[] a){
        for (int i = 0; i < a.length-1; i++) {
            int minIndex=i;
            for (int j = i+1; j < a.length; j++) {
                if (greater(a[minIndex],a[j])){
                    minIndex=j;
                }
            }
            swap(a,i,minIndex);
        }
    }
    //两值比较
    private static boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    //交换方法
    private static void swap(Comparable[] a,int i,int j){
        Comparable tem=a[i];
        a[i]=a[j];
        a[j]=tem;
    }

}
