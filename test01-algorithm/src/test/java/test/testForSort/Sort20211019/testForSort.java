package test.testForSort.Sort20211019;


import com.wyx.sort.*;
import org.junit.Test;

public class testForSort {
    Integer[] a={9,7,8,4,6,5,2,3,1};


    @Test
    public void testForQuick(){
        testSelect(new Quick());
    }

    @Test
    public void testForBigData(){
        testForBigData(new Quick());

    }







    public void testSelect(Sort sort){
        sort.sort(a);
        System.out.println(sort.getClass().getName());
        for (Integer i : a) {
            System.out.print(i +"  ");
        }

    }
    public void testForBigData(Sort sort){
        Integer[] b=new Integer[100000];
        Integer max=99999;
        for (int i = 0; i <100000; i++) {
            b[i]=max--;
        }
        long start = System.currentTimeMillis();
        sort.sort(b);
        long end = System.currentTimeMillis();
        //使用时间
        long time = end - start;
        System.out.println(sort.getClass().getName()+"用时"+time+"s");
    }


}
