package test.testForSort.Sort20211019;

public class Bubble_01 {
    /*
    * 1. 比较相邻的元素。如果前一个元素比后一个元素大，就交换这两个元素的位置。
      2. 对每一对相邻元素做同样的工作，从开始第一对元素到结尾的最后一对元素。
          最终最后位置的元素就是最大值。
    * */
    //排序方法
    public static void  sort(Comparable[] a){
        for (int i = a.length-1; i >0 ; i--) {
            for (int j = 0; j < i ; j++) {
                if (greater(a[j],a[j+1])){
                    swap(a,j,j+1);
                }
            }
        }
    }
    //两值比较
    private static boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    //交换方法
    private static void swap(Comparable[] a,int i,int j){
        Comparable tem=a[i];
        a[i]=a[j];
        a[j]=tem;
    }
}
