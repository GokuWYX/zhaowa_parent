package test.testForSort.Sort20211019;

import com.wyx.sort.Sort;

public class testShell implements Sort {
    @Override
    public void sort(Comparable[] a) {
        int N=a.length;
        int h=1;
        while (h<N/2){
            h=2*h+1;
        }

        while (h>=1){
            for (int i=h;i<N;i++){
                for (int j=i;i>=h;j-=h){
                    if (greater(a[j-h],a[j])){
                        swap(a,j,j-h);
                    }else {
                        break;
                    }
                }
            }

            h/=2;
        }





    }
    //两值比较
    private static boolean greater(Comparable a,Comparable b){
        return a.compareTo(b)>0;
    }
    //交换方法
    private static void swap(Comparable[] a,int i,int j){
        Comparable tem=a[i];
        a[i]=a[j];
        a[j]=tem;
    }
}
