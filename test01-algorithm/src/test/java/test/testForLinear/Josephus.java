package test.testForLinear;


/**
 * @ProjectName: zhaowa_parent
 * @Package: test.testForLinear
 * @ClassName: Josephus
 * @Author: Administrator
 * @Description: 约瑟夫问题2021年11月30日14:40:23复习测试
 * @Date: 2021/11/30 14:40
 * @Version: 1.0
 */
public class Josephus {
    /**
     * 解题思路：
     * 1.构建含有41个结点的单向循环链表，分别存储1~41的值，分别代表这41个人；
     * 2.使用计数器count，记录当前报数的值；
     * 3.遍历链表，每循环一次，count++；
     * 4.判断count的值，如果是3，则从链表中删除这个结点并打印结点的值，把count重置为0；
     */
    public static void main(String[] args) throws Exception{
        //构建循环链表
        Node<Integer> first=null;
        Node<Integer> pre=null;

        for (int i = 1; i <= 41 ; i++) {
            if (i==1){
                first=new Node<>(i,null);
                pre=first;
                continue;
            }
            Node<Integer> node=new Node<>(i,null);
            pre.next=node;
            if (i==41){
                pre.next=first;
            }
        }

        int count=0;
        Node<Integer> n=first;
        Node<Integer> before=null;
        while (n!=n.next){
            count++;
            if (count==3){
                before.next=n.next;
                System.out.println(n.item);
                count=0;
                n=n.next;
            }else {
                before=n;
                n=n.next;
            }

        }


    }

    //结点类
    private static class Node<T> {
        //存储数据
        T item;
        //下一个结点
        Node next;

        public Node(T item, Node next) {
            this.item = item;
            this.next = next;
        }
    }

}
