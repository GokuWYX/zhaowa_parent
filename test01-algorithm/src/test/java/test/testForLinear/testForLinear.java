package test.testForLinear;

import org.junit.Test;

public class testForLinear {
    @Test
    public void testForSequenceList(){
        SequenceList<String> strings = new SequenceList<String>(16);
        //测试未插入元素时元素个数
        System.out.println(strings.length());

        //尾插
        strings.insert("GoKu00");
        strings.insert("GoKu01");
        strings.insert("GoKu02");
        strings.insert("GoKu03");

        //顺序定向插入
        strings.insert(2,"Mu");

        //非定向插入
        //java.lang.RuntimeException: insert index is disorder
        //strings.insert(5,"翔");

        System.out.println("此顺序表长度为"+strings.length());

        //超越容量插入测试插入
        //java.lang.RuntimeException: insert index is illegal
        //strings.insert(17,"我就想看看插不插的进去");

        //测试删除
        System.out.println(strings.remove(1).toString());

        //测试通过下标获得值
        for (int i = 0; i < strings.length(); i++) {
            System.out.println(i+":"+strings.get(i).toString());
        }

        strings.clear();
        System.out.println(strings.length());
    }
    @Test
    public void testForQueue(){
        Queue<String> stringQueue = new Queue<>();
        for (int i = 0; i < 9; i++) {
            stringQueue.enqueue(String.valueOf(i));
        }
        System.out.println("删除前遍历开始");
        for (String s : stringQueue) {
            System.out.println(s);
        }
        System.out.println("删除元素是"+stringQueue.dequeue());
        System.out.println(stringQueue.size());
        System.out.println(stringQueue.isEmpty());
        System.out.println("删除后遍历开始");
        for (String s : stringQueue) {
            System.out.println(s);
        }
    }


}
