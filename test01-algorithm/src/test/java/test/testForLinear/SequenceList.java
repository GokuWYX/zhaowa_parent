package test.testForLinear;



public class SequenceList<T> {
    private T[] elements;
    private int length;

    public void clear(){
        length=0;
    }

    public SequenceList(int miniCapacity) {
        elements = (T[]) new Object[miniCapacity];
        length=0;
    }

    public boolean isEmpty(){
        return length==0;
    }
    public int length(){
        return this.length;
    }

    public T get(int i){

        if (i<0 || i>length-1){
            throw  new RuntimeException("index is illegal");
        }else {
            return elements[i];
        }
    }



    public void insert(int i,T t){
        if (i==elements.length){
            throw  new RuntimeException("full");
        }
        if (i>length || i<0){
            throw  new RuntimeException("error illgle");
        }

        for (int index = length; index >i ; index++) {
                elements[i]=elements[i-1];
        }
        elements[i]=t;
        length++;
    }


    public void insert(T t){
        if (length==elements.length){
            throw new RuntimeException("SequenceList is full");
        }else {
            elements[length++]=t;
        }

    }



    public T remove(int i){
        if (i>length || i<0){
            throw  new RuntimeException("error illgle");
        }
        T result = elements[i];
        for (int index = i; index < length-1; index++) {
            elements[index]=elements[index+1];
        }
        length--;
        return result;

    }
    public int indexOf(T t){
        if (t==null){
            throw new RuntimeException("element is illegal");
        }
        int result=-1;
        for (int i = 0; i < elements.length; i++) {
            if (t.equals(elements[i])){
                result =i;
            }
        }
        return result;
    }
}
