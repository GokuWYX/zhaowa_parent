package test.testForLinear;

import java.util.Iterator;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test.testForLinear
 * @ClassName: Queue
 * @Author: Administrator
 * @Description: 2021年12月1日15:03:43队列练习复习
 * @Date: 2021/12/1 15:03
 * @Version: 1.0
 */
public class Queue<T> implements Iterable<T>{
    //记录首结点
    private Node head;
    //当前栈的元素个数
    private int N;
    //记录最后一个结点
    private Node last;

    public Queue() {
        this.head = new Node(null,null);
        last=null;
        N=0;
    }

    //从队列中拿出一个元素
    public T dequeue(){
        if (isEmpty()){
            return null;
        }
        Node oldFirst = head.next;
        head.next=oldFirst.next;
        N--;
        if (isEmpty()){
            last=null;
        }
        return oldFirst.item;

    }
    //往队列中插入一个元素
    public void enqueue(T t){
        if (last==null){
            last=new Node(t,null);
            head.next=last;
            N++;
        }else {
            Node oldLast = last;
            last=new Node(t,null);
            oldLast.next=last;
            N++;
        }

    }

    //获取队列中元素的个数
    public int size(){return N;}
    //判断队列是否为空，是返回true，否返回false
    public boolean isEmpty(){return N==0;}

    @Override
    public Iterator<T> iterator() {
        return new QIterator();
    }
    public class QIterator implements Iterator{
        public Node node=head;
        @Override
        public boolean hasNext() {
            return node.next!=null;
        }

        @Override
        public Object next() {
            Node result = node.next;
            node=node.next;
            return result.item;
        }
    }

    private class Node{
        public T item;
        public Node next;

        public Node(T item, Node next) {
            this.item = item;
            this.next = next;
        }
    }
}
