package test.testForLinear;

import java.util.Stack;

public class SymbolMatch {
    public static void main(String[] args) {
        String str1="(fdafds(fafds)())";
        String str2="(fdafds(fafds)()";
        System.out.println(issMatch(str1));
        System.out.println(issMatch(str2));
    }
    public static boolean issMatch(String str){
        Stack<String> stack = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            String s = str.charAt(i) + "";
            if (s.equals("(")){
                stack.push(s);
            }else if (s.equals(")")){
                String pop = stack.pop();
                if (pop==null){
                    return false;
                }
            }
        }
        if (stack.size()==0){
            return true;
        }else {
            return false;
        }
    }

}

