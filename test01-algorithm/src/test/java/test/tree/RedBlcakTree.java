package test.tree;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test.tree
 * @ClassName: RedBlcakTree
 * @Author: GoKu
 * @Description: 红黑树
 * 1、红连接都为做链接
 * 2、任何一个节点只能和一条红连接相连
 * 3、任意空连接到根节点的路径上的黑链接数量相同
 *
 * @Date: 2022/4/18 9:37
 * @Version: 1.0
 */
public class RedBlcakTree {
    /*
    * 左旋
        当某个结点的左子结点为黑色，右子结点为红色，此时需要左旋。
        前提：当前结点为h，它的右子结点为x；
        左旋过程：
        1.让x的左子结点变为h的右子结点：h.right=x.left;
        2.让h成为x的左子结点：x.left=h;
        3.让h的color属性变为x的color属性值：x.color=h.color;
        4.让h的color属性变为RED：h.color=true;
    * */
    private Node leftReverse(Node h){
        // h的右子节点为x
        Node x=h.right;
        // 1.让x的左子结点变为h的右子结点：h.right=x.left;
        h.right=x.left;
        // 2.让h成为x左子节点
        x.left=h;
        // 3.让h的color属性值变为x的color属性值
        x.color=h.color;
        // 4.h的color属性变为RED
        h.color=RED;
        return x;
    }






    private static final boolean RED=true;
    private static final boolean BLACK=false;
    /*
    * 因为每个结点都只会有一条指向自己的链接（从它的父结点指向它），
    * 我们可以在之前的Node结点中添加一个布尔类型的变量color来表示链接的颜色。
    * 如果指向它的链接是红色的，那么该变量的值为true，如果链接是黑色的，那么该变量的值为false。
    * */
    private class Node<Key,Value>{
        //键
        public Key key;
        //值
        public Value value;
        //记录左节点
        public Node left;
        //记录右节点
        public Node right;
        //如果指向它的链接是红色的，那么该变量的值为true，如果链接是黑色的，那么该变量的值为false。
        public boolean color;

        public Node(Key key, Value value, Node left, Node right, boolean color) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
            this.color = color;
        }
    }
}

