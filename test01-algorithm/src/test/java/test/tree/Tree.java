package test.tree;


/**
 * @ProjectName: zhaowa_parent
 * @Package: test.tree
 * @ClassName: Tree
 * @Author: GoKu
 * @Description: 模拟树
 * @Date: 2021/12/10 9:19
 * @Version: 1.0
 */
public class Tree<Key extends Comparable<Key>,Value> {
    private Node root;

    //记录元素个数
    private int N;

    public int size(){
        return N;
    }
    public void put(Key key,Value value){
        root=put(root,key,value);
    }
    private Node put(Node node, Key key,Value value){
         if (node==null){
             return new Node(key,value,null,null);
         }
        int i = key.compareTo(node.key);
         if (i>0){
             node.right=put(node.right,key,value);
         }else if (i<0){
             node.left=put(node.left,key,value);
         }else {
             node.value=value ;
         }
         return node;

    }


    public Value get(Key key){
        return get(root,key);
    }
    public Value get(Node x,Key key){
        if (x==null){
            return null;
        }
        int i = key.compareTo(x.key);
        if (i>0){
            return get(x.right,key);
        }else if (i<0){
            return get(x.left,key);
        }else {
            return x.value;
        }

    }

    public void delete(Key key){
        root=delete(root,key);
    }

    public Node delete(Node x,Key key){
        if (x==null){
            return null;
        }
        int i = key.compareTo(x.key);
        if (i>0){
             x.right=delete(x.right,key);
        }else if (i<0){
             x.left=delete(x.left,key);
        }else {
            //如果key等于x结点的键，完成真正的删除结点动作，要删除的结点就是x；


            //得找到右子树中最小的结点
            //1.如果当前结点的右子树不存在，则直接返回当前结点的左子结点
            if (x.right==null){
                return x.left;
            }

            //2.如果当前结点的左子树不存在，则直接返回当前结点的右子结点
            if (x.left==null){
                return x.right;
            }

            //3.当前结点的左右子树都存在
            //3.1找到右子树中最小的结点
            Node minNode=x.right;
            while (minNode.left!=null){
                minNode=minNode.left;
            }


            //3.2删除右子树中最小的结点
            while (x.left.left==null){
                x.left=null;
            }


            //让x结点的左子树成为minNode的左子树
            minNode.left=x.left;

            //让x结点的右子树成为minNode的右子树
            minNode.right=x.right;
            //让元素个数-1
            N--;
            //让x结点的父结点指向minNode
            x=minNode;
        }
        return x;
    }




    private class Node{
        public Key key;
        private Value value;
        public Node left;
        public Node right;

        public Node(Key key, Value value, Node left, Node right) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
        }
    }
}
