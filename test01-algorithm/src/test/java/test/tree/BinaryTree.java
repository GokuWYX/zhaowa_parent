package test.tree;

import test.testForLinear.Queue;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test.tree
 * @ClassName: Tree
 * @Author: GoKu
 * @Description: 查找二叉树的模拟
 * @Date: 2021/12/6 8:35
 * @Version: 1.0
 */
public class BinaryTree<Key extends Comparable<Key>,Value> {
    private Node root;
    private int N;
    public int size(){
        return N;
    }
    public boolean isEmpty(){
        return N==0;
    }

    /**
     * 插入方法put实现思想：
     * 1.如果当前树中没有任何一个结点，则直接把新结点当做根结点使用
     * 2.如果当前树不为空，则从根结点开始：
     * 2.1如果新结点的key小于当前结点的key，则继续找当前结点的左子结点；
     * 2.2如果新结点的key大于当前结点的key，则继续找当前结点的右子结点；
     * 2.3如果新结点的key等于当前结点的key，则树中已经存在这样的结点，替换该结点的value值即可。
     */
    public void put(Key key,Value value){
        root=put(root,key,value);
    }
    public Node put(Node x,Key key,Value value){
        if (x==null){
            N++;
            return new Node(key,value,null,null);
        }
        int cmp=key.compareTo(x.key);
        if (cmp>0){
            x.right=put(x.right,key,value);
        }
        if (cmp<0){
            x.left=put(x.left,key,value);
        }else {
            x.value=value;
        }
        return x;
    }
    public Value get(Key key){
        return get(root,key);
    }
    public Value get(Node x,Key key){
        if (x==null){
            return null;
        }
        int cmp = key.compareTo(x.key);
        if (cmp>0){
            return get(x.right,key);
        }else if (cmp<0){
            return get(x.left,key);
        }else {
            return x.value;
        }
    }

    public void delete(Key key){
        root=delete(root,key);
    }
    public Node delete(Node x,Key key){
        if (x==null){
            return null;
        }
        int cmp = key.compareTo(x.key);
        if (cmp>0){
            x.right=delete(x.right,key);
        }else if (cmp<0){
            x.left=delete(x.left,key);
        }else {
            if (x.right==null){
                return x.left;
            }
            if (x.left==null){
                return x.right;
            }
            Node miniNode = x.right;
            while (miniNode.left!=null){
                miniNode=miniNode.left;
            }
            Node n = x.right;
            while (n.left!=null){
                if (n.left.left==null){
                    n.left=null;
                }else {
                    n=n.left;
                }
            }
            miniNode.left = x.left;
            miniNode.right=x.right;
            x=miniNode;
            N--;
        }
        return x;
    }

    public Queue<Key> preErgodic(){
        Queue<Key> keys=new Queue<>();
        preErgodic(root,keys);
        return keys;
    }
    public void preErgodic(Node x,Queue<Key> keys){
        if (x==null){
            return;
        }
        keys.enqueue(x.key);
        //2.找到当前结点的左子树，如果不为空，递归遍历左子树
        if (x.left!=null){
            preErgodic(x.left,keys);
        }
        //3.找到当前结点的右子树，如果不为空，递归遍历右子树
        if (x.right!=null){
            preErgodic(x.right,keys);
        }
    }

    public Queue<Key> midErgodic(){
        Queue<Key> queue = new Queue<>();
        midErgodic(root,queue);
        return queue;
    }


    private void midErgodic(Node x,Queue<Key> keys){
        if (x==null){
            return;
        }
        if (x.left!=null){
            midErgodic(x.left,keys);
        }
        keys.enqueue(x.key);

        if (x.right!=null){
            midErgodic(x.right,keys);
        }
    }

    //使用后序遍历，获取整个树中的所有键
    public Queue<Key> afterErgodic(){
        Queue<Key> queue = new Queue<>();
        afterErgodic(root,queue);
        return queue;
    }

    //使用后序遍历，把指定树x中的所有键放入到keys队列中
    private void afterErgodic(Node x,Queue<Key> keys){
        if (x==null){
            return;
        }
        if (x.left!=null){
            afterErgodic(x.left,keys);
        }
        if (x.right!=null){
            afterErgodic(x.right,keys);
        }
        keys.enqueue(x.key);
    }




    public Queue<Key> layerErgodic(){
        Queue<Node> nodes = new Queue<>();
        Queue<Key> keys = new Queue<>();
        nodes.enqueue(root);
        while (!nodes.isEmpty()){
            Node node = nodes.dequeue();
            keys.enqueue(node.key);
            if (node.left!=null){
                nodes.enqueue(node.left);
            }
            if (node.right!=null){
                nodes.enqueue(node.right);
            }
        }
        return keys;

    }






    private class Node{
        private Key key;
        private Value value;
        public Node left;
        public Node right;

        public Node(Key key, Value value, Node left, Node right) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
        }
    }
}
