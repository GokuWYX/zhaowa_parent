package test.XuLie;

import java.io.*;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test.XuLie
 * @ClassName: SerializTest
 * @Author: GoKu
 * @Description: 序列化和反序列化
 * @Date: 2022/3/24 8:51
 * @Version: 1.0
 */
public class SerializTest {
    public static void main(String[] args) throws Exception {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("F:/User.txt")));
        Student wyx = new Student(19, "wyx");
        oos.writeObject(wyx);
        oos.flush();
        oos.close();
    }

}
