package test.XuLie;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test.XuLie
 * @ClassName: SerialTest01
 * @Author: GoKu
 * @Description:
 * @Date: 2022/3/24 9:06
 * @Version: 1.0
 */
public class SerialTest01 {
    public static void main(String[] args) throws Exception{
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("object.out"));
        Student student = (Student) ois.readObject();
        System.out.println(student.getId()+student.getName());

        ObjectInputStream oiss = new ObjectInputStream(new FileInputStream(new File("F:\\User.txt")));
        Student studenta = (Student) oiss.readObject();
        System.out.println(studenta.getId()+studenta.getName());

        ArrayList<Object> arrayList = new ArrayList<>(12);
        System.out.println(arrayList.size());
        int[] ints = new int[12];
        System.out.println(ints.length);


    }
}
