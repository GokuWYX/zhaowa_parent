package test.XuLie;

import java.io.Serializable;
import java.util.Objects;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test.XuLie
 * @ClassName: Student
 * @Author: GoKu
 * @Description:
 * @Date: 2022/3/24 8:50
 * @Version: 1.0
 */
public class Student implements Serializable {
    private int Id;
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Id == student.Id &&
                Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, name);
    }

    @Override
    public String toString() {
        return "Student{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                '}';
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student() {
    }

    public Student(int id, String name) {
        Id = id;
        this.name = name;
    }
}
