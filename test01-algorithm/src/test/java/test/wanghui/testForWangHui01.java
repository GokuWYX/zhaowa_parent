package test.wanghui;

import java.util.Scanner;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test
 * @ClassName: testForWangHui01
 * @Author: 2022国产帅哥
 * @Description:
 * @Date: 2022/3/12 17:27
 * @Version: 1.0
 */
public class testForWangHui01 {
    public static void main(String[] args) {
        //顶一个scanner扫描器
        //作用：获取控制台的数据
        Scanner input=new Scanner(System.in);


        //获得stb的成绩
        //作用：将scanner获取的控制台的数据指定数据类型
        System.out.print("STB的成绩是：");
        int stbScore = input.nextInt();

        //获得java成绩
        //作用：将scanner获取的控制台的数据指定数据类型
        System.out.print("Java的成绩是：");
        int javaScore = input.nextInt();

        //获得sql成绩
        //作用：将scanner获取的控制台的数据指定数据类型
        System.out.print("SQL的成绩是：");
        int sqlScore = input.nextInt();


        System.out.println("---------------------------");
        System.out.println("STB\t\t"+"Java\t"+"SQL");
        System.out.println(stbScore+"\t\t"+javaScore+"\t\t"+sqlScore);
        System.out.println("---------------------------");
        //成绩差
        int range=0;
        if (sqlScore>javaScore){
            range=sqlScore-javaScore;
        }else {
            range=javaScore-sqlScore;
        }

        System.out.println("Java和SQL的成绩差："+range);
        //平均分
        int average=(stbScore+javaScore+sqlScore)/3;
        System.out.println("三门课的平均分是："+ (double)average);
    }
}
