package test.wanghui;

/**
 * @ProjectName: zhaowa_parent
 * @Package: test.tree
 * @ClassName: tester
 * @Author: 2022国产帅哥
 * @Description:
 * @Date: 2022/3/9 17:22
 * @Version: 1.0
 */
public class testForWangHui00 {

    public static void main(String[] args) {

        //1、声明变量

        //T恤单价
        int tShirtPrise=245;

        //网球鞋单价
        int shoePrise=570;

        //网球拍单价
        int batPrise=320;

        //T恤数量
        int tShirtPriseNum=2;

        //网球鞋数量
        int shoeNum=1;

        //网球拍数量
        int batNum=1;

        //折扣(当你输出的时候需要*10，因为我们习惯性成为8折，而不是0.8折)
        int discount=8;

        //实际缴费
        int payment=1500;

        //2、公示运算
        //总金=单价*数量;
        double total=(double) (tShirtPrise*tShirtPriseNum+shoePrise*shoeNum+batPrise*batNum)*discount/10;

        //找钱=缴费-总金额
        double change=payment-total;

        //定义积分  100元累计3分，这个可以模拟出来
        int score=(int)(total/100*3);

        System.out.println("********消费单********");

        System.out.println("购买物品\t单价\t个数\t金额");

        System.out.println("T恤\t\t￥"+tShirtPrise+"\t"+tShirtPriseNum+"\t\t￥"+tShirtPrise*tShirtPriseNum);

        System.out.println("网球鞋\t￥"+shoePrise+"\t"+shoeNum+"\t\t￥"+shoePrise*shoeNum);

        System.out.println("网球拍\t￥"+batPrise+"\t"+batNum+"\t\t￥"+batPrise*batNum);

        System.out.println();

        System.out.println("折扣：\t\t"+discount+"折");

        System.out.println("消费总金额:\t￥"+total);

        System.out.println("实际缴费\t\t￥"+payment);

        System.out.println("找钱\t\t￥"+change);

        System.out.println("本次购物所获得的积分是："+score);

    }

}
